# Pye
A 2D rouge-like action-adventure video game, that sees the protagonist stuck in a time-loop which begins and ends with their own demise.

The setting is the aftermath of an invasion, the result of years of tension between neighbouring nations finally bubbling over. Taking different routes in each cycle allows them to see the many different perspectives on what happened, encountering all sorts of characters along the way. Finding collectables will unlock new areas (often down a different path) or help you power up, in the form of new elements to use in the weapons system.

Be careful, as each run will completly reset the protagonist's memory and relationships with characters they run into. This can be helpful, trying a different tactic in a dialogue, but requires the use of one of a limited number of slots in a tape deck to propogate important information to a future loop.

The game is ostensibly complete with regard to features (settings, save files, audio, etc.), but a huge amount of work is left to bulk out the content (more levels, enemies, weapons, characters, etc.). That is notwithstanding glaring design issues - it hasn't quite found the fun and may be a tad difficult.

A quick trailer / demo can be found here: [youtube](https://youtu.be/fBWHsRfD0bE).

## Weapon System
The game features a dynamic, stacking weapon system that allows the player to rearrange the elements that they find into different combinations. Each element has a light and heavy attack (sometimes with variations when moving) as well as a modifier that is applied by the previous element in the chain.

For example, steel -> fire -> gunpowder would use the attack of steel (based on whether it was in the light or heavy slot), setting enemies on fire for a few seconds, with each tick of damage creating a small blast that damages other nearby enemies. On the other hand, steel -> gunpowder -> fire would use the same attack but instead create a small blast that damages other nearby enemies and sets them on fire.

## Scenes
All scenes are loaded at run-time from the corresponding `.g0ttlieb` files, be they for dialogue, combat or menu. See the comments in the existing files for the exact layout.

Combat scenes:

* Positions are based on a 360x640 world
* Level layouts are loaded from `levels` (minus `.txt`), tileset from `visual` (minus `.png`), background from `visual` (minus `-background.png`) and music from `music` (minus `.wav`)
* Transitions load another combat scene from `scenes` (minus `.g0ttlieb`)
* NPCs are defined in `src/world/npc::getTextureIndex` and based on `resources/visual/npcs.png`
* Collectables defined in `src/world/collectable::getTextureIndex` and based on `resources/visual/collectables.png`
* Enemy names are defined in `src/characters/spawner::create`

Dialogue scenes:

* NPCs are loaded from `visual` (minus `.png`)

Menu scenes:

* Menu types are defined in `src/scenes/scene_loader::loadMenuScene`

Level files are `.txt` files with 45 rows and 40 columns. Letters correspond to the associated tile in the tileset. `a-t` form ground, `u-z` are eyecandy only, `-` is air and `|` are invisible walls. The middle 4 tiles of the bottom row should be ground, as the HUD lives there.

WARNING: for reasons that feel inconceivable at the time of writing, there is no sanitisation behind those files so ... ahh ... just don't get it wrong!

## Flags
Certain flags are auto-generated as things happen in the game:

| Name                      | Event                               |
| --------------------------|-------------------------------------|
| *<combat-scene>\_entered* | Enter a combat scene                |
| *<combat-scene>\_cleared* | All enemies in a scene are defeated |
| *dialogue\_<scene>*       | Start a dialogue scene              |

Additionally, certain flags have special functionality:

| Type           | Description                                                    |
| ---------------|----------------------------------------------------------------|
| *!flag*        | Unsets the given flag                                          |
| *@str[<=>]int* | Checks the number of flags containing "str" relative to the int|
| *time[<=>]int* | Checks the current run time relative to the given int          |
| *%flag*        | Makes a dialogue set flag persistent                           |
| *^flag*        | Save on flag collection in a combat scene                      |

Query flags can be chained together by using `&` or `|`, with either side needing
the relevant qualifiers (most likely being `@`).

Similarly, multiple flags can be set in a dialogue by using a comma. Again, either side needs the relevant qualifiers (in this case only `%`).

There are also special flags for interacting with the tape deck:

| Name            | Description                                                   |
| ----------------|---------------------------------------------------------------|
| *tape_<>*       | Adds the flag to the tape deck                                |
| *free_tape*     | Checks if there is a free tape slot                           |
| *tape_full*     | Checks if there are not free tape slots                       |
| *tape_slot_<i>* | Adds another tape slot                                        |

Collectables trigger flags when the player hits them. Special collectables exist:

| Name               | Description            |
| -------------------|------------------------|
| *invis_<name>*     | No texture is shown    |
| *dialogue_<scene>* | Loads a dialogue scene |
| *element_<name>*   | Enable the element    |

NOTE: once a flag has been added, all further instances will not proc again.

## Setup
The only dependency (aside from cmake), is `libsfml-dev`. Development was done on Ubuntu 18.04 and testing on Windows 10, using SFML2.4. Newer versions may work but have not been tested yet.

The Makefiles scattered around provide shortcuts for some of the standard build commands. The one in the root directory uses Docker, while the one in `src` does everything in the local build folder.

The soundtrack is not part of the git repo, but any `.wav` file will work, provided it is named appropriately (in-line with the `.g0ttlieb` file for the scene). Joey Bada$$'s "1999" is canon.

## Notes
* Keep tilesets simple - it's more about the colour contrast and highlighting than it is about the crazy pattern.

## Ideas
### Enemies
* Speared enemy - long windup to attack but a long range and no CD once started.

### Dynamic Music
Music that is based on the current area and active enemies.

* Every level has a base track (soft, fairly repetition)
* Each enemy has a distinctive sound (specific instrument).
* Each enemy that is alive adds one note to the current loop.
* Whenever an enemy dies, that note is removed.

I would need a lot of music theory to have a go at this OR machine learning ... neither of which are particularly appealing at the moment.

### Weapons
* Drop a seedling that bursts after 1s

## TODO
* Ensure the appropriate cmake build type is used
* Replace `.g0ttlieb` files with JSON + sanitize all input
* Replace raw memory management with smart pointers
* Fix the relative pathed includes (cmake `target_include_directories`)
* Cleanup `-background.png` hidden logic?
* Path planning around gateways (on the same platform but gateway between - need to purge the cached path when the gateway changes)
