#include <sstream>
#include <iomanip>
#include "overlay.hpp"
#include "inventory.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const float DAMAGE_TIME = 0.4f;
const float DAMAGE_FADE_IN = 0.05f;
const float DAMAGE_FADE_OUT = 0.2f;
const float TRANSPARENT = 0.f;
const float OPAQUE = 255.f;

const float OVERLAY_LEFT = 280;
const float OVERLAY_TOP = 352;

const float HP_LEFT = 15.f;
const float HP_TOP = 1.f;
const float HP_HEIGHT = 5.f;
const float HP_WIDTH = 50.f;
const sf::Color HP_COLOR(128,0,0);

const float ICON_HEIGHT = 5.f;
const float ICON_WIDTH = 10.f;
const int LIGHT_INDEX = 0;
const float LIGHT_LEFT = 2.f;
const float LIGHT_TOP = 1.f;
const int HEAVY_INDEX = 4;
const float HEAVY_LEFT = 68.f;
const float HEAVY_TOP = 1.f;

const int FONT_SIZE = 4;
const sf::Color GREYED_OUT(100,100,100);
const sf::Color NO_SHADE(255,255,255);

sf::FloatRect HIDDEN(0,0,0,0);

Overlay::Overlay():
        damageTime(0.f),
        transparency(0.f),
        currentLight(nullptr),
        currentHeavy(nullptr){
    overlayTexture.loadFromFile(ResourceManager::getVisual("overlay.png"));
    overlaySprite.setTexture(overlayTexture);
    overlaySprite.setPosition(OVERLAY_LEFT, OVERLAY_TOP);

    damageTexture.loadFromFile(ResourceManager::getVisual("damage.png"));
    damageSprite.setTexture(damageTexture);
    damageSprite.setPosition(0.f, 0.f);

    iconTexture.loadFromFile(ResourceManager::getVisual("weapon-icons.png"));
    iconVertices.setPrimitiveType(sf::Quads);
    iconVertices.resize(8);

    healthBar.setPosition(OVERLAY_LEFT + HP_LEFT, OVERLAY_TOP + HP_TOP);
    healthBar.setFillColor(HP_COLOR);

    setupText();
    updateWeaponIcons();
    SpriteHelper::setScale(this);
}

void Overlay::setupText(){
    font.loadFromFile(ResourceManager::getFont("overlay.ttf"));
    float scale = Settings::instance().scale();

    lightCooldown.setFont(font);
    lightCooldown.setCharacterSize(FONT_SIZE * scale);
    lightCooldown.setFillColor(sf::Color::White);
    lightCooldown.setPosition(
            (OVERLAY_LEFT + LIGHT_LEFT + ICON_WIDTH/2.f) * scale,
            (OVERLAY_TOP + LIGHT_TOP + ICON_HEIGHT/2.f) * scale);

    heavyCooldown.setFont(font);
    heavyCooldown.setCharacterSize(FONT_SIZE * scale);
    heavyCooldown.setFillColor(sf::Color::White);
    heavyCooldown.setPosition(
            (OVERLAY_LEFT + HEAVY_LEFT + ICON_WIDTH/2.f) * scale,
            (OVERLAY_TOP + HEAVY_TOP + ICON_HEIGHT/2.f) * scale);

    health.setFont(font);
    health.setCharacterSize(FONT_SIZE * scale);
    health.setFillColor(sf::Color::White);
    health.setPosition(
            (OVERLAY_LEFT + HP_LEFT + HP_WIDTH/2.f) * scale,
            (OVERLAY_TOP + HP_TOP + HP_HEIGHT/2.f) * scale);
}

Overlay::~Overlay(){}

void Overlay::update(float dt){
    Inventory& inv = Inventory::instance();
    std::stringstream healthStream;
    healthStream << inv.getHealthCurrent() << "/" << inv.getHealthMax();
    SpriteHelper::centerAlignText(health, healthStream.str());
    healthBar.setSize(sf::Vector2f(HP_WIDTH * inv.getHealthPercent(), HP_HEIGHT));

    if(weaponOnCooldown(Inventory::instance().lightWeapon())){
        SpriteHelper::shadeQuad(&iconVertices[LIGHT_INDEX], GREYED_OUT);
        SpriteHelper::centerAlignText(lightCooldown,
                getCDString(inv.lightWeapon()));
    }
    else
        SpriteHelper::shadeQuad(&iconVertices[LIGHT_INDEX], NO_SHADE);

    if(weaponOnCooldown(Inventory::instance().heavyWeapon())){
        SpriteHelper::shadeQuad(&iconVertices[HEAVY_INDEX], GREYED_OUT);
        SpriteHelper::centerAlignText(heavyCooldown,
                getCDString(inv.heavyWeapon()));
    }
    else
        SpriteHelper::shadeQuad(&iconVertices[HEAVY_INDEX], NO_SHADE);

    if(inv.hasLostHealth())
        damageTime = DAMAGE_TIME;
    if(damageTime > 0){
        damageTime -= dt;
        if(damageTime < DAMAGE_FADE_OUT){
            transparency -= (dt / DAMAGE_FADE_OUT) * OPAQUE;
            if(transparency < TRANSPARENT)
                transparency = TRANSPARENT;
            damageSprite.setColor(sf::Color(255, 255, 255, transparency));
        }
        else if(transparency < OPAQUE){
            transparency += (dt / DAMAGE_FADE_IN) * OPAQUE;
            if(transparency > OPAQUE)
                transparency = OPAQUE;
            damageSprite.setColor(sf::Color(255, 255, 255, transparency));
        }
    }
}

std::string Overlay::getCDString(Weapon* weapon){
    std::stringstream cd;
    cd << std::fixed << std::setprecision(1) << weapon->getCooldown();
    return cd.str();
}

void Overlay::updateWeaponIcons(){
    Inventory& inv = Inventory::instance();
    if(inv.lightWeapon() == currentLight && inv.heavyWeapon() == currentHeavy)
        return;
    currentLight = inv.lightWeapon();
    currentHeavy = inv.heavyWeapon();

    if(currentLight != nullptr){
        int lightIndex = currentLight->getIconIndex();
        sf::FloatRect texture(lightIndex * ICON_WIDTH, 0.f,
                ICON_WIDTH, ICON_HEIGHT);
        sf::FloatRect lightPos(OVERLAY_LEFT + LIGHT_LEFT, OVERLAY_TOP + LIGHT_TOP,
                ICON_WIDTH, ICON_HEIGHT);
        SpriteHelper::setTextureQuad(&iconVertices[LIGHT_INDEX], lightPos,
                texture);
    }
    else
        SpriteHelper::setTextureQuad(&iconVertices[LIGHT_INDEX], HIDDEN, HIDDEN);

    if(currentHeavy != nullptr){
        int heavyIndex = currentHeavy->getIconIndex();
        sf::FloatRect texture(heavyIndex * ICON_WIDTH, 0.f,
                ICON_WIDTH, ICON_HEIGHT);
        sf::FloatRect heavyPos(OVERLAY_LEFT + HEAVY_LEFT, OVERLAY_TOP + HEAVY_TOP,
                ICON_WIDTH, ICON_HEIGHT);
        SpriteHelper::setTextureQuad(&iconVertices[HEAVY_INDEX], heavyPos,
                texture);
    }
    else
        SpriteHelper::setTextureQuad(&iconVertices[HEAVY_INDEX], HIDDEN, HIDDEN);
}

void Overlay::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    states.texture = &iconTexture;
    target.draw(overlaySprite, states);
    target.draw(iconVertices, states);
    target.draw(healthBar, states);
    target.draw(health);
    if(weaponOnCooldown(Inventory::instance().lightWeapon()))
        target.draw(lightCooldown);
    if(weaponOnCooldown(Inventory::instance().heavyWeapon()))
        target.draw(heavyCooldown);
    if(damageTime > 0.f)
        target.draw(damageSprite, states);
}

bool Overlay::weaponOnCooldown(Weapon* weapon) const {
    if(weapon == nullptr)
        return false;
    return weapon->onCooldown();
}
