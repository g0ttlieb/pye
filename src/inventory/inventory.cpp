#include <cstring>
#include "inventory.hpp"
#include "../helpers/music.hpp"

const int MAX_HP = 250;

const int ELEMENT_OFFSET = std::string("element_").length();
const int DIALOGUE_OFFSET = std::string("dialogue_").length();
const int TIME_QUERY_SIGN_POS = std::string("time").length();

Inventory::Inventory():
        runTime(0.f),
        tapeSize(0),
        light(nullptr),
        heavy(nullptr),
        elements(Element::getAllElements()),
        maxHp(MAX_HP),
        currentHp(MAX_HP),
        currentHpPercent(1.f),
        warping(false),
        stashedLight(nullptr),
        stashedHeavy(nullptr){
    resetBodyToDefault();
}

Inventory::~Inventory(){
    for(auto element : elements)
        delete element.second;
}

void Inventory::update(float dt){
    runTime += dt;
}

void Inventory::resetRun(){
    currentHp = MAX_HP;
    currentHpPercent = 1.f;
    filterPersistentCollectables();
    resetWeapons();
}

float Inventory::getRunTime(){
    return runTime;
}

void Inventory::setRunTime(float t){
    runTime = t;
}

bool Inventory::hasCollectable(std::string name){
    size_t orIndex = name.find("|");
    if(orIndex != std::string::npos){
        std::string firstFlag = name.substr(0, orIndex);
        std::string secondFlag = name.substr(orIndex+1);
        return hasCollectable(firstFlag) || hasCollectable(secondFlag);
    }
    size_t andIndex = name.find("&");
    if(andIndex != std::string::npos){
        std::string firstFlag = name.substr(0, andIndex);
        std::string secondFlag = name.substr(andIndex+1);
        return hasCollectable(firstFlag) && hasCollectable(secondFlag);
    }

    if(name.find("time") == 0)
        return queryTimeMatches(name);
    if(name[0] == '@')
        return queryCollectablesMatches(name);
    if(name == "free_tape")
        return isFreeTapeDeckSlot();
    else if(name == "tape_full")
        return !isFreeTapeDeckSlot();
    return collectables.count(name) > 0;
}

bool Inventory::queryTimeMatches(std::string query){
    char sign = query[TIME_QUERY_SIGN_POS];
    int val = std::stoi(query.substr(TIME_QUERY_SIGN_POS+1));
    return compareValsWithSign(sign, runTime, val);
}

bool Inventory::queryCollectablesMatches(std::string query){
    int signIndex = std::strcspn(query.c_str(), "<=>");
    char sign = query[signIndex];
    std::string expression = query.substr(1, signIndex-1);
    int val = std::stoi(query.substr(signIndex+1));
    int matchCount = 0;
    for(auto entry : collectables){
        if(entry.first.find(expression) != std::string::npos)
            matchCount++;
    }
    return compareValsWithSign(sign, matchCount, val);
}

bool Inventory::compareValsWithSign(char sign, int left, int right){
    if(sign == '>' && left > right)
        return true;
    if(sign == '<' && left < right)
        return true;
    if(sign == '=' && left == right)
        return true;
    return false;
}

void Inventory::addCollectable(std::string name, bool persistence){
    if(name[0] == '!'){
        collectables.erase(name.substr(1));
        return;
    }

    // Used for warping
    if(name == "end_entered"){
        collectables.erase("woods_shortcut_entered");
        collectables.erase("no_mans_land_start_entered");
        collectables.erase("obsfary_trenches_start_entered");
    }

    if(collectables.count(name) > 0)
        return;

    if(name.find("dialogue_") == 0)
        newDialogueScene = SceneTransition(name.substr(DIALOGUE_OFFSET),
                Transition::OVERLAY_CURRENT);
    else if(name.find("element_") == 0)
        elements[name.substr(ELEMENT_OFFSET)]->enabled = true;
    else if(name.find("tape_slot") == 0)
        tapeSize++;
    else if(name.find("tape_") == 0)
        tapeDeck.push_back(name);
    collectables[name] = persistence;
    updateMusic(name);
}

void Inventory::removeCollectable(std::string name){
    collectables.erase(name);
}

void Inventory::updateMusic(std::string name){
    if(name == "end_cleared")
        Music::playMusic("victory");
    else if(name=="invis_final_battle_lockdown" && !hasCollectable("end_cleared"))
        Music::playMusic("end");
}

std::map<std::string, bool>& Inventory::getCollectables(){
    return collectables;
}

void Inventory::filterPersistentCollectables(){
    std::map<std::string, bool>::iterator itr = collectables.begin();
    while (itr != collectables.end()) {
        if (!itr->second)
           itr = collectables.erase(itr);
        else
           ++itr;
    }
}

int Inventory::getTapeDeckSize(){
    return tapeSize;
}

std::vector<std::string>& Inventory::getTapeDeck(){
    return tapeDeck;
}

void Inventory::removeTape(unsigned int index){
    if(index >= tapeDeck.size())
        return;
    removeCollectable(tapeDeck[index]);
    tapeDeck.erase(tapeDeck.begin() + index);
}

bool Inventory::isFreeTapeDeckSlot(){
    return tapeSize > (int) tapeDeck.size();
}

bool Inventory::hasNewDialogueScene(){
    return !newDialogueScene.noNewScene();
}

SceneTransition Inventory::getNewDialogueScene(){
    SceneTransition newScene = newDialogueScene;
    newDialogueScene = SceneTransition();
    return newScene;
}

Inventory& Inventory::instance(){
    static Inventory instance;
    return instance;
}

Weapon* Inventory::lightWeapon(){
    return light;
}

Weapon* Inventory::heavyWeapon(){
    return heavy;
}

void Inventory::setLightWeapon(Weapon* weapon){
    light = weapon;
}

void Inventory::setHeavyWeapon(Weapon* weapon){
    heavy = weapon;
}

std::map<std::string,Element*>& Inventory::getElements(){
    return elements;
}

int Inventory::getHealthMax(){
    return maxHp;
}

int Inventory::getHealthCurrent(){
    return currentHp;
}

float Inventory::getHealthPercent(){
    return currentHpPercent;
}

void Inventory::setHealth(int hp){
    currentHp = hp;
    currentHpPercent = currentHp / maxHp;
}

void Inventory::dealDamage(float damage){
    if(currentHp > 0){
        currentHp = std::max(currentHp - damage, 0.f);
        currentHpPercent = currentHp / maxHp;
        lostHp = true;
    }
}

bool Inventory::hasLostHealth(){
    bool result = lostHp;
    lostHp = false;
    return result;
}

int Inventory::getSaveIndex(){
    return saveIndex;
}

void Inventory::setSaveIndex(int index){
    saveIndex = index;
}

void Inventory::updateResolution(){
    for(auto element : elements){
        element.second->light->updateResolution();
        element.second->heavy->updateResolution();
        element.second->mod->updateResolution();
    }
}

void Inventory::updateVolume(){
    for(auto element : elements){
        element.second->light->updateVolume();
        element.second->heavy->updateVolume();
        element.second->mod->updateVolume();
    }
}

void Inventory::reset(){
    for(auto element : elements){
        element.second->inUse = false;
        element.second->enabled = false;
        element.second->reset();
    }
    tapeSize = 0;
    tapeDeck.clear();
    collectables.clear();
    resetBodyToDefault();
}

void Inventory::resetWeapons(){
    for(auto element : elements)
        element.second->reset();
}

bool Inventory::canWarp(){
    if(hasCollectable("woods_shortcut_entered"))
        return true;
    if(hasCollectable("no_mans_land_start_entered"))
        return true;
    if(hasCollectable("obsfary_trenches_start_entered"))
        return true;
    return false;
}

sf::Vector2f Inventory::getWarpPos(){
    if(hasCollectable("woods_shortcut_entered"))
        return sf::Vector2f(568, 328);
    if(hasCollectable("no_mans_land_start_entered"))
        return sf::Vector2f(10, 256);
    if(hasCollectable("obsfary_trenches_start_entered"))
        return sf::Vector2f(624, 256);
    return sf::Vector2f();
}

void Inventory::warp(){
    warping = true;
}

bool Inventory::isWarping(){
    return warping;
}

void Inventory::setWarped(){
    warping = false;
}

Direction Inventory::getBodyDir(){
    return bodyDir;
}

sf::Vector2f Inventory::getBodyPos(){
    return bodyPos;
}

sf::Vector2f Inventory::getBodyCollectablePos(){
    return bodyCollectablePos;
}

void Inventory::setBody(sf::Vector2f pos, sf::Vector2f collectablePos,
        Direction dir){
    bodyDir = dir;
    bodyPos = pos;
    bodyCollectablePos = collectablePos;
}

void Inventory::resetBodyToDefault(){
    bodyDir = Direction::RIGHT;
    bodyPos = sf::Vector2f(317, 160);
    bodyCollectablePos = sf::Vector2f(316, 152);
}

void Inventory::stashLoadout(){
    for(auto it = collectables.begin(); it != collectables.end();){
        if(it->first.find("element") == 0 || it->first.find("tape") == 0){
            stashedCollectables[it->first] = it->second;
            it = collectables.erase(it);
        }
        else
            ++it;
    }
    for(auto element : elements)
        element.second->enabled = false;
    tapeSize = 0;
    tapeDeck.clear();
    stashedLight = light;
    stashedHeavy = heavy;
    light = nullptr;
    heavy = nullptr;
}

void Inventory::popLoadout(){
    for(auto collectable : stashedCollectables)
        addCollectable(collectable.first, collectable.second);
    stashedCollectables.clear();
    if(stashedLight)
        light = stashedLight;
    if(stashedHeavy)
        heavy = stashedHeavy;
    stashedLight = nullptr;
    stashedHeavy = nullptr;
}
