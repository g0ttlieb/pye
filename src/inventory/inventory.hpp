#pragma once

#include <map>
#include "../weapons/element.hpp"
#include "../scenes/scene_transition.hpp"

class Inventory {
public:
    static Inventory& instance();
    ~Inventory();

    void update(float dt);
    void resetRun();

    float getRunTime();
    void setRunTime(float t);

    bool hasCollectable(std::string name);
    void addCollectable(std::string name, bool persistence);
    void removeCollectable(std::string name);
    std::map<std::string, bool>& getCollectables();

    int getTapeDeckSize();
    std::vector<std::string>& getTapeDeck();
    void removeTape(unsigned int index);

    bool hasNewDialogueScene();
    SceneTransition getNewDialogueScene();

    Weapon* lightWeapon();
    Weapon* heavyWeapon();
    void setLightWeapon(Weapon* weapon);
    void setHeavyWeapon(Weapon* weapon);
    std::map<std::string,Element*>& getElements();

    int getHealthMax();
    int getHealthCurrent();
    float getHealthPercent();
    void setHealth(int hp);
    void dealDamage(float damage);
    bool hasLostHealth();

    int getSaveIndex();
    void setSaveIndex(int number);

    void updateResolution();
    void updateVolume();
    void reset();
    void resetWeapons();

    bool canWarp();
    sf::Vector2f getWarpPos();
    void warp();
    bool isWarping();
    void setWarped();

    Direction getBodyDir();
    sf::Vector2f getBodyPos();
    sf::Vector2f getBodyCollectablePos();
    void setBody(sf::Vector2f bodyPos, sf::Vector2f collectablePos, Direction dir);
    void resetBodyToDefault();

    void stashLoadout();
    void popLoadout();

private:
    Inventory();

    float runTime;

    std::map<std::string, bool> collectables;
    bool queryTimeMatches(std::string flag);
    bool queryCollectablesMatches(std::string flag);
    bool compareValsWithSign(char sign, int left, int right);
    void filterPersistentCollectables();
    void updateMusic(std::string name);

    int tapeSize;
    std::vector<std::string> tapeDeck;
    bool isFreeTapeDeckSlot();

    SceneTransition newDialogueScene;

    Weapon* light;
    Weapon* heavy;
    std::map<std::string,Element*> elements;

    float maxHp;
    float currentHp;
    float currentHpPercent;
    bool lostHp;

    int saveIndex;

    bool warping;

    Direction bodyDir;
    sf::Vector2f bodyPos;
    sf::Vector2f bodyCollectablePos;

    Weapon* stashedLight;
    Weapon* stashedHeavy;
    std::map<std::string, bool> stashedCollectables;
};
