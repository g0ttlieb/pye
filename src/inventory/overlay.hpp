#pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include "../weapons/weapon.hpp"

class Overlay: public sf::Drawable, public sf::Transformable {
public:
    Overlay();
    ~Overlay();
    void update(float dt);
    void updateWeaponIcons();
private:
    sf::Font font;
    sf::Text lightCooldown;
    sf::Text heavyCooldown;
    sf::Text health;
    void setupText();
    std::string getCDString(Weapon* weapon);

    float damageTime;
    float transparency;
    sf::Texture damageTexture;
    sf::Sprite damageSprite;

    sf::Texture overlayTexture;
    sf::Sprite overlaySprite;
    sf::Texture iconTexture;
    sf::VertexArray iconVertices;
    sf::RectangleShape healthBar;
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    bool weaponOnCooldown(Weapon* weapon) const;

    Weapon* currentLight;
    Weapon* currentHeavy;
};
