#include <sstream>
#include "save_manager.hpp"
#include "inventory.hpp"
#include "../helpers/file_helper.hpp"

#include "../weapons/steel_light.hpp"
#include "../weapons/steel_heavy.hpp"
#include "../weapons/steel_mod.hpp"
#include "../weapons/gunpowder_light.hpp"
#include "../weapons/gunpowder_heavy.hpp"
#include "../weapons/gunpowder_mod.hpp"

const int LOOP_TIME_INDEX = 0;
const int TOTAL_TIME_INDEX = 1;
const int LOOP_NUMBER_INDEX = 2;
const int SCENE_FILE_INDEX = 3;
const int LEVEL_TITLE_INDEX = 4;
const int LEVEL_BACKGROUND_INDEX = 5;
const int HP_INDEX = 6;
const int POS_INDEX = 7;
const int FLAGS_INDEX = 9;

const float OUT_OF_WORLD = 1000.f;

void SaveManager::save(){
    save("", "", "", sf::Vector2f(OUT_OF_WORLD, OUT_OF_WORLD));
}

void SaveManager::save(sf::Vector2f playerPos){
    save("", "", "", playerPos);
}

void SaveManager::save(sf::FloatRect playerPos){
    save("", "", "", sf::Vector2f(playerPos.left, playerPos.top));
}

void SaveManager::saveToStart(){
    save("start", "The End Of The Beginning", "restart", sf::Vector2f(77,8));
}

void SaveManager::save(std::string file, std::string name, std::string background,
        sf::Vector2f playerPos){
    Inventory::instance().popLoadout();
    std::vector<std::string> newSave;
    std::vector<std::string> oldSave = FileHelper::readSaveFile(getSaveFileName());
    addTimeToSave(newSave, oldSave, file);
    if(file.empty())
        addOldLevelToSave(newSave, oldSave);
    else
        addNewLevelToSave(newSave, file, name, background);
    addHpToSave(newSave);
    if(playerPos.x == OUT_OF_WORLD)
        addOldPosToSave(newSave, oldSave);
    else
        addNewPosToSave(newSave, playerPos);
    addCollectablesToSave(newSave);
    addWeaponsToSave(newSave);
    FileHelper::writeSaveFile(getSaveFileName(), newSave);
}

void SaveManager::addTimeToSave(LINES& newSave, LINES& oldSave, std::string file){
    Inventory& inv = Inventory::instance();
    float loopTime = std::stof(oldSave[LOOP_TIME_INDEX]);
    float totalTime = std::stof(oldSave[TOTAL_TIME_INDEX]);
    int loopNumber = std::stoi(oldSave[LOOP_NUMBER_INDEX]);
    totalTime = totalTime - loopTime + inv.getRunTime();
    if(file == "start"){
        loopTime = 0.f;
        inv.setRunTime(0);
        loopNumber++;
    }
    else
        loopTime = inv.getRunTime();

    newSave.push_back("# Loop time");
    newSave.push_back(std::to_string(loopTime));
    newSave.push_back("# Total time");
    newSave.push_back(std::to_string(totalTime));
    newSave.push_back("# Loop number");
    newSave.push_back(std::to_string(loopNumber));
}

void SaveManager::addNewLevelToSave(LINES& newSave, std::string file,
        std::string name, std::string background){
    newSave.push_back("# Scene file");
    newSave.push_back(file);
    newSave.push_back("# Level title");
    newSave.push_back(name);
    newSave.push_back("# Level background");
    newSave.push_back(background);
}

void SaveManager::addOldLevelToSave(LINES& newSave, LINES& oldSave){
    newSave.push_back("# Scene file");
    newSave.push_back(oldSave[SCENE_FILE_INDEX]);
    newSave.push_back("# Level title");
    newSave.push_back(oldSave[LEVEL_TITLE_INDEX]);
    newSave.push_back("# Level background");
    newSave.push_back(oldSave[LEVEL_BACKGROUND_INDEX]);
}

void SaveManager::addHpToSave(LINES& newSave){
    Inventory& inv = Inventory::instance();
    newSave.push_back("# HP");
    newSave.push_back(std::to_string(inv.getHealthCurrent()));
}

void SaveManager::addNewPosToSave(LINES& newSave, sf::Vector2f playerPos){
    newSave.push_back("# Position");
    newSave.push_back(std::to_string((int)playerPos.x) + " " +
                      std::to_string((int)playerPos.y));
}

void SaveManager::addOldPosToSave(LINES& newSave, LINES& oldSave){
    newSave.push_back("# Position");
    newSave.push_back(oldSave[POS_INDEX]);
}

void SaveManager::addCollectablesToSave(LINES& newSave){
    Inventory& inv = Inventory::instance();
    newSave.push_back("");
    newSave.push_back("# Flags (name persistence)");
    for(std::pair<std::string, bool> flag : inv.getCollectables())
        newSave.push_back(flag.first + " " + std::to_string(flag.second));
}

void SaveManager::addWeaponsToSave(LINES& newSave){
    Inventory& inv = Inventory::instance();
    newSave.push_back("");
    newSave.push_back("# Light Weapons");
    addWeaponToSave(newSave, inv.lightWeapon());
    newSave.push_back("");
    newSave.push_back("# Heavy Weapons");
    addWeaponToSave(newSave, inv.heavyWeapon());
}

void SaveManager::addWeaponToSave(LINES& newSave, Weapon* weapon){
    if(weapon == nullptr)
        return;
    newSave.push_back(weapon->getElementName());
    Mod* mod = weapon->getMod();
    while(mod != nullptr){
        newSave.push_back(mod->getElementName());
        mod = mod->getNextMod();
    }
}

SceneTransition SaveManager::load(){
    std::vector<std::string> save = FileHelper::readSaveFile(getSaveFileName());
    Inventory& inv = Inventory::instance();
    inv.reset();

    inv.setRunTime(std::stof(save[LOOP_TIME_INDEX]));
    inv.setHealth(std::stoi(save[HP_INDEX]));
    sf::Vector2f startPos = readPosFromLine(save[POS_INDEX]);

    unsigned int index = FLAGS_INDEX;
    while(index < save.size() && !save[index].empty())
        addCollectableToInv(save[index++]);
    index++;
    inv.getNewDialogueScene();  // Reset any dialogue scenes added

    inv.setLightWeapon(loadWeapon(save, &index, true));
    inv.setHeavyWeapon(loadWeapon(save, &index, false));

    // Decrement loop number if loading from start to counter the extra one
    // added when you load into the scene.
    if(save[SCENE_FILE_INDEX] == "start"){
        int currentLoops = std::stoi(save[LOOP_NUMBER_INDEX]);
        save[LOOP_NUMBER_INDEX] = std::to_string(currentLoops-1);
        FileHelper::writeSaveFile(getSaveFileName(), save);
    }

    return SceneTransition(save[SCENE_FILE_INDEX], sf::FloatRect(), startPos);
}

sf::Vector2f SaveManager::readPosFromLine(std::string line){
    float x;
    float y;
    std::stringstream ss(line);
    ss >> x;
    ss >> y;
    return sf::Vector2f(x, y);

}

void SaveManager::addCollectableToInv(std::string line){
    std::string name;
    bool persistence;
    std::stringstream ss(line);
    ss >> name;
    ss >> persistence;
    Inventory::instance().addCollectable(name, persistence);
}

Weapon* SaveManager::loadWeapon(LINES& lines, unsigned int* index, bool light){
    std::map<std::string,Element*> elements = Inventory::instance().getElements();
    Weapon* weapon = nullptr;
    Mod* mod = nullptr;
    while(*index < lines.size() && !lines[*index].empty()){
        Element* element = elements[lines[(*index)++]];
        element->inUse = true;
        if(weapon == nullptr){
            if(light) weapon = element->light;
            else weapon = element->heavy;
        }
        else if(mod == nullptr){
            mod = element->mod;
            weapon->setMod(mod);
        }
        else{
            mod->setNextMod(element->mod);
            mod = element->mod;
        }
    }
    (*index)++;
    if(weapon != nullptr)
        weapon->resetCooldown();
    return weapon;
}

// Static variable - easier than making a singleton for a single variable!
bool wasReset = false;

void SaveManager::reset(){
    wasReset = true;
    std::vector<std::string> lines = {
        "# Loop time",
        "0.0",
        "# Total time",
        "0.0",
        "# Loop number",
        "0",
        "# Scene file",
        "tutorial1",
        "# Level title",
        "An Abandoned Battlefield",
        "# Level background",
        "tutorial1",
        "# HP",
        "1000",
        "# Position",
        "24 336",
        "",
        "# Flags (name persistence)",
        "",
        "# Light Weapons",
        "",
        "# Heavy Weapons"
    };
    FileHelper::writeSaveFile(getSaveFileName(), lines);
}

bool SaveManager::saveWasReset(){
    bool val = wasReset;
    wasReset = false;
    return val;
}

std::string SaveManager::getSaveFileName(){
    return "save" + std::to_string(Inventory::instance().getSaveIndex());
}
