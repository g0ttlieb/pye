#pragma once

#include "../scenes/scene_transition.hpp"
#include "../weapons/weapon.hpp"

typedef std::vector<std::string> LINES;

class SaveManager {
public:
    static void save();
    static void save(sf::Vector2f playerPos);
    static void save(sf::FloatRect playerPos);
    static void saveToStart();
    static void save(std::string file, std::string name, std::string background,
            sf::Vector2f playerPos);

    static SceneTransition load();

    static void reset();
    static bool saveWasReset();

private:
    static void addTimeToSave(LINES& newSave, LINES& oldSave, std::string file);
    static void addNewLevelToSave(LINES& lines, std::string file,
            std::string name, std::string background);
    static void addOldLevelToSave(LINES& newSave, LINES& oldSave);
    static void addHpToSave(LINES& lines);
    static void addNewPosToSave(LINES& lines, sf::Vector2f pos);
    static void addOldPosToSave(LINES& newSave, LINES& oldSave);
    static void addCollectablesToSave(LINES& lines);
    static void addWeaponsToSave(LINES& lines);
    static void addWeaponToSave(LINES& lines, Weapon* weapon);

    static sf::Vector2f readPosFromLine(std::string line);
    static void addCollectableToInv(std::string line);
    static Weapon* loadWeapon(LINES& lines, unsigned int* index, bool light);

    static std::string getSaveFileName();
};
