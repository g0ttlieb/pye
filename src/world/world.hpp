# pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include "gateway.hpp"
#include "collectable.hpp"
#include "npc.hpp"

class World : public sf::Drawable, public sf::Transformable {
public:
    World(std::string name, std::string levelLayout, std::string tileset,
            std::vector<Gateway*> gateways,
            std::vector<Collectable*> collectables,
            std::vector<NPC*> npcs,
            std::vector<std::pair<sf::Vector2f, std::string>> texts);
    ~World();

    void update(float dt, sf::FloatRect& playerPos);

    bool isObstacle(float xPos, float yPos);
    bool isTerrainObstacle(float xPos, float yPos);
    bool isClearPath(sf::Vector2f pos1, sf::Vector2f pos2);

    void setObstacle(int row, int col, bool state);
private:
    int8_t getObstacle(float xPos, float yPos);
    bool verticalClearPath(sf::Vector2f origin, sf::Vector2f dest);
    bool orderedClearPath(sf::Vector2f origin, sf::Vector2f dest, float xGrad,
            float yGrad);

    std::vector<Collectable*> collectables;
    sf::Texture collectableTexture;
    sf::VertexArray collectableVertices;
    void prepCollectables();

    std::vector<NPC*> npcs;
    sf::Texture npcTexture;
    sf::VertexArray npcVertices;
    void prepNPCs();

    std::vector<Gateway*> gateways;
    sf::Texture gatewayTexture;
    sf::VertexArray gatewayVertices;
    void prepGateways();

    sf::Font font;
    sf::Text levelTitle;
    float displayLevelTitle;
    std::vector<sf::Text> textEntries;
    void setupLevelTitle(std::string name);
    void createText(sf::Vector2f pos, std::string content);

    int numberOfTextures;
    std::vector<std::vector<int8_t>> level;
    sf::Texture texture;
    sf::VertexArray textureVertices;
    void loadLevel(std::string layoutName);
    void loadTexture(std::string tileset);
    void setTextureQuad(sf::Vertex* quad, int row, int col, int8_t texture);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};
