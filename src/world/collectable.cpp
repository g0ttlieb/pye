#include "collectable.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../inventory/inventory.hpp"
#include "../inventory/save_manager.hpp"

const float WIDTH = 8.f;
const float HEIGHT = 8.f;

Collectable::Collectable(sf::Vector2f topLeft, std::string name, bool persistence):
        deactive(false),
        persistence(persistence),
        saveOnCollect(false),
        isDialogue(false),
        name(name),
        position(sf::FloatRect(topLeft.x, topLeft.y, WIDTH, HEIGHT)){
    if(name[0] == '^'){
        this->name = name.substr(1);
        saveOnCollect = true;
    }
    if(this->name.find("dialogue_") == 0)
        isDialogue = true;
}

Collectable::~Collectable(){}

void Collectable::setTexture(sf::Vertex* texturePointer){
    texture = texturePointer;
    sf::FloatRect tex(WIDTH * getTextureIndex(), 0.f, WIDTH, HEIGHT);
    SpriteHelper::setTextureQuad(texture, position, tex);

    if(Inventory::instance().hasCollectable(name))
        disable();
}

void Collectable::update(sf::FloatRect& playerPos){
    Inventory& inv = Inventory::instance();

    if(deactive && isDialogue && !inv.hasCollectable(name) &&
            !playerPos.intersects(position)){
        deactive = false;
        sf::FloatRect tex(WIDTH * getTextureIndex(), 0.f, WIDTH, HEIGHT);
        SpriteHelper::setQuadTex(texture, tex);
        return;
    }

    if(deactive)
        return;
    if(inv.hasCollectable(name)){
        disable();
        return;
    }
    if(!playerPos.intersects(position))
        return;

    if(saveOnCollect)
        SaveManager::save(sf::Vector2f(playerPos.left, playerPos.top));
    inv.addCollectable(name, persistence);
    disable();
}

void Collectable::disable(){
    sf::FloatRect tex(0.f, 0.f, 0.f, 0.f);
    SpriteHelper::setQuadTex(texture, tex);
    deactive = true;
}

int Collectable::getTextureIndex(){
    if(name == "element_steel")
        return 0;
    if(name == "element_gunpowder")
        return 1;
    if(name == "element_fire")
        return 2;
    if(name == "forest_key")
        return 8;
    if(name.find("invis") == 0)
        return 18;
    if(name.find("dialogue") == 0)
        return 19;
    if(name.find("key") == 0)
        return 20;
    if(name.find("cleared") != std::string::npos)
        return 21;
    if(name.find("tape_slot") == 0)
        return 22;
    if(name.find("time") ==  0)
        return 23;
    if(name == "player_body_loot")
        return 24;
    return -1;
}
