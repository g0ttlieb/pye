#pragma once

#include <SFML/Graphics.hpp>

class NPC {
public:
    NPC(sf::Vector2f topLeft, std::string name);
    ~NPC();

    void setTexture(sf::Vertex* texture);
private:
    std::string name;
    sf::Vertex* texture;
    sf::FloatRect position;
    int getTextureIndex();
};
