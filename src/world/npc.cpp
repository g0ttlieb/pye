#include "npc.hpp"
#include "../helpers/sprite_helper.hpp"

const float WIDTH = 16.f;
const float HEIGHT = 16.f;

NPC::NPC(sf::Vector2f topLeft, std::string name):
    name(name),
    position(sf::FloatRect(topLeft.x, topLeft.y, WIDTH, HEIGHT)){}

NPC::~NPC(){}

void NPC::setTexture(sf::Vertex* texturePointer){
    texture = texturePointer;
    sf::FloatRect tex(WIDTH * getTextureIndex(), 0.f, WIDTH, HEIGHT);
    SpriteHelper::setTextureQuad(texture, position, tex);
}

int NPC::getTextureIndex(){
    if(name == "player_body_right")
        return 0;
    if(name == "player_body_left")
        return 1;
    if(name == "obsfary_trenches_encounter_soldier")
        return 3;
    if(name == "no_mans_land_encounter_dead_obsfary")
        return 4;
    if(name == "no_mans_land_encounter_wounded_soldier")
        return 5;
    if(name == "woods_forest_exit_actrian_leader")
        return 6;
    if(name == "woods_forest_exit_actrian_idle0")
        return 7;
    if(name == "woods_forest_exit_actrian_idle1")
        return 8;
    if(name == "woods_forest_exit_actrian_idle2")
        return 9;
    if(name == "woods_dead_end_pre_slaughter_obsfary_idle0")
        return 10;
    if(name == "woods_dead_end_pre_slaughter_obsfary_idle1")
        return 11;
    if(name == "woods_dead_end_pre_slaughter_obsfary_idle2")
        return 12;
    if(name == "woods_dead_end_pre_slaughter_obsfary_idle3")
        return 13;
    if(name == "woods_dead_end_pre_slaughter_obsfary_commander")
        return 14;
    if(name == "woods_dead_end_post_slaughter_bodies0_0")
        return 15;
    if(name == "woods_dead_end_post_slaughter_bodies0_1")
        return 16;
    if(name == "woods_dead_end_post_slaughter_bodies1_0")
        return 17;
    if(name == "woods_dead_end_post_slaughter_bodies1_1")
        return 18;
    if(name == "woods_dead_end_post_slaughter_bodies2_0")
        return 19;
    if(name == "woods_dead_end_post_slaughter_bodies2_1")
        return 20;
    if(name == "woods_dead_end_post_slaughter_bodies3_0")
        return 21;
    if(name == "woods_dead_end_post_slaughter_bodies3_1")
        return 22;
    if(name == "woods_dead_end_post_slaughter_obsfary_commander")
        return 23;
    if(name == "woods_trenches_bridge_armour")
        return 24;
    return -1;
}
