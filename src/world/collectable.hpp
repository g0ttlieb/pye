#pragma once

#include <SFML/Graphics.hpp>

class Collectable {
public:
    Collectable(sf::Vector2f topLeft, std::string name, bool persistence);
    ~Collectable();

    void setTexture(sf::Vertex* texture);

    void update(sf::FloatRect& playerPos);
private:
    bool deactive;
    bool persistence;
    bool saveOnCollect;
    bool isDialogue;
    std::string name;
    sf::Vertex* texture;
    sf::FloatRect position;
    void disable();
    int getTextureIndex();
};
