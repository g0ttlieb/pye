#include <algorithm>
#include <fstream>
#include "world.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/direction.hpp"

const int LEVEL_ROWS = 45;
const int LEVEL_COLS = 40;
const int TILE_X_PX = 16;
const int TILE_Y_PX = 8;
const int MAX_X = LEVEL_COLS * TILE_X_PX;
const int MAX_Y = LEVEL_ROWS * TILE_Y_PX;

const int OBSTACLE = 0;
const int GATEWAY = -1;
const int INVISIBLE_WALL = -2;
const int OPEN_AIR = 127;
const int OBSTACLE_CUTOFF = 21; // 21 obstacles, index = [0,20]

const int TEXT_FONT_SIZE = 7;
const int TITLE_FONT_SIZE = 16;
const float TITLE_DISPLAY_TIME = 3.f;
const float TITLE_LEFT = 320.f;
const float TITLE_TOP = 120.f;

const float WIGGLE = 0.01f;

World::World(std::string name, std::string levelLayout, std::string tileset,
             std::vector<Gateway*> gateways,
             std::vector<Collectable*> collectables,
             std::vector<NPC*> npcs,
             std::vector<std::pair<sf::Vector2f, std::string>> texts):
        collectables(collectables),
        npcs(npcs),
        gateways(gateways){
    setupLevelTitle(name);
    for(auto entry : texts)
        createText(entry.first, entry.second);
    loadLevel(levelLayout);
    loadTexture(tileset);
    prepGateways();
    prepCollectables();
    prepNPCs();
    SpriteHelper::setScale(this);
}

World::~World(){
    for(auto entry : gateways)
        delete entry;
    for(auto entry : collectables)
        delete entry;
}

void World::setupLevelTitle(std::string name){
    float scale = Settings::instance().scale();
    font.loadFromFile(ResourceManager::getFont("level_title.ttf"));
    levelTitle.setFont(font);
    levelTitle.setCharacterSize(TITLE_FONT_SIZE * scale);
    levelTitle.setFillColor(sf::Color::White);
    levelTitle.setOutlineColor(sf::Color::Black);
    levelTitle.setOutlineThickness(scale);
    levelTitle.setPosition(TITLE_LEFT * scale, TITLE_TOP * scale);
    SpriteHelper::centerAlignText(levelTitle, name);
    displayLevelTitle = TITLE_DISPLAY_TIME;
}

void World::createText(sf::Vector2f pos, std::string content){
    float scale = Settings::instance().scale();
    textEntries.emplace_back();
    sf::Text& text = textEntries.back();
    text.setFont(font);
    text.setFillColor(sf::Color::White);
    text.setPosition(pos.x * scale, pos.y * scale);
    text.setCharacterSize(TEXT_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(text, content);
}

void World::loadLevel(std::string layoutName){
    level.resize(LEVEL_ROWS, std::vector<int8_t>(LEVEL_COLS, OPEN_AIR));
    numberOfTextures = 0;
    std::ifstream file(ResourceManager::getLevel(layoutName + ".txt"));
    std::string line;
    for(int row = 0; row < LEVEL_ROWS; row++){
        std::getline(file, line);
        for(int col = 0; col < LEVEL_COLS; col++){
            char current = line[col];
            if(current == '|')
                level[row][col] = INVISIBLE_WALL;
            else if(current != '-'){
                level[row][col] = current - 'a';
                numberOfTextures++;
            }
        }
    }
    file.close();
}

void World::loadTexture(std::string tileset){
    texture.loadFromFile(ResourceManager::getVisual(tileset + ".png"));
    textureVertices.setPrimitiveType(sf::Quads);
    textureVertices.resize(numberOfTextures*4);
    int currentTexture = 0;
    for(int row = 0; row < LEVEL_ROWS; row++){
        for(int col = 0; col < LEVEL_COLS; col++){
            int8_t current = level[row][col];
            if(current == OPEN_AIR || current == INVISIBLE_WALL)
                continue;
            sf::Vertex* quad = &textureVertices[currentTexture*4];
            setTextureQuad(quad, row, col, current);
            currentTexture++;
        }
    }
}

void World::setTextureQuad(sf::Vertex* quad, int row, int col, int8_t texture){
    sf::FloatRect pos(col * TILE_X_PX, row * TILE_Y_PX, TILE_X_PX, TILE_Y_PX);
    sf::FloatRect tex(texture * TILE_X_PX, 0.f, TILE_X_PX, TILE_Y_PX);
    SpriteHelper::setTextureQuad(quad, pos, tex);
}

void World::prepGateways(){
    gatewayTexture.loadFromFile(ResourceManager::getVisual("gateway.png"));
    gatewayVertices.setPrimitiveType(sf::Quads);
    gatewayVertices.resize(gateways.size()*4);
    int i = 0;
    for(auto entry : gateways){
        sf::Vertex* quad = &gatewayVertices[i*4];
        i++;
        entry->calculateSize(*this);
        entry->setTexture(quad);
        if(entry->shouldToggleState())
            entry->toggleStateImmediately(*this);
    }
}

void World::prepCollectables(){
    collectableTexture.loadFromFile(
            ResourceManager::getVisual("collectables.png"));
    collectableVertices.setPrimitiveType(sf::Quads);
    collectableVertices.resize(collectables.size()*4);
    int i = 0;
    for(auto entry : collectables){
        sf::Vertex* quad = &collectableVertices[i*4];
        i++;
        entry->setTexture(quad);
    }
}

void World::prepNPCs(){
    npcTexture.loadFromFile(ResourceManager::getVisual("npcs.png"));
    npcVertices.setPrimitiveType(sf::Quads);
    npcVertices.resize(npcs.size()*4);
    int i = 0;
    for(auto entry : npcs){
        sf::Vertex* quad = &npcVertices[i*4];
        i++;
        entry->setTexture(quad);
    }
}

void World::update(float dt, sf::FloatRect& playerPos){
    for(auto entry : collectables)
        entry->update(playerPos);
    for(auto entry : gateways){
        if(entry->shouldToggleState())
            entry->startToggleAnimation();
        entry->update(dt);
        if(entry->isFinishedAnimation())
            entry->updateWorldObstacles(*this);
    }
    if(displayLevelTitle > 0.f)
        displayLevelTitle -= dt;
}

bool World::isObstacle(float xPos, float yPos){
    return(getObstacle(xPos, yPos) < OBSTACLE_CUTOFF);
}

bool World::isTerrainObstacle(float xPos, float yPos){
    int8_t obstacle = getObstacle(xPos, yPos);
    return(obstacle != GATEWAY && obstacle < OBSTACLE_CUTOFF);
}

int8_t World::getObstacle(float xPos, float yPos){
    if(xPos < 0 || yPos < 0 || xPos >= MAX_X || yPos >= MAX_Y){
        if(xPos < -TILE_X_PX || yPos < -TILE_Y_PX ||
                xPos >= MAX_X + TILE_X_PX || yPos >= MAX_Y + TILE_Y_PX)
            return OBSTACLE_CUTOFF;
        return OBSTACLE;
    }
    int row = (int) yPos / TILE_Y_PX;
    int col = (int) xPos / TILE_X_PX;
    return level[row][col];
}

bool World::isClearPath(sf::Vector2f pos1, sf::Vector2f pos2){
    if(pos1.x == pos2.x)
        return verticalClearPath(pos1, pos2);

    sf::Vector2f origin = pos1;
    sf::Vector2f dest = pos2;
    if(pos1.x > pos2.x){
        origin = pos2;
        dest = pos1;
    }

    if(origin.y == dest.y)
        return orderedClearPath(origin, dest, TILE_X_PX, 0);

    float gradient = (dest.y - origin.y) / (dest.x - origin.x);
    bool horizPass = orderedClearPath(origin, dest, TILE_X_PX, gradient*TILE_X_PX);
    if(horizPass == false)
        return false;
    Direction dir = makeLeftDir(gradient < 0);
    return orderedClearPath(origin, dest, dir * TILE_Y_PX/gradient,
            dir * TILE_Y_PX);
}

bool World::verticalClearPath(sf::Vector2f origin, sf::Vector2f dest){
    float xPos = origin.x;
    float yPos = origin.y;
    float yTarget = dest.y;
    if(yPos > yTarget){
        yPos = dest.y;
        yTarget = origin.y;
    }
    float gradient = TILE_Y_PX / 2;
    while(yPos < yTarget){
        if(isObstacle(xPos, yPos))
            return false;
        yPos += gradient;
    }
    return !isObstacle(xPos, yTarget);
}

bool World::orderedClearPath(sf::Vector2f origin, sf::Vector2f dest, float xGrad,
        float yGrad){
    float xPos = origin.x;
    float yPos = origin.y;
    if(isObstacle(xPos, yPos))
        return false;
    xPos += xGrad * WIGGLE; // WIGGLE forward to offset from grid
    yPos += yGrad * WIGGLE;
    while(xPos < dest.x){
        if(isObstacle(xPos, yPos))
            return false;
        xPos += xGrad;
        yPos += yGrad;
    }
    return !isObstacle(dest.x, dest.y);
}

void World::setObstacle(int row, int col, bool state){
    int8_t val = state ? GATEWAY : OPEN_AIR;
    level[row][col] = val;
}

void World::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    states.texture = &gatewayTexture;
    target.draw(gatewayVertices, states);
    states.texture = &collectableTexture;
    target.draw(collectableVertices, states);
    states.texture = &npcTexture;
    target.draw(npcVertices, states);
    states.texture = &texture;
    target.draw(textureVertices, states);

    for(auto text: textEntries)
        target.draw(text);
    if(displayLevelTitle > 0.f)
        target.draw(levelTitle);
}
