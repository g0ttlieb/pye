#pragma once

#include <SFML/Graphics.hpp>
#include <string>

// Prevent cyclical inclusion
class World;

class Gateway{
public:
    Gateway(sf::Vector2f topLeft, bool state, std::string control);
    ~Gateway();

    sf::Vector2f getTopLeft();

    void calculateSize(World& world);
    void setTexture(sf::Vertex* texture);

    bool shouldToggleState();
    void toggleStateImmediately(World& world);
    void startToggleAnimation();
    void retoggle();
    void reallowToggle();

    void update(float dt);
    bool isFinishedAnimation();
    void updateWorldObstacles(World& world);
private:
    bool startedOpen;
    std::string control;
    sf::FloatRect closedPos;
    sf::FloatRect currentPos;

    bool togglingState;
    bool alreadyToggled;

    sf::Vertex* texture;
};
