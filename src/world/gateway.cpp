#include "gateway.hpp"
#include "world.hpp"
#include "../inventory/inventory.hpp"
#include "../helpers/sprite_helper.hpp"

const float SPEED = 80.f;

const int UP = -1;
const int DOWN = 1;
const float TILE_X_PX = 16.f;
const float TILE_Y_PX = 8.f;

const float TEXTURE_HEIGHT = 160.f;

Gateway::Gateway(sf::Vector2f topLeft, bool state, std::string control):
    startedOpen(state),
    control(control),
    closedPos(sf::FloatRect(topLeft.x, topLeft.y, TILE_X_PX, 0.f)),
    togglingState(false),
    alreadyToggled(false){
}

Gateway::~Gateway(){}

sf::Vector2f Gateway::getTopLeft(){
    return sf::Vector2f(closedPos.left, closedPos.top);
}

void Gateway::calculateSize(World& world){
    while(!world.isObstacle(closedPos.left, closedPos.top + closedPos.height))
        closedPos.height += TILE_Y_PX;
    currentPos = closedPos;
    if(startedOpen)
        currentPos.top -= closedPos.height;
    updateWorldObstacles(world);
}

void Gateway::setTexture(sf::Vertex* texturePointer){
    texture = texturePointer;
    sf::FloatRect tex(0.f, TEXTURE_HEIGHT - closedPos.height,
            closedPos.width, closedPos.height);
    SpriteHelper::setTextureQuad(texture, currentPos, tex);
}

bool Gateway::shouldToggleState(){
    if(alreadyToggled)
        return false;
    return Inventory::instance().hasCollectable(control);
}

void Gateway::toggleStateImmediately(World& world){
    alreadyToggled = true;
    sf::FloatRect pos(closedPos);
    if(!startedOpen)
        pos.top -= pos.height;
    SpriteHelper::setQuadPos(texture, pos);
    updateWorldObstacles(world);
}

void Gateway::startToggleAnimation(){
    togglingState = true;
    alreadyToggled = true;
}

void Gateway::retoggle(){
    startedOpen = !startedOpen;
    startToggleAnimation();
}

void Gateway::reallowToggle(){
    startedOpen = !startedOpen;
    alreadyToggled = false;
}

void Gateway::update(float dt){
    if(!togglingState)
        return;
    int dir = startedOpen ? DOWN : UP;
    currentPos.top += dir * SPEED * dt;
    if(startedOpen && currentPos.top >= closedPos.top){
        currentPos.top = closedPos.top;
        togglingState = false;
    }
    else if(!startedOpen && currentPos.top <= closedPos.top - closedPos.height){
        currentPos.top = closedPos.top - closedPos.height;
        togglingState = false;
    }
    SpriteHelper::setQuadPos(texture, currentPos);
}

bool Gateway::isFinishedAnimation(){
    return alreadyToggled && !togglingState;
}

void Gateway::updateWorldObstacles(World& world){
    bool currentState = !startedOpen;
    if(alreadyToggled)
        currentState = !currentState;
    int col = (int) closedPos.left / TILE_X_PX;
    for(float yPos = closedPos.top; yPos < closedPos.top + closedPos.height;
            yPos += TILE_Y_PX){
        int row = (int) yPos / TILE_Y_PX;
        world.setObstacle(row, col, currentState);
    }
}
