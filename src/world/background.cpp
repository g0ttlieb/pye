#include "background.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"

const float EXTRA_SCALE = 4.f;

Background::Background(std::string name) {
    texture.loadFromFile(ResourceManager::getVisual(name + "-background.png"));
    sprite.setTexture(texture);
    sprite.setPosition(0.f, 0.f);
    float scale;
    if(name == "debug")
        scale = 1;
    else
        scale = Settings::instance().scale() * EXTRA_SCALE;
    setScale(scale, scale);
}

void Background::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    target.draw(sprite, states);
}
