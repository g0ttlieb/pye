# pragma once

#include <SFML/Graphics.hpp>

class Background : public sf::Drawable, public sf::Transformable {
public:
    Background(std::string name);
private:
    sf::Texture texture;
    sf::Sprite sprite;
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};
