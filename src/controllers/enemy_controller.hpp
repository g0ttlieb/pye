#pragma once

#include "../characters/enemy.hpp"

class EnemyController {
public:
    static void update(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
};
