#include "dialogue_controller.hpp"

const int NO_SELECTION = 0;

DialogueController::DialogueController(): selection(NO_SELECTION){
}

DialogueController::~DialogueController(){}

void DialogueController::handleInput(sf::Event& event){
    if(event.type == sf::Event::KeyPressed){
        int key = event.key.code - sf::Keyboard::Num0;
        if(key >= 1 && key <= 9)
            selection = key;
    }
}

bool DialogueController::selectionWasMade(){
    return selection > NO_SELECTION;
}

int DialogueController::getThenResetSelection(){
    int temp = selection;
    selection = NO_SELECTION;
    return temp;
}
