# pragma once

#include "controller.hpp"

class MenuController : public Controller {
public:
    MenuController();
    virtual ~MenuController();
    virtual void handleInput(sf::Event& event);

    std::vector<sf::Vector2i> getAndResetClicks();
private:
    std::vector<sf::Vector2i> clicks;
};
