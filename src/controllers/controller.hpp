# pragma once

#include <SFML/Window.hpp>

class Controller {
public:
    virtual ~Controller(){}
    virtual void handleInput(sf::Event& event) = 0;
private:
};
