# pragma once

#include "controller.hpp"

class DialogueController : public Controller {
public:
    DialogueController();
    virtual ~DialogueController();
    virtual void handleInput(sf::Event& event);

    bool selectionWasMade();
    int getThenResetSelection();
private:
    int selection;
};
