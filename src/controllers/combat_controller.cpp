#include "combat_controller.hpp"

CombatController::CombatController(Player* player): player(player){
    bool leftPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
    bool rightPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::D);
    if(!(leftPressed || rightPressed))
        return;
    if(leftPressed && !rightPressed)
        player->moveLeft();
    else if(!leftPressed  && rightPressed)
        player->moveRight();
    else{
        if(player->getDirection() == Direction::LEFT)
            player->moveLeft();
        else
            player->moveRight();
    }
}

CombatController::~CombatController(){}

void CombatController::handleInput(sf::Event& event){
    if(player->isWarping() || player->isDying())
        return;
    if(event.type == sf::Event::KeyPressed)
        handleKeyPress(event);
    else if(event.type == sf::Event::KeyReleased)
        handleKeyRelease(event);
    else if(event.type == sf::Event::MouseButtonPressed)
        handleButtonPress(event);
}

void CombatController::handleKeyPress(sf::Event& event){
    switch(event.key.code){
        case sf::Keyboard::A :
            player->moveLeft();
            break;
        case sf::Keyboard::D :
            player->moveRight();
            break;
        case sf::Keyboard::Space :
            player->jump();
            break;
        case sf::Keyboard::LShift :
            player->dash();
            break;
        default:
            break;
    }
}

void CombatController::handleKeyRelease(sf::Event& event){
    switch(event.key.code){
        case sf::Keyboard::A :
            player->stopMovingLeft();
            break;
        case sf::Keyboard::D :
            player->stopMovingRight();
            break;
        default:
            break;
    }
}

void CombatController::handleButtonPress(sf::Event& event){
    switch(event.mouseButton.button){
        case sf::Mouse::Button::Left :
            player->lightAttack();
            break;
        case sf::Mouse::Button::Right :
            player->heavyAttack();
            break;
        default:
            break;
    }
}
