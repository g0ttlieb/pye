# pragma once

#include "controller.hpp"
#include "../characters/player.hpp"

class CombatController : public Controller {
public:
    CombatController(Player* player);
    virtual ~CombatController();
    virtual void handleInput(sf::Event& event);
private:
    Player* player;
    void handleKeyPress(sf::Event& event);
    void handleKeyRelease(sf::Event& event);
    void handleButtonPress(sf::Event& event);
};
