#include "menu_controller.hpp"

MenuController::MenuController(){
}

MenuController::~MenuController(){
}

void MenuController::handleInput(sf::Event& event){
    if(event.type == sf::Event::MouseButtonReleased){
        if(event.mouseButton.button == sf::Mouse::Button::Left)
            clicks.push_back(sf::Vector2i(event.mouseButton.x,
                                          event.mouseButton.y));
    }
}

std::vector<sf::Vector2i> MenuController::getAndResetClicks(){
    std::vector<sf::Vector2i> temp(clicks);
    clicks.clear();
    return temp;
}
