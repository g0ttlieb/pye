#include "enemy_controller.hpp"
#include "../helpers/ai_helpers.hpp"

void EnemyController::update(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    if(enemy.areDead() || enemy.areStaggered())
        return;

    std::map<std::string, float>& state = enemy.getState();
    if(state["activeCooldown"] > 0)
        state["activeCooldown"] -= dt;

    if(enemy.areAttacking())
        return;

    if(AiHelpers::playerInSight(enemy, world, target))
        enemy.activateAggression();

    if(state["activeCooldown"] <= 0.f)
        enemy.passiveUpdateAI(pathPlanner, world, target, dt);
    else
        enemy.activeUpdateAI(pathPlanner, world, target, dt);
}
