#include "boss_chase.hpp"
#include "../helpers/ai_helpers.hpp"

const float TILE_Y = 8.f;

void BossChase::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) dt;

    Boss& boss = (Boss&) enemy;
    boss.run();
    boss.getState()["BOSS_AGROED"] = true;
    int viableAttacks = attacksInRange(boss, world, target);
    if(viableAttacks > 0 && boss.attack())
        return;

    auto& state = enemy.getState();
    bool overlapping = boss.getPos().intersects(target);
    bool resetRunningAway = true;
    if(viableAttacks == boss.numberOfAttacks() || !pathPlanner.pathToPlayer(enemy))
        enemy.stopMoving();
    else if((overlapping || boss.isStationary()) && viableAttacks == 0){
        resetRunningAway = false;
        Direction dir = (Direction) state["BOSS_RUNNING_AWAY_DIR"];
        if(state["BOSS_RUNNING_AWAY"]){
            if(enemy.getPos().left == state["BOSS_RUNNING_AWAY_X"])
                dir = !dir;
        }
        else{
            dir = makeRightDir(AiHelpers::getRandomInt(0,1));
            state["BOSS_RUNNING_AWAY"] = true;
        }
        AiHelpers::moveInDirection(boss, dir);
        state["BOSS_RUNNING_AWAY_DIR"] = (float) dir;
        state["BOSS_RUNNING_AWAY_X"] = boss.getPos().left;
    }
    else if(boss.isStationary()){
        Direction dir = makeDirection(target.left - boss.getPos().left);
        AiHelpers::moveInDirection(boss, dir);
        boss.stopMoving();
    }

    if(resetRunningAway)
        state["BOSS_RUNNING_AWAY"] = false;
}

int BossChase::attacksInRange(Boss& boss, World& world, sf::FloatRect& target){
    if(boss.isMidair())
        return 0;

    std::vector<BossAttack> attacks = boss.getAttackOptions();
    std::vector<int> attacksInRange;
    for(unsigned int i = 0; i < attacks.size(); i++){
        sf::FloatRect range = attacks[i].hitbox;
        if(!range.intersects(target))
            continue;
        if(attacks[i].isMelee){
            attacksInRange.push_back(i);
            continue;
        }
        bool inRange = true;
        for(float y = 0; y <= range.height; y += std::min(range.height-y, TILE_Y)){
            float rangeCheckY = range.top + y;
            sf::Vector2f targetPos(target.left, rangeCheckY);
            sf::Vector2f attackPos(range.left, rangeCheckY);
            if(boss.getDirection() == Direction::RIGHT)
                inRange &= world.isClearPath(attackPos, targetPos);
            else
                inRange &= world.isClearPath(targetPos, attackPos);
            if(!inRange)
                break;
            if(y == range.height) break;
        }
        if(inRange)
            attacksInRange.push_back(i);
    }

    if(!attacksInRange.empty()){
        int randomAttack = AiHelpers::getRandomInt(0, attacksInRange.size());
        boss.setAttack(attacksInRange[randomAttack]);
    }
    return attacksInRange.size();;
}

void BossChase::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
    if(enemy.getState()["BOSS_AGROED"])
        enemy.activateAggression();
}
