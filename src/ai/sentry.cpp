#include "sentry.hpp"

void Sentry::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
    enemy.attack();
}
void Sentry::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) enemy;
    (void) world;
    (void) target;
    (void) dt;
}
