#pragma once

#include "path_planner.hpp"
#include "../characters/boss.hpp"

struct BossChase {
    static void Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
    static void Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
private:
    static int attacksInRange(Boss& boss, World& world, sf::FloatRect& target);
};
