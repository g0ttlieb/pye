#include "stationary.hpp"

void Stationary::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
    enemy.stopMoving();
}

void Stationary::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) enemy;
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
}
