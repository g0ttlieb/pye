#pragma once

#include <SFML/Graphics.hpp>
#include "../helpers/direction.hpp"

struct Path;

struct Platform {
    sf::Vector2f left;
    sf::Vector2f right;
    std::vector<Path*> paths;
    sf::Vector2f getSide(Direction dir);
};

enum class PathType{
    VERTICAL, ANGLED, INLINE
};

struct Path {
    Path(Platform*, Platform*, sf::Vector2f, sf::Vector2f, PathType);

    PathType type;
    int vertDistance;
    int horizDistance;
    float totalDistance;
    float getSignedVertical(Platform* platform);

    Platform* sourcePlatform;
    Platform* destinationPlatform;
    Platform* getOtherPlatform(Platform* platform);

    sf::Vector2f sourcePos;
    sf::Vector2f destinationPos;
    sf::Vector2f getSourcePos(Platform* platform);
    sf::Vector2f getDestinationPos(Platform* platform);
};
