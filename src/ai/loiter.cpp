#include "loiter.hpp"
#include "../helpers/ai_helpers.hpp"

const float WALK_SPEED = 0.5f;
const float MIN_STOP_TIME = 2.f;
const float MAX_STOP_TIME = 4.f;
const float MIN_WALK_TIME = 1.f;
const float MAX_WALK_TIME = 3.f;

void Loiter::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) target;

    std::map<std::string, float>& state = enemy.getState();
    if(state["loiterStop"] > 0.f)
        state["loiterStop"] -= dt;
    else if(enemy.isStationary()){
        enemy.walk(WALK_SPEED);
        enemy.move();
        int moveDir = state["loiterMoveDir"];
        if(moveDir != 0)
            enemy.setDirection((Direction) moveDir);
        state["loiterWalk"] = AiHelpers::getRandom(MIN_WALK_TIME, MAX_WALK_TIME);
    }
    else if(AiHelpers::isBlockedByWall(enemy, world)){
        enemy.turnAround();
        stopAndSetNextDir(enemy, enemy.getDirection());
    }
    else if(AiHelpers::isAboutToDrop(enemy, world))
        stopAndSetNextDir(enemy, !enemy.getDirection());
    else if(state["loiterWalk"] < 0.f){
        Direction newDir = (AiHelpers::getRandom(-1,1) < 0.f) ? Direction::LEFT
                                                              : Direction::RIGHT;
        stopAndSetNextDir(enemy, newDir);
    }
    else
        state["loiterWalk"] -= dt;
}

void Loiter::stopAndSetNextDir(Enemy& enemy, Direction dir){
    enemy.stopMoving();
    std::map<std::string, float>& state = enemy.getState();
    state["loiterMoveDir"] = (int) dir;
    state["loiterStop"] = AiHelpers::getRandom(MIN_STOP_TIME, MAX_STOP_TIME);
}

void Loiter::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
    enemy.getState()["loiterStop"] = MIN_STOP_TIME;
}
