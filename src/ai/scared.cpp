#include <cmath>
#include "scared.hpp"
#include "../helpers/ai_helpers.hpp"

const int FLEE_DISTANCE = 32.f;
const int SAFE_DISTANCE = 48.f;
const int VERY_SEPERATED = 999.f;
const int Y_RANGE = 16.f;
const float WIGGLE = 0.001f;

enum State {
    FLEEING, SAFE, FIGHT
};

void Scared::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) dt;
    if(enemy.areAttacking())
        return;
    State fleeing = (State) enemy.getState()["FLEEING"];
    if(fleeing == FLEEING)
        fleeingUpdate(enemy, pathPlanner, world, target);
    else if(fleeing == SAFE)
        safeUpdate(enemy, pathPlanner, world, target);
    else if(fleeing == FIGHT)
        fightUpdate(enemy, pathPlanner, world, target);
}

void Scared::fleeingUpdate(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target){
    if(getVisibleDistance(enemy.getPos(), target, world) > SAFE_DISTANCE){
        enemy.getState()["FLEEING"] = SAFE;
        return;
    }

    sf::FloatRect& pos = enemy.getPos();
    Platform* platform = pathPlanner.getPlatformForPos(pos);
    if(platform == nullptr)
        return;

    Direction dir = makeLeftDir(pos.left < target.left);
    sf::Vector2f aimPos = platform->getSide(dir);
    for(unsigned int i = 0; i < platform->paths.size(); i++){
        Path* current = platform->paths[i];
        if(current->getSourcePos(platform) == aimPos
                && current->type == PathType::VERTICAL){
            aimPos = sf::Vector2f(current->getDestinationPos(platform));
            break;
        }
    }

    enemy.run();
    enemy.move();
    pathPlanner.pathToPos(enemy, aimPos);

    if(AiHelpers::isBlockedByWall(enemy, world)){
        enemy.getState()["FLEEING"] = FIGHT;
        enemy.stopMoving();
        enemy.turnAround();
    }
}

void Scared::safeUpdate(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target){
    sf::FloatRect& pos = enemy.getPos();
    if(getVisibleDistance(pos, target, world) < FLEE_DISTANCE)
        enemy.getState()["FLEEING"] = FLEEING;
    else if(AiHelpers::canAttack(enemy, world, target)){
        enemy.attack();
        enemy.stopMoving();
    }
    else if(getSeparationDistance(pos, target) > SAFE_DISTANCE){
        enemy.run();
        pathPlanner.pathToPlayer(enemy);
        if(AiHelpers::isBlockedByWall(enemy, world) ||
                AiHelpers::isAboutToDrop(enemy, world))
            enemy.stopMoving();
    }
    else
        enemy.stopMoving();
}

void Scared::fightUpdate(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target){
    (void) pathPlanner;
    if(getVisibleDistance(enemy.getPos(), target, world) > SAFE_DISTANCE)
        enemy.getState()["FLEEING"] = SAFE;
    Direction dir = makeRightDir(enemy.getPos().left < target.left);
    AiHelpers::moveInDirection(enemy, dir); // Face the correct direction
    enemy.stopMoving();
    if(AiHelpers::canAttack(enemy, world, target))
        enemy.attack();
}

float Scared::getSeparationDistance(sf::FloatRect& enemy, sf::FloatRect& target){
    float xDif = std::abs(enemy.left - target.left);
    float yDif = std::abs(enemy.top - target.top);
    return std::hypot(xDif, yDif);
}

float Scared::getVisibleDistance(sf::FloatRect& enemy, sf::FloatRect& target,
        World& world){
    if(!world.isClearPath(
                sf::Vector2f(enemy.left, enemy.top + enemy.height/2),
                sf::Vector2f(target.left, target.top + target.height/2)))
        return VERY_SEPERATED;
    return getSeparationDistance(enemy, target);
}

void Scared::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
    enemy.getState()["FLEEING"] = SAFE;
}
