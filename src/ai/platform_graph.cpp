#include <cmath>
#include "platform_graph.hpp"

sf::Vector2f Platform::getSide(Direction dir){
    if(dir == Direction::LEFT)
        return left;
    return right;
}

Path::Path(Platform* platform1, Platform* platform2,
           sf::Vector2f pos1, sf::Vector2f pos2, PathType type):
        type(type){
    sourcePlatform = platform1;
    destinationPlatform = platform2;
    sourcePos = pos1;
    destinationPos = pos2;
    vertDistance = std::abs(pos1.y - pos2.y);
    horizDistance = std::abs(pos1.x - pos2.x);
    totalDistance = std::hypot(vertDistance, horizDistance);
}

float Path::getSignedVertical(Platform* platform){
    if(platform == sourcePlatform)
        return (sourcePos.y - destinationPos.y);
    return (destinationPos.y - sourcePos.y);
}

Platform* Path::getOtherPlatform(Platform* platform){
    if(sourcePlatform == platform)
        return destinationPlatform;
    return sourcePlatform;
}

sf::Vector2f Path::getSourcePos(Platform* platform){
    if(sourcePlatform == platform)
        return sourcePos;
    return destinationPos;
}

sf::Vector2f Path::getDestinationPos(Platform* platform){
    if(sourcePlatform == platform)
        return destinationPos;
    return sourcePos;
}
