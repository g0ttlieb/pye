#include "patrol.hpp"
#include "../helpers/ai_helpers.hpp"

const float WALK_SPEED = 0.5f;
const float PATROL_PAUSE_TIME = 3.f;

void Patrol::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) target;
    std::map<std::string, float>& state = enemy.getState();
    if(state["pause"] > 0.f){
        state["pause"] -= dt;
        return;
    }

    enemy.walk(WALK_SPEED);
    if(enemy.isStationary()){
        int moveDir = state["moveDir"];
        enemy.move();
        if(moveDir != 0)
            enemy.setDirection((Direction) moveDir);
    }
    else if(AiHelpers::isBlockedByWall(enemy, world)){
        enemy.turnAround();
        enemy.stopMoving();
        state["moveDir"] = (int) enemy.getDirection();
        state["pause"] = PATROL_PAUSE_TIME;
    }
    else if(AiHelpers::isAboutToDrop(enemy, world)){
        enemy.stopMoving();
        state["moveDir"] = (int) !enemy.getDirection();
        state["pause"] = PATROL_PAUSE_TIME;
    }
}

void Patrol::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) pathPlanner;
    (void) world;
    (void) target;
    std::map<std::string, float>& state = enemy.getState();
    if(state["pause"] > 0)
        state["pause"] -= dt;
}
