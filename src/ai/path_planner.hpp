#pragma once

#include <vector>
#include "platform_graph.hpp"
#include "../characters/enemy.hpp"
#include "../characters/player.hpp"

class PathPlanner{
public:
    PathPlanner(World& world);
    ~PathPlanner();

    void updatePlayerPos(Player& player);
    bool pathToPlayer(Enemy& enemy);

    bool pathToPos(Enemy& enemy, sf::Vector2f target);

    Platform* getPlatformForPos(float xPos, float yPos);
    Platform* getPlatformForPos(sf::Vector2f pos);
    Platform* getPlatformForPos(sf::FloatRect& pos);
private:

    void initTilePlatformMap();
    void createPlatforms();
    void createPaths();
    void addVerticalAndInlinePaths(Platform* platform, Direction dir);
    void addAngledPaths(Platform* current, Platform* other);
    bool viablePath(sf::Vector2f src, sf::Vector2f dest);
    void addPathToPlatforms(Platform*, Platform*, Path*);

    World& world;
    sf::Vector2f playerPos;
    std::vector<Path*> paths;
    std::vector<Platform*> platforms;
    std::vector<std::vector<Platform*>> tilePlatformMap;

    Enemy* enemy;
    sf::Vector2f currentPos;
    Platform* currentPlatform;

    void setupEnemy(Enemy& enemy);
    bool onSamePlatform(sf::Vector2f target);

    Path* getCachedPath(sf::Vector2f target);
    void cachePath(Path* edge, sf::Vector2f target);

    Path* findBestPath(sf::Vector2f target);
    bool athleticallyViable(float horiz, float vert);
    bool athleticallyViable(float xVel, float yVel, float horiz, float vert);

    bool moveToPath(Path* edge);
    void startPath(Path* edge);
    bool needToJump(Path* edge);
    bool moveTowards(sf::Vector2f target, bool backfoot=true);
    void moveNearTarget(sf::Vector2f target);
    void followPath();

    std::pair<Platform*, sf::Vector2f> getPlatformAndPos(sf::FloatRect& pos);
};
