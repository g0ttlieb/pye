#include "chase.hpp"
#include "../helpers/ai_helpers.hpp"

void Chase::Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) dt;
    enemy.run();
    if(AiHelpers::canAttack(enemy, world, target)){
        enemy.stopMoving();
        enemy.attack();
    }
    else if(!pathPlanner.pathToPlayer(enemy)){
        enemy.stopMoving();
        enemy.deactivateAggression();
    }
}

void Chase::Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    (void) enemy;
    (void) pathPlanner;
    (void) world;
    (void) target;
    (void) dt;
}
