#include <queue>
#include <cmath>
#include "path_planner.hpp"
#include "../helpers/ai_helpers.hpp"

const int LEVEL_ROWS = 45;
const int LEVEL_COLS = 40;
const int TILE_X_PX = 16;
const int TILE_Y_PX = 8;
const int HALF_TILE_Y = 4;
const int MAX_X = LEVEL_COLS * TILE_X_PX;
const int MAX_Y = LEVEL_ROWS * TILE_Y_PX;

const int VERTICAL_PATH_OFFSET = 8;
const float RNG_MIN = 4.f;
const float RNG_RANGE = 8.f;
const float JUMP_GRADIENT = 1.f;
const float INLINE_PATHING = 8.f;
const float CLOSE_ENOUGH = 2.00f;
const float MOVE_NEAR_OFFSET = 2.f;

const float WIGGLE = 0.01f;
const float GRAVITY = 350.f;
const float FAT = 12.f;

const int PLAYER_ALTITUDE_CUTOFF = 32;
const float MAX_X_VEL = 75.f;
const float MAX_Y_VEL = 200.f;

enum Locked {
    UNLOCKED, WAITING, LOCKED
};

const int NO_PATH_FOUND = -1;

// A NOTE ABOUT PHYICS!
//
// I would love to have a more detailed simulation of a jump's motion, considering
// if other terrain will be collided with and how this effects the viability of the
// path. However at this time, it doesn't feel like a sufficient priority or
// viable in any satisfactory way.
//
// It would be nice to have this included at the graph generation step, however
// a great of deal of variety would need to be considered. In addition to looking
// at both directions of the jump, the key problem is what velocity to use.
//      - Too high and you might collide early, breaking the jump
//      - Too low and you might not be able to make the jump at all
// As such, most of it would need to be done at runtime, only eliminating extreme
// outliers at that stage.
//
// As such, the best bet seems to be to do it at query time, eliminating
// paths that are not viable. This means we only need to consider the direction
// and velocity that is relevant, simplfiying and ensuring the accuracy of the
// process. The key thing standing in the way is how this could impact the graph
// itself. We might leave enemies stuck somewhere as know they cannot make the
// jump / find a path, even if it might not be immediately obvious to the player.
// A partial solution to this is to consider the final endpoint of the jump as a
// new option, rather than simply ignoring it. Realistically we have the time ...
//
// With regards to implementation, we would only need to consider the upward path
// for collisions, safely assuming the descent is safe. Similar to checking if a
// path is clear, running a scan based on x and y ticks seems like a good option.
// If a collision does occur, we simply set y vel to 0 and start the descent,
// experiencing a lower ascent time than otherwise.
//
// Another thought is that we might be able to partically pre-process this. There
// might always be a problem platform that we could store in the edge so we
// can directly query it, without needing to check every tile in the trajectory.
// That said, there is no gurantee that the platforms have a consistent underbelly.
// Checking for null platforms could be the way to go for this though.

PathPlanner::PathPlanner(World& world): world(world){
    initTilePlatformMap();
    createPlatforms();
    createPaths();
}

void PathPlanner::initTilePlatformMap(){
    tilePlatformMap.resize(LEVEL_ROWS+1,
            std::vector<Platform*>(LEVEL_COLS,nullptr));
}

void PathPlanner::createPlatforms(){
    Platform* current = nullptr;
    // Include 46th row to allow for standing on exits
    for(int row = LEVEL_ROWS; row >= 0; row--){
        for(int col = 0; col <= LEVEL_COLS; col++){
            int xPos = col * TILE_X_PX;
            int yPos = row * TILE_Y_PX;
            bool above = world.isTerrainObstacle(xPos, yPos - WIGGLE);
            bool below = world.isTerrainObstacle(xPos, yPos + WIGGLE);
            bool isPlatform = !above && below;
            if(isPlatform && current == nullptr){
                current = new Platform();
                current->left = sf::Vector2f(xPos, yPos);
            }
            else if(!isPlatform && current != nullptr){
                current->right = sf::Vector2f(xPos - WIGGLE, yPos);
                platforms.push_back(current);
                current = nullptr;
            }

            if(col == LEVEL_COLS)   // Only process 41th col to end platforms
                break;
            if(current != nullptr)
                tilePlatformMap[row][col] = current;
            else if(row == LEVEL_ROWS || (above && below))
                tilePlatformMap[row][col] = nullptr;
            else
                tilePlatformMap[row][col] = tilePlatformMap[row+1][col];
        }
    }
}

void PathPlanner::createPaths(){
    for(Platform* current : platforms){
        addVerticalAndInlinePaths(current, Direction::LEFT);
        addVerticalAndInlinePaths(current, Direction::RIGHT);
    }
    for(unsigned int current = 0; current < platforms.size(); current++){
        for(unsigned int other = current + 1; other < platforms.size(); other++)
            addAngledPaths(platforms[current], platforms[other]);
    }
}

void PathPlanner::addVerticalAndInlinePaths(Platform* platform, Direction dir){
    sf::Vector2f pos = platform->getSide(dir);
    int xPos = pos.x + (dir * VERTICAL_PATH_OFFSET);
    Platform* vertical = getPlatformForPos(xPos, pos.y);
    if(vertical == nullptr)
        return;
    sf::Vector2f otherPos = sf::Vector2f(xPos, vertical->left.y);
    Path* edge = new Path(platform, vertical, pos, otherPos, PathType::VERTICAL);
    addPathToPlatforms(platform, vertical, edge);

    Platform* linedUp = getPlatformForPos(xPos, pos.y + TILE_Y_PX);
    if(linedUp == nullptr || linedUp->left.y >= vertical->left.y)
        return;
    otherPos = sf::Vector2f(pos.x, linedUp->left.y);
    edge = new Path(platform, linedUp, pos, otherPos, PathType::INLINE);
    addPathToPlatforms(platform, linedUp, edge);
}

void PathPlanner::addAngledPaths(Platform* current, Platform* other){
    for(Path* edge : current->paths){
        if(edge->getOtherPlatform(current) == other)
            return;
    }

    sf::Vector2f currentLeft = current->left;
    sf::Vector2f currentRight = current->right;
    sf::Vector2f otherLeft  = other->left;
    sf::Vector2f otherRight  = other->right;

    // Wiggle so on non-terrain tile.
    currentLeft.x -= WIGGLE;
    currentRight.x += WIGGLE;
    otherLeft.x -= WIGGLE;
    otherRight.x += WIGGLE;

    bool leftLeft = viablePath(currentLeft, otherLeft);
    bool leftRight = viablePath(currentLeft, otherRight);
    bool rightLeft = viablePath(currentRight, otherLeft);
    bool rightRight = viablePath(currentRight, otherRight);

    if(leftRight){
        Path* edge = new Path(current, other, current->left, other->right,
                PathType::ANGLED);
        addPathToPlatforms(current, other, edge);
    }
    if(rightLeft){
        Path* edge = new Path(current, other, current->right, other->left,
                PathType::ANGLED);
        addPathToPlatforms(current, other, edge);
    }

    if(leftLeft && !leftRight && !rightLeft){
        Path* edge = new Path(current, other, current->left, other->left,
                PathType::ANGLED);
        addPathToPlatforms(current, other, edge);
    }

    if(rightRight && !rightLeft && !leftRight){
        Path* edge = new Path(current, other, current->right, other->right,
                PathType::ANGLED);
        addPathToPlatforms(current, other, edge);
    }
}

bool PathPlanner::viablePath(sf::Vector2f src, sf::Vector2f dest){
    float horizDistance = std::abs(src.x - dest.x);
    if(horizDistance < VERTICAL_PATH_OFFSET) // Prevent duplicate inline
        return false;
    float vertDistance = -std::abs(src.y - dest.y);
    if(!athleticallyViable(MAX_X_VEL, MAX_Y_VEL, horizDistance, vertDistance))
        return false;
    return world.isClearPath(src, dest);
}

void PathPlanner::addPathToPlatforms(Platform* node1, Platform* node2, Path* edge){
    node1->paths.push_back(edge);
    node2->paths.push_back(edge);
    paths.push_back(edge);
}

PathPlanner::~PathPlanner(){
    for(Path* current : paths)
        delete current;
    for(Platform* current : platforms)
        delete current;
}

void PathPlanner::updatePlayerPos(Player& player){
    auto platformAndPos = getPlatformAndPos(player.getPos());
    sf::Vector2f pos = platformAndPos.second;
    if(player.isMidair()){
        Platform* platform = platformAndPos.first;
        float yDif = -pos.y;
        if(platform == nullptr)
            yDif += MAX_Y;
        else
            yDif += platform->left.y;
        if(yDif > PLAYER_ALTITUDE_CUTOFF)
            return;
    }
    playerPos = pos;
}

bool PathPlanner::pathToPlayer(Enemy& enemy){
    return pathToPos(enemy, playerPos);
}

// The current method calculates paths on-demand during the level.
//
// If this doesn't work, I should swap to pre-computing the path to take from
// a given platform to another so at runtime you only need to check the different
// outgoing nodes, taking into account current position.
//
// To implement this, add a construction function that iterates over each platform
// to create a map of platforms -> list of path + distance.
// Iterate over all platforms and split into outgoing positions. For each position,
// fully explore Dijkstra, tracking distance + causational path. Add an entry to
// the platform mapping for each platform found. Repeat on a fresh graph.
//
// At runtime, check all of the position options for the given platform, taking
// into account current position.
//
// If the setup takes too long, run it in a separate thread. Draw a black screen
// and don't update the scene while polling for completion.

bool PathPlanner::pathToPos(Enemy& currentEnemy, sf::Vector2f target){
    // Skip attacking because means ignore once-off movement commands
    if(currentEnemy.areAttacking())
        return false;

    setupEnemy(currentEnemy);
    std::map<std::string, float>& state = enemy->getState();
    if(state["LOCKED_IN"] != UNLOCKED){
        followPath();
        return true;
    }
    else if(onSamePlatform(target)){
        moveTowards(target);
        return true;
    }

    Path* edge = getCachedPath(target);
    if(!state["CACHED"]){
        edge = findBestPath(target);
        cachePath(edge, target);
    }
    if(edge == nullptr && currentPlatform == getPlatformForPos(target)){
        moveNearTarget(target);
        return true;
    }
    return moveToPath(edge);
}

void PathPlanner::setupEnemy(Enemy& current){
    auto platformAndPos = getPlatformAndPos(current.getPos());
    enemy = &current;
    currentPlatform = platformAndPos.first;
    currentPos = platformAndPos.second;
}

bool PathPlanner::onSamePlatform(sf::Vector2f target){
    if(currentPlatform != getPlatformForPos(target))
        return false;
    float yPos = enemy->getPos().top;
    return world.isClearPath(sf::Vector2f(target.x, yPos),
                             sf::Vector2f(currentPos.x, yPos));
}

// --------

Path* PathPlanner::getCachedPath(sf::Vector2f target){
    std::map<std::string, float>& state = enemy->getState();
    if(!state["CACHED"])
        return nullptr;

    float cachedX = state["CACHED_XPOS"];
    float cachedY = state["CACHED_YPOS"];
    Platform* cached = getPlatformForPos(cachedX, cachedY);
    if(cached != currentPlatform){
        state["CACHED"] = false;
        return nullptr;
    }

    float cachedTargetX = state["CACHED_TARGET_XPOS"];
    float cachedTargetY = state["CACHED_TARGET_YPOS"];
    Platform* targetCached = getPlatformForPos(cachedTargetX, cachedTargetY);
    Platform* targetCurrent = getPlatformForPos(target.x, target.y);
    if(targetCached != targetCurrent){
        state["CACHED"] = false;
        return nullptr;
    }

    int cachedIndex = state["CACHED_PATH_INDEX"];
    if(cachedIndex == NO_PATH_FOUND)
        return nullptr;
    return cached->paths[cachedIndex];
}

void PathPlanner::cachePath(Path* edge,sf::Vector2f target){
    std::map<std::string, float>& state = enemy->getState();
    state["CACHED"] = true;
    state["CACHED_XPOS"] = currentPos.x;
    state["CACHED_YPOS"] = currentPos.y;
    state["CACHED_TARGET_XPOS"] = target.x;
    state["CACHED_TARGET_YPOS"] = target.y;

    if(edge == nullptr){
        state["CACHED_PATH_INDEX"] = NO_PATH_FOUND;
        return;
    }
    for(unsigned int i = 0; i < currentPlatform->paths.size(); i++){
        if(edge == currentPlatform->paths[i]){
            state["CACHED_PATH_INDEX"] = i;
            return;
        }
    }
}

// --------

typedef std::map<int, float> Entrypoints;

struct Journey {
    Journey(float dist, Path* origin, Path* current, Platform* destination):
        dist(dist),
        originEdge(origin),
        currentEdge(current),
        destination(destination){
    }
    float dist;
    Path* originEdge;
    Path* currentEdge;
    Platform* destination;
};

struct JourneyComparator {
    template<typename T>
    bool operator()(const T& l, const T& r) const {
        return l.dist > r.dist;
    }
};

// Dijkstra with each path endpoint as a node.
// Add the final destination as a nullptr Journey Platform that takes into the
// account the actual position ON the platform itself.
Path* PathPlanner::findBestPath(sf::Vector2f target){
    Platform* origin = currentPlatform;
    Platform* destination = getPlatformForPos(target.x, target.y);
    std::map<Platform*, Entrypoints> visited;
    std::priority_queue<Journey, std::vector<Journey>, JourneyComparator> journeys;

    float enemyHeight = enemy->getPos().height;
    for(Path* edge : origin->paths){
        float vertical = edge->getSignedVertical(origin);
        if(!athleticallyViable(edge->horizDistance, vertical))
            continue;
        float yPos = enemy->getPos().top;
        float destX = edge->getSourcePos(origin).x;
        if(!world.isClearPath(sf::Vector2f(currentPos.x, yPos),
                              sf::Vector2f(destX, yPos)))
                continue;
        float dist = std::abs(currentPos.x - destX);
        journeys.push(Journey(dist, edge, edge, edge->getOtherPlatform(origin)));
        visited[origin][edge->getSourcePos(origin).x] = dist;
    }

    while(!journeys.empty()){
        Journey current = journeys.top();
        journeys.pop();

        // ACTUALLY found desired endpoint
        if(current.destination == nullptr)
            return current.originEdge;

        float dist = current.dist;
        Path* edge = current.currentEdge;
        Path* originEdge = current.originEdge;
        Platform* platform = current.destination;
        sf::Vector2f pos = edge->getSourcePos(platform);

        if(visited.count(platform) > 0 && visited[platform].count(pos.x) > 0 &&
                visited[platform][pos.x] < dist)
            continue;
        visited[platform][pos.x] = dist;

        // Found platform so add entry for actual desired position
        if(platform == destination){
            dist += std::abs(pos.x - target.x);
            journeys.push(Journey(dist, originEdge, nullptr, nullptr));
            continue;
        }

        for(Path* outgoing : platform->paths){
            float vertical = outgoing->getSignedVertical(platform);
            if(!athleticallyViable(outgoing->horizDistance, vertical))
                continue;
            float destX = outgoing->getSourcePos(platform).x;
            float yPos = pos.y - enemyHeight;
            if(!world.isClearPath(sf::Vector2f(pos.x, yPos),
                                  sf::Vector2f(destX, yPos)))
                    continue;
            float currentDist = dist + outgoing->totalDistance +
                std::abs(pos.x - destX);
            Platform* currentDest = outgoing->getOtherPlatform(platform);
            journeys.push(Journey(currentDist, originEdge, outgoing, currentDest));
        }
    }
    return nullptr;
}

bool PathPlanner::athleticallyViable(float horiz, float vert){
    return athleticallyViable(enemy->getHorizSpeed(), enemy->getVertSpeed(),
            horiz, vert);
}

bool PathPlanner::athleticallyViable(float xVel, float yVel, float horiz,
        float vert){
    float peakYPos = (yVel * yVel) / (2 * GRAVITY);
    if(peakYPos < vert)
        return false;
    float peakTime = (2 * peakYPos) / yVel;
    float descentTime = std::sqrt(2 * (peakYPos - vert) / GRAVITY);
    float totalTime = peakTime + descentTime;
    float xDist = xVel * totalTime;
    return xDist > horiz;
}

// --------

bool PathPlanner::moveToPath(Path* edge){
    if(edge == nullptr)
        return false;
    else if(enemy->isMidair())
        return true;

    sf::Vector2f targetNode(edge->getSourcePos(currentPlatform));
    bool useBackfoot = edge->type == PathType::INLINE;
    if(edge->type == PathType::VERTICAL){
        Direction approach = makeDirection(targetNode.x - enemy->getBackFootX());
        sf::Vector2f otherNode = edge->getDestinationPos(currentPlatform);
        Direction route = makeDirection(otherNode.x - targetNode.x);
        if(approach != route)
            useBackfoot = true;
    }
    if(moveTowards(targetNode, useBackfoot))
        startPath(edge);
    else if(enemy->getPos().width > FAT){
        sf::FloatRect& pos = enemy->getPos();
        float stuckLeft = enemy->getState()["FAT_STUCK_X"];
        float stuckTop = enemy->getState()["FAT_STUCK_Y"];
        if(pos.left == stuckLeft && pos.top == stuckTop &&
                pos.contains(targetNode.x, targetNode.y - WIGGLE)){
            startPath(edge);
            enemy->getState()["FAT_STUCK_X"] = -1;
        }
        else{
            enemy->getState()["FAT_STUCK_X"] = pos.left;
            enemy->getState()["FAT_STUCK_Y"] = pos.top;
        }
    }
    return true;
}

void PathPlanner::startPath(Path* edge){
    sf::Vector2f targetPos = edge->getDestinationPos(currentPlatform);
    Direction dir = makeRightDir(targetPos.x > currentPos.x);
    if(edge->type == PathType::INLINE)
        dir = makeRightDir(
                targetPos == edge->getOtherPlatform(currentPlatform)->left);

    std::map<std::string, float>& state = enemy->getState();
    state["LOCKED_IN"] = WAITING;
    state["LOCKED_Y"] = targetPos.y;
    state["LOCKED_TYPE"] = (int) edge->type;

    Platform* other = edge->getOtherPlatform(currentPlatform);
    float platformWidth = other->right.x - other->left.x;
    float rng = RNG_MIN + ((float) rand()/RAND_MAX) * RNG_RANGE;
    state["LOCKED_X"] = targetPos.x + dir * (platformWidth - rng);

    if(edge->type == PathType::VERTICAL && targetPos.y > currentPos.y)
        state["LOCKED_X"] -= dir * VERTICAL_PATH_OFFSET;
    if(edge->type == PathType::INLINE){
        dir = !dir;
        state["INLINE_START"] = currentPos.y;
        state["INLINE_PATHING"] = targetPos.x + (dir * INLINE_PATHING);
    }

    if(targetPos.y <= currentPos.y)
        enemy->jump();
    else if(needToJump(edge)){
        if(state["JUMP_EDGE_SOURCE_X"] != edge->getDestinationPos(other).x &&
                state["JUMP_EDGE_SOURCE_Y"] != edge->getDestinationPos(other).y){
            state["JUMP_EDGE_SOURCE_X"] = edge->getDestinationPos(other).x;
            state["JUMP_EDGE_SOURCE_Y"] = edge->getDestinationPos(other).y;
            enemy->jump();
        }
        else{
            state["JUMP_EDGE_SOURCE_X"] = -1;
            state["JUMP_EDGE_SOURCE_Y"] = -1;
        }
    }
    AiHelpers::moveInDirection(*enemy, dir);
}

bool PathPlanner::needToJump(Path* edge){
    float descentTime = std::sqrt(2 * edge->vertDistance / GRAVITY);
    float xDist = descentTime * enemy->getHorizSpeed();
    return edge->horizDistance > xDist;
}

bool PathPlanner::moveTowards(sf::Vector2f target, bool backfoot){
    std::map<std::string, float>& state = enemy->getState();
    float xPos = backfoot ? enemy->getBackFootX() : enemy->getFrontFootX();
    if(state["MOVE_TOWARDS_X"] == target.x){
        float xDif = (target.x - xPos) * enemy->getDirection();
        if(xDif > CLOSE_ENOUGH)
            return false;
        enemy->stopMoving();
        return true;
    }
    state["MOVE_TOWARDS_X"] = target.x;
    AiHelpers::moveInDirection(*enemy, makeRightDir(xPos < target.x));
    return false;
}

void PathPlanner::followPath(){
    std::map<std::string, float>& state = enemy->getState();
    if(enemy->isStationary()){
        state["LOCKED_IN"] = UNLOCKED;
        return;
    }
    if(enemy->isMidair())
        state["LOCKED_IN"] = LOCKED;
    else {
        if(state["LOCKED_IN"] == LOCKED)
            state["LOCKED_IN"] = UNLOCKED;
        return;
    }

    float yPos = enemy->getFeetY();
    sf::Vector2f target = sf::Vector2f(state["LOCKED_X"], state["LOCKED_Y"]);
    PathType type = (PathType) state["LOCKED_TYPE"];

    if(type == PathType::VERTICAL){
        if(yPos > target.y)
            enemy->stopMoving();
        else
            moveTowards(target);
    }
    else if(type == PathType::ANGLED)
        moveTowards(target);
    else if(type == PathType::INLINE){
        float initY = state["INLINE_START"];
        bool goingUp = initY > target.y;
        if(goingUp){
            if(yPos < target.y)
                moveTowards(target);
            else
                moveTowards(sf::Vector2f(state["INLINE_PATHING"], target.y));
        }
        else if(!goingUp){
            if(yPos > initY + TILE_Y_PX)
                moveTowards(target);
            else
                enemy->stopMoving();
        }
    }
}

void PathPlanner::moveNearTarget(sf::Vector2f target){
    sf::FloatRect& pos = enemy->getPos();
    Direction dir = makeRightDir(target.x > pos.left);
    float x = (((int) (pos.left / TILE_X_PX)) * TILE_X_PX) + WIGGLE;
    while(!world.isObstacle(x, pos.top))
        x += TILE_X_PX * dir;
    if(dir == Direction::LEFT)
        x += TILE_X_PX;
    x -= dir * MOVE_NEAR_OFFSET;
    moveTowards(sf::Vector2f(x, pos.top), false);
}

// --------

Platform* PathPlanner::getPlatformForPos(float xPos, float yPos){
    if(xPos < 0 || xPos >= MAX_X || yPos < 0 || yPos >= MAX_Y)
        return nullptr;
    int row = yPos / TILE_Y_PX;
    int col = xPos / TILE_X_PX;
    return tilePlatformMap[row][col];
}

Platform* PathPlanner::getPlatformForPos(sf::Vector2f pos){
    return getPlatformForPos(pos.x, pos.y);
}

Platform* PathPlanner::getPlatformForPos(sf::FloatRect& pos){
    return getPlatformAndPos(pos).first;
}

std::pair<Platform*, sf::Vector2f> PathPlanner::getPlatformAndPos(
        sf::FloatRect& pos){
    float yPos = pos.top + pos.height;
    sf::Vector2f leftPos(pos.left, yPos);
    sf::Vector2f rightPos(pos.left + pos.width, yPos);
    Platform* left = getPlatformForPos(leftPos);
    Platform* right = getPlatformForPos(rightPos);
    if(left == nullptr)
        return std::make_pair(right, rightPos);
    if(right == nullptr)
        return std::make_pair(left, leftPos);
    if(left->left.y < right->left.y)
        return std::make_pair(left, leftPos);
    if(right->left.y < left->left.y)
        return std::make_pair(right, rightPos);
    return std::make_pair(left, sf::Vector2f(pos.left + pos.width/2.f, yPos));
}
