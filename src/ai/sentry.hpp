#pragma once

#include "path_planner.hpp"
#include "../characters/enemy.hpp"

struct Sentry {
    static void Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
    static void Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
};
