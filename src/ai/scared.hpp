#pragma once

#include "path_planner.hpp"
#include "../characters/enemy.hpp"

struct Scared {
    static void Enabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
    static void Disabled(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
private:
    static void fleeingUpdate(Enemy& enemy, PathPlanner& pathPlanner,
            World& world, sf::FloatRect& target);
    static void safeUpdate(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target);
    static void fightUpdate(Enemy& enemy, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target);

    static float getSeparationDistance(sf::FloatRect&, sf::FloatRect&);
    static float getVisibleDistance(sf::FloatRect&, sf::FloatRect&, World&);
};
