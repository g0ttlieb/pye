# pragma once

#include <SFML/Graphics.hpp>
#include <list>
#include "scenes/scene.hpp"

class GameManager {
public:
    GameManager();
    ~GameManager();
    void updateGame(float dt);
    void renderGame(sf::RenderWindow* window);
    void handleInput(sf::Event& event);
    bool shouldQuit();
private:
    std::list<Scene*> scenes;
    bool quit = false;

    void handleSceneTransition(SceneTransition newScene);
};
