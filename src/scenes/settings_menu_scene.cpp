#include "settings_menu_scene.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/settings.hpp"

const float TITLE_X = 244;
const float MODE_TITLE_Y = 158;
const float RES_TITLE_Y = 191;
const float VSYNC_TITLE_Y = 212;
const float MUSIC_TITLE_Y = 236;
const float EFFECTS_TITLE_Y = 257;

const float MODE_X = 336;
const float BORDERLESS_Y = 145;
const float FULLSCREEN_Y = 158;
const float WINDOWED_Y = 171;

const float CURRENT_RES_X = 360;
const float CURRENT_RES_Y = 191;

const float VSYNC_Y = 212;
const float VSYNC_ON_X = 336;
const float VSYNC_OFF_X = 373;

const float VOLUME_X = 360;
const float MUSIC_Y = 236;
const float EFFECTS_Y = 257;

const int TITLE_FONT_SIZE = 10;
const int OPTION_FONT_SIZE = 8;
const int CURRENT_RES_FONT_SIZE = 7;

const float CHECKMARK_X_OFFSET = 10;
const float CHECKMARK_Y_OFFSET = 2;

SettingsMenuScene::SettingsMenuScene(ButtonAreas buttonAreas):
            MenuScene("menu-settings"),
            updateWindowMode(false),
            updateRes(false),
            updateVSync(false){
    for(std::pair<std::string, sf::FloatRect> button : buttonAreas){
        if(button.first == "borderless")
            buttons.push_back(Button{button.second, setBorderless, nullptr});
        else if(button.first == "fullscreen")
            buttons.push_back(Button{button.second, setFullscreen, nullptr});
        else if(button.first == "windowed")
            buttons.push_back(Button{button.second, setWindowed, nullptr});
        else if(button.first == "decrease_res")
            buttons.push_back(Button{button.second, decreaseRes, nullptr});
        else if(button.first == "increase_res")
            buttons.push_back(Button{button.second, increaseRes, nullptr});
        else if(button.first == "vsync_on")
            buttons.push_back(Button{button.second, setVSyncOn, nullptr});
        else if(button.first == "vsync_off")
            buttons.push_back(Button{button.second, setVSyncOff, nullptr});
        else if(button.first == "decrease_music")
            buttons.push_back(Button{button.second, decreaseMusicVol, nullptr});
        else if(button.first == "increase_music")
            buttons.push_back(Button{button.second, increaseMusicVol, nullptr});
        else if(button.first == "decrease_effects")
            buttons.push_back(Button{button.second, decreaseEffectsVol, nullptr});
        else if(button.first == "increase_effects")
            buttons.push_back(Button{button.second, increaseEffectsVol, nullptr});
        else if(button.first == "cancel")
            buttons.push_back(Button{button.second, cancel, nullptr});
    }
    setupText();
    setupCheckMarks();
}

void SettingsMenuScene::setupText(){
    font.loadFromFile(ResourceManager::getFont("settings_menu.ttf"));

    setupTextLine(windowMode, TITLE_X, MODE_TITLE_Y, TITLE_FONT_SIZE);
    SpriteHelper::leftAlignText(windowMode, "Window Mode");
    setupTextLine(modeBorderless, MODE_X, BORDERLESS_Y, OPTION_FONT_SIZE);
    SpriteHelper::leftAlignText(modeBorderless, "Borderless");
    setupTextLine(modeFullscreen, MODE_X, FULLSCREEN_Y, OPTION_FONT_SIZE);
    SpriteHelper::leftAlignText(modeFullscreen, "Fullscreen");
    setupTextLine(modeWindowed, MODE_X, WINDOWED_Y, OPTION_FONT_SIZE);
    SpriteHelper::leftAlignText(modeWindowed, "Windowed");

    setupTextLine(resolution, TITLE_X, RES_TITLE_Y, TITLE_FONT_SIZE);
    SpriteHelper::leftAlignText(resolution, "Resolution");
    setupTextLine(currentResolution, CURRENT_RES_X, CURRENT_RES_Y,
            CURRENT_RES_FONT_SIZE);
    SpriteHelper::centerAlignText(currentResolution, getCurrentRes());

    setupTextLine(vSync, TITLE_X, VSYNC_TITLE_Y, TITLE_FONT_SIZE);
    SpriteHelper::leftAlignText(vSync, "V-Sync");
    setupTextLine(vSyncOn, VSYNC_ON_X, VSYNC_Y, OPTION_FONT_SIZE);
    SpriteHelper::leftAlignText(vSyncOn, "On");
    setupTextLine(vSyncOff, VSYNC_OFF_X, VSYNC_Y, OPTION_FONT_SIZE);
    SpriteHelper::leftAlignText(vSyncOff, "Off");

    setupTextLine(music, TITLE_X, MUSIC_TITLE_Y, TITLE_FONT_SIZE);
    SpriteHelper::leftAlignText(music, "Music Volume");
    setupTextLine(musicLevel, VOLUME_X, MUSIC_Y, TITLE_FONT_SIZE);
    SpriteHelper::centerAlignText(musicLevel, getMusicLevel());
    setupTextLine(effects, TITLE_X, EFFECTS_TITLE_Y, TITLE_FONT_SIZE);
    SpriteHelper::leftAlignText(effects, "Effects Volume");
    setupTextLine(effectsLevel, VOLUME_X, EFFECTS_Y, TITLE_FONT_SIZE);
    SpriteHelper::centerAlignText(effectsLevel, getEffectsLevel());
}

void SettingsMenuScene::setupTextLine(sf::Text& text, float x, float y, int size){
    float scale = Settings::instance().scale();
    text.setFont(font);
    text.setFillColor(sf::Color::White);
    text.setPosition(x * scale, y * scale);
    text.setCharacterSize(size * scale);
}

void SettingsMenuScene::setupCheckMarks(){
    checkTexture.loadFromFile(ResourceManager::getVisual("menu-checkmark.png"));
    modeCheckMark.setTexture(checkTexture);
    vSyncCheckMark.setTexture(checkTexture);

    float scale = Settings::instance().scale();
    modeCheckMark.setScale(scale, scale);
    vSyncCheckMark.setScale(scale, scale);

    setModeCheckMark();
    setVsyncCheckMark();
}

void SettingsMenuScene::setModeCheckMark(){
    WindowMode mode = Settings::instance().getWindowMode();
    float scale = Settings::instance().scale();
    float x = MODE_X - CHECKMARK_X_OFFSET;
    float y = -CHECKMARK_Y_OFFSET;
    if(mode == WindowMode::BORDERLESS)
        y += BORDERLESS_Y;
    else if(mode == WindowMode::FULLSCREEN)
        y += FULLSCREEN_Y;
    else if(mode == WindowMode::WINDOWED)
        y += WINDOWED_Y;
    modeCheckMark.setPosition(x * scale, y * scale);
}

void SettingsMenuScene::setVsyncCheckMark(){
    bool enabled = Settings::instance().vSyncEnabled();
    float scale = Settings::instance().scale();
    float x = -CHECKMARK_X_OFFSET;
    float y = VSYNC_Y - CHECKMARK_Y_OFFSET;
    if(enabled)
        x += VSYNC_ON_X;
    else
        x += VSYNC_OFF_X;
    vSyncCheckMark.setPosition(x * scale,y * scale);
}

SettingsMenuScene::~SettingsMenuScene(){
}

SceneTransition SettingsMenuScene::escapeKeyScene(){
    return SceneTransition(Transition::EXIT_CURRENT_SCENE);
}

void SettingsMenuScene::renderGame(sf::RenderWindow* window){
    Settings& settings = Settings::instance();
    if(updateVSync){
        window->setVerticalSyncEnabled(settings.vSyncEnabled());
        updateVSync = false;
    }
    if(updateRes){
        settings.setupWindow(window);
        updateResolution();
        updateRes = false;
    }
    if(updateWindowMode){
        settings.setupWindow(window);
        updateWindowMode = false;
    }

    MenuScene::renderGame(window);

    window->draw(windowMode);
    window->draw(modeBorderless);
    window->draw(modeFullscreen);
    window->draw(modeWindowed);

    window->draw(resolution);
    window->draw(currentResolution);

    window->draw(vSync);
    window->draw(vSyncOn);
    window->draw(vSyncOff);

    window->draw(modeCheckMark);
    window->draw(vSyncCheckMark);

    window->draw(music);
    window->draw(musicLevel);
    window->draw(effects);
    window->draw(effectsLevel);
}

void SettingsMenuScene::setWindowMode(WindowMode mode){
    Settings::instance().setMode(mode);
    setModeCheckMark();
    updateWindowMode = true;
}

void SettingsMenuScene::changeRes(int dif){
    updateRes = Settings::instance().changeRes(dif);
    SpriteHelper::centerAlignText(currentResolution, getCurrentRes());
}

void SettingsMenuScene::setVSync(bool enabled){
    Settings::instance().setVSync(enabled);
    setVsyncCheckMark();
    updateVSync = true;
}

void SettingsMenuScene::changeMusicVolume(int dif){
    Settings::instance().changeMusicLevel(dif);
    SpriteHelper::centerAlignText(musicLevel, getMusicLevel());
}

void SettingsMenuScene::changeEffectsVolume(int dif){
    Settings::instance().changeEffectsLevel(dif);
    SpriteHelper::centerAlignText(effectsLevel, getEffectsLevel());
}

void SettingsMenuScene::updateResolution(){
    setupText();
    setupCheckMarks();
    MenuScene::updateResolution();
}

void SettingsMenuScene::setBorderless(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->setWindowMode(WindowMode::BORDERLESS);
}

void SettingsMenuScene::setFullscreen(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->setWindowMode(WindowMode::FULLSCREEN);
}

void SettingsMenuScene::setWindowed(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->setWindowMode(WindowMode::WINDOWED);
}

void SettingsMenuScene::decreaseRes(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeRes(-1);
}

void SettingsMenuScene::increaseRes(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeRes(1);
}

void SettingsMenuScene::setVSyncOn(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->setVSync(true);
}

void SettingsMenuScene::setVSyncOff(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->setVSync(false);
}

void SettingsMenuScene::decreaseMusicVol(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeMusicVolume(-1);
}

void SettingsMenuScene::increaseMusicVol(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeMusicVolume(1);
}

void SettingsMenuScene::decreaseEffectsVol(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeEffectsVolume(-1);
}

void SettingsMenuScene::increaseEffectsVol(MenuScene& scene, void* data){
    (void) data;
    SettingsMenuScene* instance = dynamic_cast<SettingsMenuScene*>(&scene);
    instance->changeEffectsVolume(1);
}

void SettingsMenuScene::cancel(MenuScene& scene, void* data){
    (void) data;
    scene.setSceneTransition(SceneTransition(Transition::EXIT_CURRENT_SCENE));
}

std::string SettingsMenuScene::getCurrentRes(){
    Settings& settings = Settings::instance();
    return std::to_string(settings.getXRes()) + " x " +
           std::to_string(settings.getYRes());
}

std::string SettingsMenuScene::getMusicLevel(){
    return std::to_string(Settings::instance().getMusicLevel());
}

std::string SettingsMenuScene::getEffectsLevel(){
    return std::to_string(Settings::instance().getEffectsLevel());
}
