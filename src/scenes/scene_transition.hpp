#pragma once

#include <SFML/Graphics.hpp>
#include <string>

enum class Transition{
    NONE, OVERLAY_CURRENT, EXIT_CURRENT_SCENE, EXIT_TO_BASE, EXIT_ALL
};

class SceneTransition{
public:
    SceneTransition();
    SceneTransition(Transition transition);
    SceneTransition(std::string name, Transition transition);
    SceneTransition(std::string name, sf::FloatRect trigger,
                    sf::Vector2f startPos, std::string triggerFlag="",
                    Transition transition=Transition::EXIT_CURRENT_SCENE);

    bool noNewScene();
    bool exitCurrentScene();
    bool exitAll();
    bool exitToBase();
    std::string getName();

    bool triggered(sf::FloatRect& pos);

    void calibrateStartPos(sf::FloatRect& pos);
    sf::Vector2f getStartPos();

private:
    std::string name;
    sf::FloatRect trigger;
    std::string triggerFlag;
    sf::Vector2f startPos;
    Transition transition;
};
