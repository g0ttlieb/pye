#include <cmath>
#include <sstream>
#include <iomanip>
#include "main_menu_scene.hpp"
#include "../inventory/inventory.hpp"
#include "../inventory/save_manager.hpp"
#include "../helpers/music.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/file_helper.hpp"
#include "../helpers/settings.hpp"

const int CENTER_X = 320;
const int TITLE_Y = 75;
const int TITLE_FONT_SIZE = 50;
const int EXIT_Y = 325;
const int EXIT_FONT_SIZE = 10;

const int SAVE_SLOTS = 3;
const int LINES_OF_TEXT = 4;
const int TEXT_PER_LINE = 2;
const int TEXT_PER_SAVE_SLOT = LINES_OF_TEXT * TEXT_PER_LINE;

const int SLOT_POS_X[] = {68, 240, 412};
const int SLOT_POS_Y[] = {133, 138, 133};

const int LOOP_TIME_INDEX = 0;
const int TOTAL_TIME_INDEX = 1;
const int LOOP_NUMBER_INDEX = 2;
const int LEVEL_TITLE_INDEX = 4;
const int LEVEL_BACKGROUND_INDEX = 5;

const std::string DETAIL_HEADINGS[] = {
    "Level Title",
    "Loop Number",
    "Loop Runtime",
    "Total Runtime"
};

const int BACKGROUND_HEIGHT = 90;
const int SAVE_SLOT_WIDTH = 160;
const int TOP_PAD = 10;
const int LEFT_PAD = 2;
const int RIGHT_PAD = 2;
const int LINE_HEIGHT = 13;
const int SAVE_FONT_SIZE = 8;

const int SECONDS_PER_MINUTE = 60;
const int SECONDS_PER_HOUR = 60 * SECONDS_PER_MINUTE;

MainMenuScene::MainMenuScene(ButtonAreas buttonAreas):
            MenuScene("menu-main"){
    for(std::pair<std::string, sf::FloatRect> button : buttonAreas){
        if(button.first == "delete_save_0")
            buttons.push_back(Button{button.second, deleteSave0, nullptr});
        else if(button.first == "delete_save_1")
            buttons.push_back(Button{button.second, deleteSave1, nullptr});
        else if(button.first == "delete_save_2")
            buttons.push_back(Button{button.second, deleteSave2, nullptr});
        else if(button.first == "load_save_0")
            buttons.push_back(Button{button.second, loadSave0, nullptr});
        else if(button.first == "load_save_1")
            buttons.push_back(Button{button.second, loadSave1, nullptr});
        else if(button.first == "load_save_2")
            buttons.push_back(Button{button.second, loadSave2, nullptr});
        else if(button.first == "exit")
            buttons.push_back(Button{button.second, exitGame, nullptr});
        else if(button.first == "settings")
            buttons.push_back(Button{button.second, settings, nullptr});
        else if(button.first == "credit")
            buttons.push_back(Button{button.second, credit, nullptr});
    }
    setupTitleAndExit();
    setupSaveDetails();
    Music::playMusic("main-menu");
}

void MainMenuScene::setupTitleAndExit(){
    font.loadFromFile(ResourceManager::getFont("main_menu.ttf"));
    float scale = Settings::instance().scale();

    titleText.setFont(font);
    titleText.setFillColor(sf::Color::White);
    titleText.setPosition(CENTER_X * scale, TITLE_Y * scale);
    titleText.setCharacterSize(TITLE_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(titleText, "Pye");

    exitText.setFont(font);
    exitText.setFillColor(sf::Color::White);
    exitText.setPosition(CENTER_X * scale, EXIT_Y * scale);
    exitText.setCharacterSize(EXIT_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(exitText, "EXIT");
}

void MainMenuScene::setupSaveDetails(){
    saveTexts.resize(SAVE_SLOTS * LINES_OF_TEXT * TEXT_PER_LINE);
    saveTextures.resize(SAVE_SLOTS);
    saveSprites.resize(SAVE_SLOTS);
    for(int i = 0; i < SAVE_SLOTS; i++)
        setupSaveSlotDetails(i);
}

void MainMenuScene::setupSaveSlotDetails(int saveNumber){
    float scale = Settings::instance().scale();
    int x = SLOT_POS_X[saveNumber];
    int y = SLOT_POS_Y[saveNumber];
    std::vector<std::string> lines = FileHelper::readSaveFile(
            "save" + std::to_string(saveNumber));

    sf::Texture* texture = new sf::Texture();
    texture->loadFromFile(ResourceManager::getVisual(
                lines[LEVEL_BACKGROUND_INDEX] + "-background.png"));
    saveTextures[saveNumber] = texture;
    sf::Sprite* sprite = new sf::Sprite(*texture);
    sprite->setPosition(x * scale, y * scale);
    sprite->setScale(scale, scale);
    saveSprites[saveNumber] = sprite;

    std::vector<std::string> details;
    details.push_back(lines[LEVEL_TITLE_INDEX]);
    details.push_back(lines[LOOP_NUMBER_INDEX]);
    details.push_back(formatTime(lines[LOOP_TIME_INDEX]));
    details.push_back(formatTime(lines[TOTAL_TIME_INDEX]));

    int baseY = y + BACKGROUND_HEIGHT + TOP_PAD;
    int fontSize = SAVE_FONT_SIZE * scale;
    int textIndex = saveNumber * TEXT_PER_SAVE_SLOT;
    for(int i = 0; i < LINES_OF_TEXT; i++){
        sf::Text* key = new sf::Text();
        key->setFont(font);
        key->setFillColor(sf::Color::White);
        key->setPosition((x + LEFT_PAD) * scale,
                         (baseY + i * LINE_HEIGHT) * scale);
        key->setCharacterSize(fontSize);
        SpriteHelper::leftAlignText(*key, DETAIL_HEADINGS[i]);
        saveTexts[textIndex++] = key;

        sf::Text* val = new sf::Text();
        val->setFont(font);
        val->setFillColor(sf::Color::White);
        val->setPosition((x + SAVE_SLOT_WIDTH - RIGHT_PAD) * scale,
                         (baseY + i * LINE_HEIGHT) * scale);
        val->setCharacterSize(fontSize);
        SpriteHelper::rightAlignText(*val, details[i]);
        saveTexts[textIndex++] = val;
    }
}

std::string MainMenuScene::formatTime(std::string rawSeconds){
    int remainingSeconds = std::stof(rawSeconds);
    int hourCount = remainingSeconds / SECONDS_PER_HOUR;
    remainingSeconds = remainingSeconds % SECONDS_PER_HOUR;
    int minuteCount = remainingSeconds / SECONDS_PER_MINUTE;
    int secoundCount = remainingSeconds % SECONDS_PER_MINUTE;
    std::stringstream ss;
    ss << std::setw(2) << std::setfill('0') << hourCount;
    ss << ":";
    ss << std::setw(2) << std::setfill('0') << minuteCount;
    ss << ":";
    ss << std::setw(2) << std::setfill('0') << secoundCount;
    return ss.str();
}

MainMenuScene::~MainMenuScene(){
    for(auto text : saveTexts)
        delete text;
    for(auto texture : saveTextures)
        delete texture;
    for(auto sprite : saveSprites)
        delete sprite;
}

SceneTransition MainMenuScene::escapeKeyScene(){
    return SceneTransition();
}

void MainMenuScene::updateCurrentSaveSlotVisual(){
    int saveNumber = Inventory::instance().getSaveIndex();
    delete saveTextures[saveNumber];
    delete saveSprites[saveNumber];
    int baseTextNumber = saveNumber * TEXT_PER_SAVE_SLOT;
    for(int i = 0; i < TEXT_PER_SAVE_SLOT; i++)
        delete saveTexts[baseTextNumber + i];
    setupSaveSlotDetails(saveNumber);
}

void MainMenuScene::renderGame(sf::RenderWindow* window){
    if(SaveManager::saveWasReset())
        updateCurrentSaveSlotVisual();
    MenuScene::renderGame(window);
    window->draw(titleText);
    window->draw(exitText);
    for(sf::Sprite* background : saveSprites)
        window->draw(*background);
    for(sf::Text* text : saveTexts)
        window->draw(*text);
}

void MainMenuScene::inactiveUpdate(){
    if(Settings::instance().resHasChanged())
        updateResolution();
    MenuScene::inactiveUpdate();
}

void MainMenuScene::updateResolution(){
    setupTitleAndExit();
    setupSaveDetails();
    MenuScene::updateResolution();
}

void MainMenuScene::deleteSave0(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(0);
    scene.setSceneTransition(SceneTransition("menu_confirm", Transition::NONE));
}

void MainMenuScene::deleteSave1(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(1);
    scene.setSceneTransition(SceneTransition("menu_confirm", Transition::NONE));
}

void MainMenuScene::deleteSave2(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(2);
    scene.setSceneTransition(SceneTransition("menu_confirm", Transition::NONE));
}

void MainMenuScene::loadSave0(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(0);
    scene.setSceneTransition(SaveManager::load());
}

void MainMenuScene::loadSave1(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(1);
    scene.setSceneTransition(SaveManager::load());
}

void MainMenuScene::loadSave2(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(2);
    scene.setSceneTransition(SaveManager::load());
}

void MainMenuScene::exitGame(MenuScene& scene, void* data){
    (void) data;
    scene.setSceneTransition(SceneTransition(Transition::EXIT_ALL));
}

void MainMenuScene::settings(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(2);
    scene.setSceneTransition(SceneTransition("menu_settings", Transition::NONE));
}

void MainMenuScene::credit(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().setSaveIndex(2);
    scene.setSceneTransition(SceneTransition("menu_credit", Transition::NONE));
}
