#pragma once

#include "menu_scene.hpp"

class CreditMenuScene : public MenuScene {
public:
    CreditMenuScene(ButtonAreas buttonAreas);
    virtual ~CreditMenuScene();

    virtual SceneTransition escapeKeyScene();
    virtual void renderGame(sf::RenderWindow* window);

    static void cancel(MenuScene& scene, void* data);
private:
    sf::Font font;
    sf::Text line1;
    sf::Text line2;
    sf::Text button;
    void setupText();
};
