#include "confirm_menu_scene.hpp"
#include "../inventory/save_manager.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/settings.hpp"

const int CENTER_X = 320;
const int LINE1_Y = 170;
const int LINE2_Y = 182;
const int FONT_SIZE = 10;

ConfirmMenuScene::ConfirmMenuScene(ButtonAreas buttonAreas):
            MenuScene("menu-confirm"){
    for(std::pair<std::string, sf::FloatRect> button : buttonAreas){
        if(button.first == "cancel")
            buttons.push_back(Button{button.second, cancel, nullptr});
        else if(button.first == "delete_save")
            buttons.push_back(Button{button.second, deleteSave, nullptr});
    }
    setupText();
}

void ConfirmMenuScene::setupText(){
    font.loadFromFile(ResourceManager::getFont("confirm_menu.ttf"));
    float scale = Settings::instance().scale();

    line1.setFont(font);
    line1.setFillColor(sf::Color::White);
    line1.setPosition(CENTER_X * scale, LINE1_Y * scale);
    line1.setCharacterSize(FONT_SIZE * scale);
    SpriteHelper::centerAlignText(line1, "Are you sure?");

    line2.setFont(font);
    line2.setFillColor(sf::Color::White);
    line2.setPosition(CENTER_X * scale, LINE2_Y * scale);
    line2.setCharacterSize(FONT_SIZE * scale);
    SpriteHelper::centerAlignText(line2, "Save deletion is permanent!");
}

ConfirmMenuScene::~ConfirmMenuScene(){
}

SceneTransition ConfirmMenuScene::escapeKeyScene(){
    return SceneTransition(Transition::EXIT_CURRENT_SCENE);
}

void ConfirmMenuScene::renderGame(sf::RenderWindow* window){
    MenuScene::renderGame(window);
    window->draw(line1);
    window->draw(line2);
}

void ConfirmMenuScene::deleteSave(MenuScene& scene, void* data){
    (void) data;
    SaveManager::reset();
    scene.setSceneTransition(SceneTransition(Transition::EXIT_CURRENT_SCENE));
}

void ConfirmMenuScene::cancel(MenuScene& scene, void* data){
    (void) data;
    scene.setSceneTransition(SceneTransition(Transition::EXIT_CURRENT_SCENE));
}
