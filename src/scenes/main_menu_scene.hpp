#pragma once

#include "menu_scene.hpp"

class MainMenuScene : public MenuScene {
public:
    MainMenuScene(ButtonAreas buttonAreas);
    virtual ~MainMenuScene();

    virtual SceneTransition escapeKeyScene();
    void updateCurrentSaveSlotVisual();
    virtual void renderGame(sf::RenderWindow* window);
    virtual void inactiveUpdate();

    virtual void updateResolution();

    static void deleteSave0(MenuScene& scene, void* data);
    static void deleteSave1(MenuScene& scene, void* data);
    static void deleteSave2(MenuScene& scene, void* data);
    static void loadSave0(MenuScene& scene, void* data);
    static void loadSave1(MenuScene& scene, void* data);
    static void loadSave2(MenuScene& scene, void* data);
    static void exitGame(MenuScene& scene, void* data);
    static void settings(MenuScene& scene, void* data);
    static void credit(MenuScene& scene, void* data);

private:
    sf::Font font;

    sf::Text titleText;
    sf::Text exitText;
    void setupTitleAndExit();

    std::vector<sf::Text*> saveTexts;
    std::vector<sf::Texture*> saveTextures;
    std::vector<sf::Sprite*> saveSprites;
    void setupSaveDetails();
    void setupSaveSlotDetails(int saveNumber);
    std::string formatTime(std::string rawSeconds);
};
