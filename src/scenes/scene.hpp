# pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include "scene_transition.hpp"
#include "../controllers/controller.hpp"

class Scene {
public:
    virtual ~Scene(){}

    virtual Controller& getController() = 0;
    virtual void updateGame(float dt) = 0;
    virtual void renderGame(sf::RenderWindow* window) = 0;
    virtual void inactiveUpdate(){}

    virtual bool mouseVisible() { return false; }
    virtual SceneTransition getNextScene() = 0;
    virtual SceneTransition escapeKeyScene() = 0;
};
