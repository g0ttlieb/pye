#include "credit_menu_scene.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/settings.hpp"

const int CENTER_X = 320;
const int LINE1_Y = 168;
const int LINE2_Y = 182;
const int BUTTON_Y = 203;

const int LINE1_FONT_SIZE = 8;
const int LINE2_FONT_SIZE = 10;
const int BUTTON_FONT_SIZE = 6;

CreditMenuScene::CreditMenuScene(ButtonAreas buttonAreas):
            MenuScene("menu-credit"){
    for(std::pair<std::string, sf::FloatRect> button : buttonAreas){
        if(button.first == "cancel")
            buttons.push_back(Button{button.second, cancel, nullptr});
    }
    setupText();
}

void CreditMenuScene::setupText(){
    font.loadFromFile(ResourceManager::getFont("credit_menu.ttf"));
    float scale = Settings::instance().scale();

    line1.setFont(font);
    line1.setFillColor(sf::Color::White);
    line1.setPosition(CENTER_X * scale, LINE1_Y * scale);
    line1.setCharacterSize(LINE1_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(line1, "Made by");

    line2.setFont(font);
    line2.setFillColor(sf::Color::White);
    line2.setPosition(CENTER_X * scale, LINE2_Y * scale);
    line2.setCharacterSize(LINE2_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(line2, "HUGH GOTTLIEB");

    button.setFont(font);
    button.setFillColor(sf::Color::White);
    button.setPosition(CENTER_X * scale, BUTTON_Y * scale);
    button.setCharacterSize(BUTTON_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(button, "Ok...?");
}

CreditMenuScene::~CreditMenuScene(){
}

SceneTransition CreditMenuScene::escapeKeyScene(){
    return SceneTransition(Transition::EXIT_CURRENT_SCENE);
}

void CreditMenuScene::renderGame(sf::RenderWindow* window){
    MenuScene::renderGame(window);
    window->draw(line1);
    window->draw(line2);
    window->draw(button);
}

void CreditMenuScene::cancel(MenuScene& scene, void* data){
    (void) data;
    scene.setSceneTransition(SceneTransition(Transition::EXIT_CURRENT_SCENE));
}
