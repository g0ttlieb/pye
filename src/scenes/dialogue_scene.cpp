#include "dialogue_scene.hpp"
#include "../controllers/dialogue_controller.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../inventory/inventory.hpp"

const std::string INIT_PHRASE = "init";
const std::string FINAL_PHRASE = "exit";
const int START_AT_ONE = 1;

DialogueScene::DialogueScene(std::string npc,
        std::vector<std::pair<std::string, std::string>> initialPhrases,
        std::map<std::string, Phrase*> phrases):
            phrases(phrases){
    Inventory& inv = Inventory::instance();
    std::string firstPhrase = INIT_PHRASE;
    for(auto current : initialPhrases){
        if(inv.hasCollectable(current.second)){
            firstPhrase = current.first;
            break;
        }
    }
    currentPhrase = phrases[firstPhrase];
    addExitPhrase();
    controller = new DialogueController();
    visual = new DialogueVisual(npc);
    visual->setPhrase(currentPhrase);
}

void DialogueScene::addExitPhrase(){
    Phrase* finalPhrase = new Phrase(FINAL_PHRASE);
    for(int i = 0; i < Phrase::getNumberOfLines(); i++)
        finalPhrase->addLine("");
    phrases[FINAL_PHRASE] = finalPhrase;
}

DialogueScene::~DialogueScene(){
    for(auto entry : phrases)
        delete entry.second;
}

Controller& DialogueScene::getController(){
    return *controller;
}

SceneTransition DialogueScene::getNextScene(){
    if(currentPhrase->name == FINAL_PHRASE)
        return SceneTransition(Transition::EXIT_CURRENT_SCENE);
    return SceneTransition();
}

SceneTransition DialogueScene::escapeKeyScene(){
    return SceneTransition("menu_game", Transition::OVERLAY_CURRENT);
}

void DialogueScene::updateGame(float dt){
    (void) dt;
    if(!controller->selectionWasMade())
        return;
    int selection = controller->getThenResetSelection() - START_AT_ONE;
    if(!currentPhrase->hasResponse(selection))
        return;
    Response* resp = currentPhrase->getResponse(selection);
    resp->setSelectedFlag();
    freeTapeSlot = Inventory::instance().hasCollectable("free_tape");
    currentPhrase = phrases[resp->nextPhrase];
    visual->setPhrase(currentPhrase);
}

void DialogueScene::inactiveUpdate(){
    bool newFreeTapeSlot = Inventory::instance().hasCollectable("free_tape");
    if(newFreeTapeSlot == freeTapeSlot)
        return;
    freeTapeSlot = newFreeTapeSlot;
    visual->setPhrase(currentPhrase);
}

void DialogueScene::renderGame(sf::RenderWindow* window){
    window->draw(*visual);
}
