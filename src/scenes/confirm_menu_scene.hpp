#pragma once

#include "menu_scene.hpp"

class ConfirmMenuScene : public MenuScene {
public:
    ConfirmMenuScene(ButtonAreas buttonAreas);
    virtual ~ConfirmMenuScene();

    virtual SceneTransition escapeKeyScene();
    virtual void renderGame(sf::RenderWindow* window);

    static void deleteSave(MenuScene& scene, void* data);
    static void cancel(MenuScene& scene, void* data);
private:
    sf::Font font;
    sf::Text line1;
    sf::Text line2;
    void setupText();
};
