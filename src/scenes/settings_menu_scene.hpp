#pragma once

#include "menu_scene.hpp"
#include "../helpers/settings.hpp"

class SettingsMenuScene : public MenuScene {
public:
    SettingsMenuScene(ButtonAreas buttonAreas);
    virtual ~SettingsMenuScene();

    virtual SceneTransition escapeKeyScene();
    virtual void renderGame(sf::RenderWindow* window);

    void setWindowMode(WindowMode mode);
    void changeRes(int dif);
    void setVSync(bool enabled);
    void changeMusicVolume(int dif);
    void changeEffectsVolume(int dif);

    virtual void updateResolution();

    static void setBorderless(MenuScene& scene, void* data);
    static void setFullscreen(MenuScene& scene, void* data);
    static void setWindowed(MenuScene& scene, void* data);
    static void decreaseRes(MenuScene& scene, void* data);
    static void increaseRes(MenuScene& scene, void* data);
    static void setVSyncOff(MenuScene& scene, void* data);
    static void setVSyncOn(MenuScene& scene, void* data);
    static void decreaseMusicVol(MenuScene& scene, void* data);
    static void increaseMusicVol(MenuScene& scene, void* data);
    static void decreaseEffectsVol(MenuScene& scene, void* data);
    static void increaseEffectsVol(MenuScene& scene, void* data);
    static void cancel(MenuScene& scene, void* data);
private:
    bool updateWindowMode;
    bool updateRes;
    bool updateVSync;

    sf::Text windowMode;
    sf::Text modeBorderless;
    sf::Text modeFullscreen;
    sf::Text modeWindowed;

    sf::Text resolution;
    sf::Text currentResolution;
    std::string getCurrentRes();

    sf::Text vSync;
    sf::Text vSyncOn;
    sf::Text vSyncOff;

    sf::Text music;
    sf::Text musicLevel;
    sf::Text effects;
    sf::Text effectsLevel;
    std::string getMusicLevel();
    std::string getEffectsLevel();

    sf::Texture checkTexture;
    sf::Sprite modeCheckMark;
    sf::Sprite vSyncCheckMark;
    void setupCheckMarks();
    void setModeCheckMark();
    void setVsyncCheckMark();

    sf::Font font;
    void setupText();
    void setupTextLine(sf::Text& text, float x, float y, int size);
};
