#pragma once

#include "menu_scene.hpp"
#include "../weapons/element.hpp"

class GameMenuScene : public MenuScene {
public:
    GameMenuScene(ButtonAreas buttonAreas);
    virtual ~GameMenuScene();

    virtual SceneTransition escapeKeyScene();
    virtual void renderGame(sf::RenderWindow* window);

    void selectElement(Element* element);
    void selectLight(Element* element);
    void selectHeavy(Element* element);
    void removeTape(int index);

    static void clickAvailable(MenuScene& scene, void* data);
    static void clickLight(MenuScene& scene, void* data);
    static void clickHeavy(MenuScene& scene, void* data);
    static void clickTape(MenuScene& scene, void* data);
    static void warp(MenuScene& scene, void* data);
    static void exitGame(MenuScene& scene, void* data);
    static void mainMenu(MenuScene& scene, void* data);
private:
    Element* selected;
    std::list<Element*> lightChain;
    std::list<Element*> heavyChain;
    std::map<Element*, int> enabledElements;
    void setupWeaponState();
    void setupWeaponChain(std::list<Element*>& chain, Weapon* weapon);

    sf::Font font;
    sf::Text exitText;
    sf::Text mainMenuText;
    sf::Text lightWeaponText;
    sf::Text heavyWeaponText;
    void setupExitText();
    void setupWeaponLabelText();

    sf::Texture weaponIconTexture;
    sf::VertexArray availableVertices;
    void setupAvailableElements();
    void shadeElement(Element* element);

    sf::VertexArray chainVertices;
    void updateChain(std::list<Element*>& chain, Element* element, bool isLight);
    void removeFromChain(std::list<Element*>& chain, Element* element);
    void insertSelected(std::list<Element*>& chain, Element* before);
    void updateWeapon(std::list<Element*>& chain, bool isLight);
    void updateChainVisuals();
    void updateChainVisual(float y, std::list<Element*>& chain, int initIndex);

    sf::Texture keysTexture;
    sf::VertexArray keysVertices;
    void setupKeys();

    sf::VertexArray tapeVertices;
    std::vector<sf::Text> tapeText;
    void setupTapes();
    std::string getTapeString(std::string text);

    sf::Text warpText;
    bool warpEnabled;
    void setupWarp();

    std::vector<Button> baseButtons;
    std::vector<sf::RectangleShape> buttonHighlights;
    void setupButtons();
    bool weaponActive(Weapon* weapon);
    void setupAvailableButtons();
    void setupChainButtons(float y, std::list<Element*> chain,
            ButtonCallback callback);
    void setupArrowButtons(float y, std::list<Element*> chain,
            ButtonCallback callback);
    void setupTapeButtons();
    void setupWarpButton();
    void setupButtonHighlights();
};
