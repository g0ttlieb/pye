# pragma once

#include "combat_scene.hpp"

class EndCombatScene : public CombatScene {
public:
    EndCombatScene(std::string file,
                std::string name,
                std::string levelLayout,
                std::string tileset,
                std::string backgroundImage,
                std::string music,
                sf::Vector2f playerPos,
                std::vector<EnemyGroup*> enemyGroups,
                std::vector<SceneTransition> sceneTransitions,
                std::vector<Gateway*> gateways,
                std::vector<Collectable*> collectables,
                std::vector<NPC*> npcs,
                std::vector<std::pair<sf::Vector2f, std::string>> textEntries);
    virtual ~EndCombatScene();

    virtual SceneTransition getNextScene();
    virtual void updateGame(float dt);
    virtual void renderGame(sf::RenderWindow* window);

private:
    Enemy* boss;
    enum class TriggerDialogue{
        WAITING, YES, DONE
    } triggerDialogue;
    float dialogueDelay;

    Gateway* exitGateway;
    bool triggerExit;
    float sceneEndDelay;

    sf::Font font;
    sf::Text bossHealth;
    sf::Texture bossHealthBarTexture;
    sf::Sprite bossHealthBarSprite;
    sf::RectangleShape bossHealthBar;
    sf::RenderStates scaleState;

    void updateLevelCleared();
};
