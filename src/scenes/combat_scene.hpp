# pragma once

#include "scene.hpp"
#include "../ai/path_planner.hpp"
#include "../characters/player.hpp"
#include "../characters/enemy_group.hpp"
#include "../controllers/controller.hpp"
#include "../inventory/overlay.hpp"
#include "../world/world.hpp"
#include "../world/background.hpp"
#include "../world/gateway.hpp"
#include "../world/collectable.hpp"

class CombatScene : public Scene {
public:
    CombatScene(std::string file,
                std::string name,
                std::string levelLayout,
                std::string tileset,
                std::string backgroundImage,
                std::string music,
                sf::Vector2f playerPos,
                std::vector<EnemyGroup*> enemyGroups,
                std::vector<SceneTransition> sceneTransitions,
                std::vector<Gateway*> gateways,
                std::vector<Collectable*> collectables,
                std::vector<NPC*> npcs,
                std::vector<std::pair<sf::Vector2f, std::string>> textEntries);
    virtual ~CombatScene();

    virtual Controller& getController();
    virtual SceneTransition getNextScene();
    virtual SceneTransition escapeKeyScene();

    virtual void updateGame(float dt);
    virtual void renderGame(sf::RenderWindow* window);
    virtual void inactiveUpdate();

protected:
    std::string name;
    World* world;
    PathPlanner* pathPlanner;
    Player* player;
    Overlay* overlay;
    Background* background;
    Controller* controller;
    std::vector<EnemyGroup*> enemies;
    std::vector<SceneTransition> sceneTransitions;

    bool paused;
    void pause();
    void unpause();

    bool cleared;
    virtual void updateLevelCleared();
};
