#include <map>
#include <sstream>
#include "scene_loader.hpp"
#include "combat_scene.hpp"
#include "start_combat_scene.hpp"
#include "end_combat_scene.hpp"
#include "dialogue_scene.hpp"
#include "main_menu_scene.hpp"
#include "game_menu_scene.hpp"
#include "confirm_menu_scene.hpp"
#include "credit_menu_scene.hpp"
#include "settings_menu_scene.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/file_helper.hpp"
#include "../dialogue/phrase.hpp"

Scene* SceneLoader::getScene(SceneTransition newScene){
    std::vector<std::string> lines = FileHelper::readSceneFile(newScene.getName());
    if(lines[0] == "combat")
        return loadCombatScene(lines, newScene);
    else if(lines[0] == "dialogue")
        return loadDialogueScene(lines);
    else if(lines[0] == "menu")
        return loadMenuScene(lines);
    else
        return NULL;
}

Scene* SceneLoader::loadCombatScene(std::vector<std::string>& lines,
        SceneTransition& transition){
    unsigned int index = 1;
    std::string sceneName = lines[index++];
    std::string levelLayout = lines[index++];
    std::string tileset = lines[index++];
    std::string backgroundImage = lines[index++];
    std::string music = lines[index++];
    index++;

    // Scene transitions
    std::vector<SceneTransition> transitions;
    while(index < lines.size() && lines[index] != "")
        transitions.push_back(readTransitionFromLine(lines[index++]));
    index++;

    // Locked doors
    std::vector<Gateway*> gateways;
    while(index < lines.size() && lines[index] != "")
        gateways.push_back(readGatewayFromLine(lines[index++]));
    index++;

    // Collectables
    std::vector<Collectable*> collectables;
    while(index < lines.size() && lines[index] != "")
        collectables.push_back(readCollectableFromLine(lines[index++]));
    index++;

    // NPCS
    std::vector<NPC*> npcs;
    while(index < lines.size() && lines[index] != "")
        npcs.push_back(readNPCFromLine(lines[index++]));
    index++;

    // Text entries
    std::vector<std::pair<sf::Vector2f, std::string>> textEntries;
    while(index < lines.size() && lines[index] != "")
        textEntries.push_back(readTextEntryFromLine(lines[index++]));
    index++;

    // Enemies
    std::vector<EnemyGroup*> enemyGroups;
    while(index < lines.size()){
        std::string enemyType = lines[index++];
        std::vector<std::pair<sf::Vector2f,Direction>> positions;
        while(index < lines.size() && lines[index] != "")
            positions.push_back(readEnemySpawnFromLine(lines[index++]));
        enemyGroups.push_back(new EnemyGroup(enemyType, positions));
        index++;
    }

    if(transition.getName() == "start")
        return new StartCombatScene(transition.getName(), sceneName, levelLayout,
            tileset, backgroundImage, music, transition.getStartPos(), enemyGroups,
            transitions, gateways, collectables, npcs, textEntries);
    if(transition.getName() == "end")
        return new EndCombatScene(transition.getName(), sceneName, levelLayout,
            tileset, backgroundImage, music, transition.getStartPos(), enemyGroups,
            transitions, gateways, collectables, npcs, textEntries);
    return new CombatScene(transition.getName(), sceneName, levelLayout, tileset,
            backgroundImage, music, transition.getStartPos(), enemyGroups,
            transitions, gateways, collectables, npcs, textEntries);
}

sf::Vector2f SceneLoader::readPosFromLine(std::string line){
    float xPos;
    float yPos;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    return sf::Vector2f(xPos, yPos);
}

SceneTransition SceneLoader::readTransitionFromLine(std::string line){
    float triggerX;
    float triggerY;
    float triggerWidth;
    float triggerHeight;
    std::string scene;
    float startX;
    float startY;
    std::string flag;
    std::stringstream ss(line);
    ss >> triggerX;
    ss >> triggerY;
    ss >> triggerWidth;
    ss >> triggerHeight;
    ss >> scene;
    ss >> startX;
    ss >> startY;
    ss >> flag;
    return SceneTransition(scene,
            sf::FloatRect(triggerX, triggerY, triggerWidth, triggerHeight),
            sf::Vector2f(startX, startY), flag);
}

Gateway* SceneLoader::readGatewayFromLine(std::string line){
    float xPos;
    float yPos;
    bool state;
    std::string control;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    ss >> state;
    ss >> control;
    return new Gateway(sf::Vector2f(xPos, yPos), state, control);
}

Collectable* SceneLoader::readCollectableFromLine(std::string line){
    float xPos;
    float yPos;
    std::string name;
    bool persistence;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    ss >> name;
    ss >> persistence;
    return new Collectable(sf::Vector2f(xPos, yPos), name, persistence);
}

NPC* SceneLoader::readNPCFromLine(std::string line){
    float xPos;
    float yPos;
    std::string name;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    ss >> name;
    return new NPC(sf::Vector2f(xPos, yPos), name);
}

std::pair<sf::Vector2f, std::string> SceneLoader::readTextEntryFromLine(
        std::string line){
    float xPos;
    float yPos;
    std::string content;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    getline(ss, content);
    return std::make_pair(sf::Vector2f(xPos, yPos), content);
}

std::pair<sf::Vector2f,Direction> SceneLoader::readEnemySpawnFromLine(
        std::string line){
    float xPos;
    float yPos;
    int dir;
    std::stringstream ss(line);
    ss >> xPos;
    ss >> yPos;
    ss >> dir;
    return std::make_pair(sf::Vector2f(xPos, yPos), (Direction) dir);
}

Scene* SceneLoader::loadDialogueScene(std::vector<std::string>& lines){

    unsigned int index = 1;
    std::string npc = lines[index++];
    index++;

    std::vector<std::pair<std::string, std::string>> initalPhrases;
    while(index < lines.size() && lines[index] != "")
        initalPhrases.push_back(readInitialPhraseFromLine(lines[index++]));
    index++;

    std::map<std::string, Phrase*> phrases;
    while(index < lines.size()){
        std::string phraseName = lines[index++];
        Phrase* currentPhrase = new Phrase(phraseName);
        for(int i = 0; i < Phrase::getNumberOfLines(); i++)
            currentPhrase->addLine(lines[index++]);
        while(index < lines.size() && lines[index] != "")
            currentPhrase->addResponse(readResponseFromLine(lines[index++]));
        phrases[phraseName] = currentPhrase;
        index++;
    }

    return new DialogueScene(npc, initalPhrases, phrases);
}

std::pair<std::string, std::string> SceneLoader::readInitialPhraseFromLine(
        std::string line){
    std::string phrase;
    std::string flag;
    std::stringstream ss(line);
    ss >> phrase;
    ss >> flag;
    return std::make_pair(phrase, flag);
}

Response* SceneLoader::readResponseFromLine(std::string line){
    int pos = line.find("|");
    std::string text = line.substr(0, pos);
    std::string other = line.substr(pos+1);

    std::string nextPhrase;
    std::string showIfFlag;
    std::string setFlag;
    std::stringstream ss(other);
    ss >> nextPhrase;
    ss >> showIfFlag;
    ss >> setFlag;
    return new Response(text, nextPhrase, showIfFlag, setFlag);
}

Scene* SceneLoader::loadMenuScene(std::vector<std::string>& lines){
    unsigned int index = 1;
    std::string name = lines[index++];
    index++;

    std::vector<std::pair<std::string, sf::FloatRect>> buttons;
    while(index < lines.size())
        buttons.push_back(readButtonFromLine(lines[index++]));

    if(name == "main-menu")
        return new MainMenuScene(buttons);
    else if(name == "game-menu")
        return new GameMenuScene(buttons);
    else if(name == "confirm-menu")
        return new ConfirmMenuScene(buttons);
    else if(name == "credit-menu")
        return new CreditMenuScene(buttons);
    else if(name == "settings-menu")
        return new SettingsMenuScene(buttons);
    else
        return NULL;
}

std::pair<std::string, sf::FloatRect> SceneLoader::readButtonFromLine(
        std::string line){
    std::string name;
    int x;
    int y;
    int width;
    int height;
    std::stringstream ss(line);
    ss >> name;
    ss >> x;
    ss >> y;
    ss >> width;
    ss >> height;
    return std::make_pair(name, sf::FloatRect(x, y, width, height));
}
