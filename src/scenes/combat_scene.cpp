#include "combat_scene.hpp"
#include "../controllers/combat_controller.hpp"
#include "../inventory/inventory.hpp"
#include "../inventory/save_manager.hpp"
#include "../helpers/music.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const sf::Vector2f TUTORIAL_SPAWN_POS(24,336);
const sf::Vector2f RESPAWN_POS(77,8);

CombatScene::CombatScene(std::string name,
                         std::string title,
                         std::string levelLayout,
                         std::string tileset,
                         std::string backgroundImage,
                         std::string music,
                         sf::Vector2f playerPos,
                         std::vector<EnemyGroup*> enemyGroups,
                         std::vector<SceneTransition> sceneTransitions,
                         std::vector<Gateway*> gateways,
                         std::vector<Collectable*> collectables,
                         std::vector<NPC*> npcs,
                         std::vector<std::pair<sf::Vector2f, std::string>> texts):
            name(name),
            enemies(enemyGroups),
            sceneTransitions(sceneTransitions),
            paused(false),
            cleared(false){
    world = new World(title, levelLayout, tileset, gateways, collectables, npcs,
                      texts);
    pathPlanner = new PathPlanner(*world);
    player = new Player(playerPos.x, playerPos.y);
    overlay = new Overlay();
    background = new Background(backgroundImage);
    controller = new CombatController(player);
    Inventory::instance().resetWeapons();

    if(Inventory::instance().hasCollectable(name + "_cleared")){
        for(auto enemyGroup : enemies)
            delete enemyGroup;
        enemies.clear();
        cleared = true;
    }
    Inventory::instance().addCollectable(name + "_entered", false);
    SaveManager::save(name, title, backgroundImage, playerPos);
    Music::playMusic(music);
}

CombatScene::~CombatScene(){
    delete world;
    delete pathPlanner;
    delete player;
    delete overlay;
    delete background;
    delete controller;
    for(auto enemyGroup : enemies)
        delete enemyGroup;
}

Controller& CombatScene::getController(){
    return *controller;
}

SceneTransition CombatScene::getNextScene(){
    Inventory& inventory = Inventory::instance();
    if(player->finishedWarp()){
        inventory.setWarped();
        return SceneTransition("end", sf::FloatRect(), inventory.getWarpPos());
    }
    if(player->finishedDying() && name.find("tutorial") == 0){
        Inventory::instance().resetRun();
        return SceneTransition("tutorial1", sf::FloatRect(), TUTORIAL_SPAWN_POS);
    }

    if(inventory.hasNewDialogueScene())
        return inventory.getNewDialogueScene();
    for(auto entry : sceneTransitions){
        if(entry.triggered(player->getPos())){
            entry.calibrateStartPos(player->getPos());
            return entry;
        }
    }
    return SceneTransition();
}

SceneTransition CombatScene::escapeKeyScene(){
    if(player->isWarping() || player->isDying())
        return SceneTransition();
    return SceneTransition("menu_game", Transition::OVERLAY_CURRENT);
}

void CombatScene::updateGame(float dt){
    Inventory& inventory = Inventory::instance();
    if(inventory.getHealthCurrent() <= 0){
        if(inventory.canWarp()){
            inventory.warp();
            inventory.setHealth(1);
        }
        else
            player->die();
    }
    if(inventory.isWarping()){
        player->updateWarp(dt);
        return;
    }
    if(paused)
        unpause();

    inventory.update(dt);
    world->update(dt, player->getPos());
    player->update(dt, *world);
    pathPlanner->updatePlayerPos(*player);
    if(inventory.lightWeapon())
        inventory.lightWeapon()->update(dt, enemies, *world);
    if(inventory.heavyWeapon())
        inventory.heavyWeapon()->update(dt, enemies, *world);
    // Must be updated after weapons so accurate alive count
    for(auto enemyGroup : enemies)
        enemyGroup->update(dt, *pathPlanner, *world, player->getPos());
    overlay->update(dt);
    updateLevelCleared();
}

void CombatScene::renderGame(sf::RenderWindow* window){
    window->draw(*background);
    for(auto enemyGroup : enemies)
        window->draw(*enemyGroup);
    if(Inventory::instance().lightWeapon())
        window->draw(*Inventory::instance().lightWeapon());
    if(Inventory::instance().heavyWeapon())
        window->draw(*Inventory::instance().heavyWeapon());
    window->draw(*player);
    window->draw(*world);
    window->draw(*overlay);
}

void CombatScene::inactiveUpdate(){
    if(!paused)
        pause();
}

void CombatScene::pause(){
    paused = true;
    player->stopMoving();
    if(Inventory::instance().lightWeapon())
        Inventory::instance().lightWeapon()->pause();
    if(Inventory::instance().heavyWeapon())
        Inventory::instance().heavyWeapon()->pause();
}

void CombatScene::unpause(){
    paused = false;
    overlay->updateWeaponIcons();
    if(Inventory::instance().lightWeapon())
        Inventory::instance().lightWeapon()->unpause();
    if(Inventory::instance().heavyWeapon())
        Inventory::instance().heavyWeapon()->unpause();
}

void CombatScene::updateLevelCleared(){
    if(cleared)
        return;

    for(auto enemyGroup : enemies){
        if(enemyGroup->areRemainingEnemies())
            return;
    }
    Inventory::instance().addCollectable(name + "_cleared", false);
    SaveManager::save(player->getPos());
    cleared = true;
}
