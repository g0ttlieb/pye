#pragma once

#include "scene.hpp"
#include "../controllers/menu_controller.hpp"

typedef std::vector<std::pair<std::string, sf::FloatRect>> ButtonAreas;

class MenuScene : public Scene {
public:

    typedef void (*ButtonCallback)(MenuScene&, void*);
    struct Button {
        sf::FloatRect pos;
        ButtonCallback callback;
        void* data;
    };

    MenuScene(std::string name);
    virtual ~MenuScene();

    virtual Controller& getController();
    virtual SceneTransition getNextScene();
    void setSceneTransition(SceneTransition nextScene);

    virtual void updateGame(float dt);
    virtual void renderGame(sf::RenderWindow* window);
    virtual void inactiveUpdate();

    virtual bool mouseVisible();

    virtual void updateResolution();
protected:
    std::vector<Button> buttons;
    virtual sf::Color getHighlightColor();
    void drawHighlight(sf::RenderWindow* window);

private:
    MenuController* controller;
    SceneTransition sceneTransition;

    sf::Sprite menuSprite;
    sf::Texture menuTexture;

    bool showHighlight;
    sf::RectangleShape highlightRect;
};
