#pragma once

#include <map>
#include "scene.hpp"
#include "../controllers/dialogue_controller.hpp"
#include "../dialogue/phrase.hpp"
#include "../dialogue/dialogue_visual.hpp"

class DialogueScene : public Scene {
public:
    DialogueScene(std::string npc,
                  std::vector<std::pair<std::string, std::string>> initalPhrases,
                  std::map<std::string, Phrase*> phrases);
    virtual ~DialogueScene();

    virtual Controller& getController();
    virtual SceneTransition getNextScene();
    virtual SceneTransition escapeKeyScene();

    virtual void updateGame(float dt);
    virtual void renderGame(sf::RenderWindow* window);
    virtual void inactiveUpdate();
private:
    Phrase* currentPhrase;
    std::map<std::string, Phrase*> phrases;
    void addExitPhrase();

    bool freeTapeSlot;

    DialogueVisual* visual;
    DialogueController* controller;
};
