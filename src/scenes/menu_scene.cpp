#include "menu_scene.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/resource_manager.hpp"

const int HIGHLIGHT_THICKNESS = 1;

MenuScene::MenuScene(std::string name):
        showHighlight(false){
    controller = new MenuController();
    menuTexture.loadFromFile(ResourceManager::getVisual(name + ".png"));
    menuSprite.setTexture(menuTexture);
    highlightRect.setOutlineColor(getHighlightColor());
    highlightRect.setOutlineThickness(HIGHLIGHT_THICKNESS);
    highlightRect.setFillColor(sf::Color::Transparent);
    updateResolution();
}

void MenuScene::updateResolution(){
    float scale = Settings::instance().scale();
    menuSprite.setScale(scale, scale);
    highlightRect.setScale(scale, scale);
}

MenuScene::~MenuScene(){
}

Controller& MenuScene::getController(){
    return *controller;
}

SceneTransition MenuScene::getNextScene(){
    SceneTransition current = sceneTransition;
    sceneTransition = SceneTransition();
    return current;
}

void MenuScene::setSceneTransition(SceneTransition nextScene){
    sceneTransition = nextScene;
}

void MenuScene::updateGame(float dt){
    (void) dt;
    showHighlight = true;
    float scale = Settings::instance().scale();
    std::vector<sf::Vector2i> clicks = controller->getAndResetClicks();
    for(sf::Vector2i click : clicks){
        sf::Vector2i scaledPos(click.x / scale, click.y / scale);
        for(Button button : buttons){
            if(button.pos.contains(scaledPos.x, scaledPos.y)){
                button.callback(*this, button.data);
                break;
            }
        }
    }
}

void MenuScene::renderGame(sf::RenderWindow* window){
    window->draw(menuSprite);
    if(showHighlight)
        drawHighlight(window);
}

void MenuScene::inactiveUpdate(){
    showHighlight = false;
}

bool MenuScene::mouseVisible(){
    return true;
}

void MenuScene::drawHighlight(sf::RenderWindow* window){
    float scale = Settings::instance().scale();
    sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
    mousePos.x /= scale;
    mousePos.y /= scale;
    for(Button button : buttons){
        sf::FloatRect box = button.pos;
        if(box.contains(mousePos.x, mousePos.y)){
            highlightRect.setPosition(box.left * scale, box.top * scale);
            highlightRect.setSize(sf::Vector2f(box.width, box.height));
            window->draw(highlightRect);
            return;
        }
    }
}

sf::Color MenuScene::getHighlightColor(){
    return sf::Color(172, 165, 38);
}
