# pragma once

#include <SFML/System.hpp>
#include <string>
#include <vector>
#include "scene.hpp"
#include "scene_transition.hpp"
#include "../dialogue/phrase.hpp"
#include "../world/gateway.hpp"
#include "../world/collectable.hpp"
#include "../world/npc.hpp"
#include "../helpers/direction.hpp"

class SceneLoader {
public:
    static Scene* getScene(SceneTransition newScene);
private:
    static Scene* loadCombatScene(std::vector<std::string>& lines,
            SceneTransition& transition);
    static sf::Vector2f readPosFromLine(std::string line);
    static SceneTransition readTransitionFromLine(std::string line);
    static Gateway* readGatewayFromLine(std::string line);
    static Collectable* readCollectableFromLine(std::string line);
    static NPC* readNPCFromLine(std::string line);
    static std::pair<sf::Vector2f, std::string> readTextEntryFromLine(
            std::string line);
    static std::pair<sf::Vector2f,Direction> readEnemySpawnFromLine(
            std::string line);

    static Scene* loadDialogueScene(std::vector<std::string>& lines);
    static std::pair<std::string, std::string> readInitialPhraseFromLine(
            std::string line);
    static Response* readResponseFromLine(std::string line);

    static Scene* loadMenuScene(std::vector<std::string>& lines);
    static std::pair<std::string, sf::FloatRect> readButtonFromLine(
            std::string line);
};
