#include "start_combat_scene.hpp"
#include "../inventory/inventory.hpp"

const std::string BODY_NPC_NAME = "player_body_";
const std::string LEFT_STR = "left";
const std::string RIGHT_STR = "right";
const std::string LOOT_NAME = "player_body_loot";

const sf::Vector2f EXIT_GATEWAY(624, 240);

StartCombatScene::StartCombatScene(std::string name, std::string title,
        std::string levelLayout, std::string tileset, std::string backgroundImage,
        std::string music, sf::Vector2f playerPos,
        std::vector<EnemyGroup*> enemyGroups,
        std::vector<SceneTransition> sceneTransitions,
        std::vector<Gateway*> gateways,
        std::vector<Collectable*> collectables,
        std::vector<NPC*> npcs,
        std::vector<std::pair<sf::Vector2f, std::string>> texts):
            CombatScene(name, title, levelLayout, tileset, backgroundImage, music,
                        playerPos, enemyGroups, sceneTransitions, gateways,
            addBodyCollectable(collectables), addBodyNpc(npcs), texts),
            closedExitGateway(false),
            poppedLoadout(false){
    auto& inv = Inventory::instance();
    if(!inv.hasCollectable(LOOT_NAME)){
        inv.stashLoadout();
        overlay->updateWeaponIcons();
    }
    else
        poppedLoadout = true;

    for(auto gateway : gateways){
        if(gateway->getTopLeft() == EXIT_GATEWAY){
            exitGateway = gateway;
            if(poppedLoadout)
                exitGateway->toggleStateImmediately(*world);
            else
                exitGateway->startToggleAnimation();
            break;
        }
    }
}

StartCombatScene::~StartCombatScene(){
}

std::vector<NPC*> StartCombatScene::addBodyNpc(std::vector<NPC*> npcs){
    auto& inv = Inventory::instance();
    Direction dir = inv.getBodyDir();
    std::string postfix = (dir == Direction::LEFT) ? LEFT_STR : RIGHT_STR;
    npcs.push_back(new NPC(inv.getBodyPos(), BODY_NPC_NAME + postfix));
    return npcs;
}

std::vector<Collectable*> StartCombatScene::addBodyCollectable(
        std::vector<Collectable*> collectables){
    collectables.push_back(new Collectable(
                Inventory::instance().getBodyCollectablePos(), LOOT_NAME, false));
    return collectables;
}

void StartCombatScene::updateGame(float dt){
    CombatScene::updateGame(dt);
    if(!closedExitGateway && exitGateway->isFinishedAnimation()){
        closedExitGateway = true;
        exitGateway->reallowToggle();
    }
    if(!poppedLoadout && Inventory::instance().hasCollectable(LOOT_NAME)){
        poppedLoadout = true;
        Inventory::instance().popLoadout();
        overlay->updateWeaponIcons();
    }
}

void StartCombatScene::updateLevelCleared(){
}
