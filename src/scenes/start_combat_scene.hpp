# pragma once

#include "combat_scene.hpp"

class StartCombatScene : public CombatScene {
public:
    StartCombatScene(std::string file,
                std::string name,
                std::string levelLayout,
                std::string tileset,
                std::string backgroundImage,
                std::string music,
                sf::Vector2f playerPos,
                std::vector<EnemyGroup*> enemyGroups,
                std::vector<SceneTransition> sceneTransitions,
                std::vector<Gateway*> gateways,
                std::vector<Collectable*> collectables,
                std::vector<NPC*> npcs,
                std::vector<std::pair<sf::Vector2f, std::string>> textEntries);
    virtual ~StartCombatScene();
    virtual void updateGame(float dt);

protected:
    void updateLevelCleared();

private:
    std::vector<NPC*> addBodyNpc(std::vector<NPC*> npcs);
    std::vector<Collectable*> addBodyCollectable(
            std::vector<Collectable*> collectables);

    Gateway* exitGateway;
    bool closedExitGateway;
    bool poppedLoadout;
};
