#include <sstream>
#include <iomanip>
#include "end_combat_scene.hpp"
#include "../inventory/inventory.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/resource_manager.hpp"

const float DIALOGUE_DELAY = 0.5f;
const float SCENE_END_DELAY = 12.f;
const float ALL_LEFT_END_DELAY = 0.5f;

const float HP_LEFT = 1.f;
const float HP_TOP = 1.f;
const float HP_WIDTH = 94;
const float HP_HEIGHT = 6.f;
const float HP_BAR_X = 272;
const float HP_BAR_Y = 36;
const float FONT_SIZE = 4;
const sf::Color HP_COLOUR(128,0,0);

const sf::Vector2f RESPAWN_POS(77,8);
const sf::Vector2f EXIT_POS(639, 272);
const sf::FloatRect EXIT_BOX(637, 240, 6, 32);
const sf::Vector2f EXIT_GATEWAY(624, 240);

const float NPC_HEIGHT = 16.f;
const float HALF_PLAYER_WIDTH = 3.f;
const float HALF_COLLECTABLE_WIDTH = 4.f;
const float LOOT_HOVER = 12.f;
const float LOOT_X_DIFF = 16.f;
const float LOOT_Y_DIFF = 8.f;
const float MIDDLE_X_POS = 320.f;

const std::string BOSS_DIALOGUE = "end_actrian_leader";

EndCombatScene::EndCombatScene(std::string name, std::string title,
        std::string levelLayout, std::string tileset, std::string backgroundImage,
        std::string music, sf::Vector2f playerPos,
        std::vector<EnemyGroup*> enemyGroups,
        std::vector<SceneTransition> sceneTransitions,
        std::vector<Gateway*> gateways,
        std::vector<Collectable*> collectables,
        std::vector<NPC*> npcs,
        std::vector<std::pair<sf::Vector2f, std::string>> texts):
    CombatScene(name, title, levelLayout, tileset, backgroundImage, music,
            playerPos, enemyGroups, sceneTransitions, gateways, collectables,
            npcs, texts),
    triggerDialogue(TriggerDialogue::WAITING),
    dialogueDelay(DIALOGUE_DELAY),
    triggerExit(false),
    sceneEndDelay(SCENE_END_DELAY){
    for(auto enemyGroup : enemyGroups){
        if(enemyGroup->getName() == "actrian-leader"){
            boss = enemyGroup->getEnemies().front();
            break;
        }
    }
    for(auto gateway : gateways){
        if(gateway->getTopLeft() == EXIT_GATEWAY){
            exitGateway = gateway;
            break;
        }
    }

    float scale = Settings::instance().scale();
    scaleState.transform.scale(scale, scale);
    bossHealthBarTexture.loadFromFile(
            ResourceManager::getVisual("boss-health-bar.png"));
    bossHealthBarSprite.setTexture(bossHealthBarTexture);
    bossHealthBarSprite.setPosition(HP_BAR_X, HP_BAR_Y);
    bossHealthBar.setPosition(HP_BAR_X + HP_LEFT, HP_BAR_Y + HP_TOP);
    bossHealthBar.setFillColor(HP_COLOUR);

    font.loadFromFile(ResourceManager::getFont("overlay.ttf"));
    bossHealth.setFont(font);
    bossHealth.setCharacterSize(FONT_SIZE * scale);
    bossHealth.setFillColor(sf::Color::White);
    bossHealth.setPosition((HP_BAR_X + HP_LEFT + HP_WIDTH/2.f) * scale,
                           (HP_BAR_Y + HP_TOP + HP_HEIGHT/2.f) * scale);
}

EndCombatScene::~EndCombatScene(){
}

SceneTransition EndCombatScene::getNextScene(){
    if(triggerDialogue == TriggerDialogue::YES){
        triggerDialogue = TriggerDialogue::DONE;
        if(Inventory::instance().getHealthCurrent() > 0.f)
            return SceneTransition(BOSS_DIALOGUE, Transition::OVERLAY_CURRENT);
    }
    if(sceneEndDelay < 0.f){
        Inventory::instance().resetRun();
        return SceneTransition("start", sf::FloatRect(), RESPAWN_POS);
    }
    return CombatScene::getNextScene();
}

void EndCombatScene::updateGame(float dt){
    if(boss->areDead() && triggerDialogue == TriggerDialogue::WAITING){
        if(dialogueDelay < 0.f)
            triggerDialogue = TriggerDialogue::YES;
        else
            dialogueDelay -= dt;
    }
    if(Inventory::instance().getHealthCurrent() <= 0.f && !player->isMidair()){
        if(!triggerExit){
            triggerExit = true;
            exitGateway->retoggle();
            sf::FloatRect pos = player->getPos();
            Direction bodyDir = player->getDirection();
            sf::Vector2f bodyPos(pos.left, pos.top + pos.height - NPC_HEIGHT);
            sf::Vector2f lootPos(
                    pos.left + HALF_PLAYER_WIDTH - HALF_COLLECTABLE_WIDTH,
                    pos.top + pos.height - LOOT_HOVER);
            Direction dir = makeLeftDir(lootPos.x > MIDDLE_X_POS);
            bool validLootPos = true;
            if(!world->isObstacle(lootPos.x, lootPos.y - LOOT_Y_DIFF))
                lootPos.y -= LOOT_Y_DIFF;
            else if(!world->isObstacle(lootPos.x + dir * LOOT_X_DIFF, lootPos.y))
                lootPos.x += dir * LOOT_X_DIFF;
            else if(!world->isObstacle(lootPos.x + !dir * LOOT_X_DIFF, lootPos.y))
                lootPos.x -= dir * LOOT_X_DIFF;

            if(validLootPos)
                Inventory::instance().setBody(bodyPos, lootPos, bodyDir);
            else
                Inventory::instance().resetBodyToDefault();
        }
        sceneEndDelay -= dt;

        for(auto& group : enemies){
            group->disableAi();
            std::list<Enemy*> enemyInstances = group->getEnemies();
            for(auto enemy : enemyInstances){
                if(enemy->getPos().intersects(EXIT_BOX))
                    group->removeEnemy(enemy);
                else{
                    pathPlanner->pathToPos(*enemy, EXIT_POS);
                    enemy->run();
                }
            }
        }
    }

    CombatScene::updateGame(dt);
}

void EndCombatScene::renderGame(sf::RenderWindow* window){
    CombatScene::renderGame(window);

    std::stringstream healthStream;
    healthStream << (int) boss->getHpCurrent() << "/" << (int) boss->getHpMax();
    SpriteHelper::centerAlignText(bossHealth, healthStream.str());
    bossHealthBar.setSize(sf::Vector2f(HP_WIDTH*boss->getHpPercent(), HP_HEIGHT));

    window->draw(bossHealthBarSprite, scaleState);
    window->draw(bossHealthBar, scaleState);
    window->draw(bossHealth);
}

void EndCombatScene::updateLevelCleared(){
    if(!triggerExit)
        CombatScene::updateLevelCleared();
    else if(sceneEndDelay > ALL_LEFT_END_DELAY){
        for(auto enemyGroup : enemies){
            if(enemyGroup->areRemainingEnemies())
                return;
        }
        sceneEndDelay = ALL_LEFT_END_DELAY;
    }
}
