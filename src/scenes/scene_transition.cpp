#include "scene_transition.hpp"
#include "../inventory/inventory.hpp"

const float DIRECTION_CUTOFF = 4.1f;

SceneTransition::SceneTransition():
    SceneTransition("", Transition::NONE){}

SceneTransition::SceneTransition(Transition transition):
    SceneTransition("", transition){}

SceneTransition::SceneTransition(std::string name, Transition transition):
    name(name),
    transition(transition){}

SceneTransition::SceneTransition(std::string name, sf::FloatRect trigger,
        sf::Vector2f startPos, std::string triggerFlag, Transition transition):
    name(name),
    trigger(trigger),
    triggerFlag(triggerFlag),
    startPos(startPos),
    transition(transition){}

bool SceneTransition::noNewScene(){
    return name.empty();
}

bool SceneTransition::exitCurrentScene(){
    return transition == Transition::EXIT_CURRENT_SCENE;
}

bool SceneTransition::exitAll(){
    return transition == Transition::EXIT_ALL;
}

bool SceneTransition::exitToBase(){
    return transition == Transition::EXIT_TO_BASE;
}

std::string SceneTransition::getName(){
    return name;
}

bool SceneTransition::triggered(sf::FloatRect& pos){
    if(!triggerFlag.empty() && !Inventory::instance().hasCollectable(triggerFlag))
        return false;
    return pos.intersects(trigger);
}

void SceneTransition::calibrateStartPos(sf::FloatRect& pos){
    if(trigger.width > DIRECTION_CUTOFF)
        startPos.x += pos.left - trigger.left;
    if(trigger.height > DIRECTION_CUTOFF)
        startPos.y += pos.top - trigger.top;
}

sf::Vector2f SceneTransition::getStartPos(){
    return startPos;
}
