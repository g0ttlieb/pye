#include "game_menu_scene.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../inventory/inventory.hpp"
#include "../inventory/save_manager.hpp"

const std::vector<std::string> KEYS {
    "forest_key",
};

const int MAIN_MENU_X = 287;
const int EXIT_X = 352;
const int QUIT_Y = 325;
const int QUIT_FONT_SIZE = 8;

const float WEAPON_LABEL_X = 110.5f;
const float LIGHT_LABEL_Y = 137.5f;
const float HEAVY_LABEL_Y = 191.5f;
const int WEAPON_LABEL_FONT_SIZE = 8;

const float WEAPON_ICON_SIZE = 32.f;
const float AVAILABLE_X_INIT = 129.f;
const float AVAILABLE_X_SPACING = 50.f;
const float AVAILABLE_Y = 62.f;

const float LIGHT_ARROW_Y = 133.f;
const float HEAVY_ARROW_Y = 187.f;
const float ARROW_WEAPON_X = 131.f;
const float ARROW_FIRST_MOD_X = 183.f;
const float ARROW_SECOND_MOD_X = 249.f;
const float ARROW_SPACING_X = 50.f;
const float ARROW_WIDTH = 12.f;
const float ARROW_HEIGHT = 8.f;
const float ARROW_FIRST_MOD_WIDTH = 28.f;

const float LIGHT_CHAIN_Y = 121.f;
const float HEAVY_CHAIN_Y = 175.f;
const float CHAIN_WEAPON_X = 148.f;
const float CHAIN_MOD_X = 214.f;
const float CHAIN_SPACING_X = 50.f;

const float KEYS_SIZE = 32;
const float KEYS_X = 103;
const float KEYS_Y = 231;
const float KEYS_SPACING_X = 42;
const float KEYS_SPACING_Y = 42;
const int KEYS_ROW_SIZE = 5;

const sf::Color IN_USE(100,100,100);
const sf::Color SELECTED(200,200,200);
const sf::Color NO_SHADE(255,255,255);

const int BUTTON_HIGHLIGHT_THICKNESS = 1;
const sf::Color BUTTON_HIGHLIGHT_COLOUR(116,111,22);

const int MAX_TAPES = 3;
const int TAPE_OFFSET = std::string("tape_").size();
const sf::Color TAPE_LOCKED(55,15,15,200);

const float TAPE_BOX_X = 337;
const float TAPE_BOX_Y = 231;
const float TAPE_BOX_WIDTH = 200;
const float TAPE_BOX_HEIGHT = 18;
const float TAPE_Y_SPACING = 28;
const float TAPE_TEXT_X_OFFSET = 24;;
const float TAPE_TEXT_Y_OFFSET = 9;
const int TAPE_TEXT_FONT_SIZE = 8;
const float TAPE_BUTTON_SIZE = 18;

const float WARP_TEXT_X = 320;
const float WARP_TEXT_Y = 35;
const float WARP_BUTTON_X = 288;
const float WARP_BUTTON_Y = 26;
const float WARP_BUTTON_WIDTH = 64;
const float WARP_BUTTON_HEIGHT= 18;
const int WARP_FONT_SIZE = 8;

const int QUAD = 4;

GameMenuScene::GameMenuScene(ButtonAreas buttonAreas):
            MenuScene("menu-game"),
            selected(nullptr){
    for(std::pair<std::string, sf::FloatRect> button : buttonAreas){
        if(button.first == "exit")
            baseButtons.push_back(Button{button.second, exitGame, nullptr});
        else if(button.first == "main_menu")
            baseButtons.push_back(Button{button.second, mainMenu,nullptr});
    }
    font.loadFromFile(ResourceManager::getFont("game_menu.ttf"));
    chainVertices.setPrimitiveType(sf::Quads);
    tapeVertices.setPrimitiveType(sf::Quads);
    setupWeaponState();
    setupExitText();
    setupWeaponLabelText();
    setupAvailableElements();
    setupKeys();
    setupTapes();
    setupWarp();
    setupButtons();
    updateChainVisuals();
}

GameMenuScene::~GameMenuScene(){
}

void GameMenuScene::setupWeaponState(){
    Inventory& inv = Inventory::instance();
    std::map<std::string,Element*>& elements = inv.getElements();
    int index = 0;
    for(auto current : elements){
        if(current.second->enabled)
            enabledElements[current.second] = index++;
    }
    setupWeaponChain(lightChain, inv.lightWeapon());
    setupWeaponChain(heavyChain, inv.heavyWeapon());
}

void GameMenuScene::setupWeaponChain(std::list<Element*>& chain, Weapon* weapon){
    if(weapon == nullptr)
        return;
    std::map<std::string,Element*>& elements = Inventory::instance().getElements();
    chain.push_back(elements[weapon->getElementName()]);
    Mod* mod = weapon->getMod();
    while(mod != nullptr){
        chain.push_back(elements[mod->getElementName()]);
        mod = mod->getNextMod();
    }
}

void GameMenuScene::setupExitText(){
    float scale = Settings::instance().scale();

    mainMenuText.setFont(font);
    mainMenuText.setFillColor(sf::Color::White);
    mainMenuText.setPosition(MAIN_MENU_X * scale, QUIT_Y * scale);
    mainMenuText.setCharacterSize(QUIT_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(mainMenuText, "MAIN MENU");

    exitText.setFont(font);
    exitText.setFillColor(sf::Color::White);
    exitText.setPosition(EXIT_X * scale, QUIT_Y * scale);
    exitText.setCharacterSize(QUIT_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(exitText, "EXIT");
}

void GameMenuScene::setupWeaponLabelText(){
    float scale = Settings::instance().scale();

    lightWeaponText.setFont(font);
    lightWeaponText.setFillColor(sf::Color::White);
    lightWeaponText.setPosition(WEAPON_LABEL_X * scale, LIGHT_LABEL_Y * scale);
    lightWeaponText.setCharacterSize(WEAPON_LABEL_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(lightWeaponText, "Light");

    heavyWeaponText.setFont(font);
    heavyWeaponText.setFillColor(sf::Color::White);
    heavyWeaponText.setPosition(WEAPON_LABEL_X * scale, HEAVY_LABEL_Y * scale);
    heavyWeaponText.setCharacterSize(WEAPON_LABEL_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(heavyWeaponText, "Heavy");
}

void GameMenuScene::setupAvailableElements(){
    weaponIconTexture.loadFromFile(ResourceManager::getVisual("elements.png"));
    availableVertices.setPrimitiveType(sf::Quads);
    availableVertices.resize(enabledElements.size() * QUAD);
    sf::FloatRect pos(0.f, AVAILABLE_Y, WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    sf::FloatRect tex(0.f, 0.f, WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    for(auto current : enabledElements){
        int vertexIndex = current.second * QUAD;
        int index = current.first->index;
        pos.left = AVAILABLE_X_INIT + (index * AVAILABLE_X_SPACING);
        tex.left = index * WEAPON_ICON_SIZE;
        SpriteHelper::setTextureQuad(&availableVertices[vertexIndex], pos, tex);
        shadeElement(current.first);
    }
}

void GameMenuScene::shadeElement(Element* element){
    int vertexIndex = enabledElements[element] * QUAD;
    if(element->inUse)
        SpriteHelper::shadeQuad(&availableVertices[vertexIndex], IN_USE);
    else if(element == selected)
        SpriteHelper::shadeQuad(&availableVertices[vertexIndex], SELECTED);
    else
        SpriteHelper::shadeQuad(&availableVertices[vertexIndex], NO_SHADE);
}

void GameMenuScene::setupKeys(){
    Inventory& inv = Inventory::instance();
    keysTexture.loadFromFile(ResourceManager::getVisual("keys.png"));
    keysVertices.setPrimitiveType(sf::Quads);
    int keysCount = 0;
    for(std::string current : KEYS){
        if(inv.hasCollectable(current))
            keysCount++;
    }
    keysVertices.resize(keysCount * QUAD);
    int index = 0;
    sf::FloatRect pos(0, 0, KEYS_SIZE, KEYS_SIZE);
    sf::FloatRect tex(0, 0, KEYS_SIZE, KEYS_SIZE);
    for(unsigned int i = 0; i < KEYS.size(); i++){
        if(!inv.hasCollectable(KEYS[i]))
            continue;
        pos.left = KEYS_X + ((i % KEYS_ROW_SIZE) * KEYS_SPACING_X);
        pos.top = KEYS_Y + ((i / KEYS_ROW_SIZE) * KEYS_SPACING_Y);
        tex.left = KEYS_SIZE * i;
        SpriteHelper::setTextureQuad(&keysVertices[index], pos, tex);
        index += QUAD;
    }
}

void GameMenuScene::setupTapes(){
    float scale = Settings::instance().scale();
    unsigned int tapeSize = Inventory::instance().getTapeDeckSize();
    std::vector<std::string>& tapes = Inventory::instance().getTapeDeck();
    unsigned int unavailableTapes = MAX_TAPES - tapeSize;

    tapeText.clear();
    tapeVertices.resize(unavailableTapes * QUAD);

    sf::FloatRect pos(TAPE_BOX_X, TAPE_BOX_Y, TAPE_BOX_WIDTH, TAPE_BOX_HEIGHT);
    for(unsigned int i = 0; i < MAX_TAPES; i++){
        if(i < tapes.size()){
            tapeText.push_back(sf::Text());
            tapeText.back().setFont(font);
            tapeText.back().setFillColor(sf::Color::White);
            tapeText.back().setPosition((pos.left + TAPE_TEXT_X_OFFSET) * scale,
                                        (pos.top  + TAPE_TEXT_Y_OFFSET) * scale);
            tapeText.back().setCharacterSize(TAPE_TEXT_FONT_SIZE* scale);
            SpriteHelper::leftAlignText(tapeText.back(), getTapeString(tapes[i]));
        }
        else if(i >= MAX_TAPES - unavailableTapes){
            int index = (i + unavailableTapes - MAX_TAPES) * QUAD;
            SpriteHelper::setColorQuad(&tapeVertices[index], pos, TAPE_LOCKED);
        }
        pos.top += TAPE_Y_SPACING;
    }
}

std::string GameMenuScene::getTapeString(std::string text){
    std::string newText;
    for(unsigned int i = TAPE_OFFSET; i < text.size(); i++){
        char current = text[i];
        if(current == '_')
            current = ' ';
        newText.push_back(current);
    }
    return newText;
}

void GameMenuScene::setupWarp(){
    float scale = Settings::instance().scale();
    warpText.setFont(font);
    warpText.setFillColor(sf::Color::White);
    warpText.setPosition(WARP_TEXT_X * scale, WARP_TEXT_Y * scale);
    warpText.setCharacterSize(WARP_FONT_SIZE * scale);
    SpriteHelper::centerAlignText(warpText, "WARP");
    warpEnabled = Inventory::instance().canWarp();
}

void GameMenuScene::setupButtons(){
    buttons.clear();
    for(auto entry : baseButtons)
        buttons.push_back(entry);
    setupTapeButtons();
    setupWarpButton();

    Inventory& inv = Inventory::instance();
    if(weaponActive(inv.lightWeapon()) || weaponActive(inv.heavyWeapon())){
        setupButtonHighlights();
        return;
    }

    if(selected == nullptr){
        setupAvailableButtons();
        setupChainButtons(LIGHT_CHAIN_Y, lightChain, clickLight);
        setupChainButtons(HEAVY_CHAIN_Y, heavyChain, clickHeavy);
    }
    else{
        setupArrowButtons(LIGHT_ARROW_Y, lightChain, clickLight);
        setupArrowButtons(HEAVY_ARROW_Y, heavyChain, clickHeavy);
    }
    setupButtonHighlights();
}

bool GameMenuScene::weaponActive(Weapon* weapon){
    if(weapon == nullptr)
        return false;
    return (weapon->onCooldown() || weapon->getFrame() > 0);
}

void GameMenuScene::setupAvailableButtons(){
    sf::FloatRect pos(0.f, AVAILABLE_Y, WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    for(auto current : enabledElements){
        if(current.first->inUse)
            continue;
        pos.left = current.first->index * AVAILABLE_X_SPACING + AVAILABLE_X_INIT;
        buttons.push_back(Button{pos, clickAvailable, current.first});
    }
}

void GameMenuScene::setupChainButtons(float y, std::list<Element*> chain,
        ButtonCallback callback){
    if(chain.empty())
        return;
    auto iter = chain.begin();
    sf::FloatRect pos = sf::FloatRect(CHAIN_WEAPON_X, y,
            WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    buttons.push_back(Button{pos, callback, *iter});
    iter++;
    pos.left = CHAIN_MOD_X;
    while(iter != chain.end()){
        buttons.push_back(Button{pos, callback, *iter});
        pos.left += CHAIN_SPACING_X;
        iter++;
    }
}

void GameMenuScene::setupArrowButtons(float y, std::list<Element*> chain,
        ButtonCallback callback){
    sf::FloatRect pos = sf::FloatRect(ARROW_WEAPON_X, y, ARROW_WIDTH,ARROW_HEIGHT);
    auto iter = chain.begin();
    buttons.push_back(Button{pos, callback, *iter});
    if(!chain.empty()){
        iter++;
        buttons.push_back(Button{sf::FloatRect(ARROW_FIRST_MOD_X, y,
                ARROW_FIRST_MOD_WIDTH, ARROW_HEIGHT), callback, *iter});
    }
    pos.left = ARROW_SECOND_MOD_X;
    while(iter != chain.end()){
        iter++;
        buttons.push_back(Button{pos, callback, *iter});
        pos.left += ARROW_SPACING_X;
    }
    buttons.back().data = nullptr;
}

void GameMenuScene::setupTapeButtons(){
    std::vector<std::string>& tapes = Inventory::instance().getTapeDeck();
    sf::FloatRect pos(TAPE_BOX_X, TAPE_BOX_Y, TAPE_BUTTON_SIZE, TAPE_BUTTON_SIZE);
    for(long i = 0; i < (long) tapes.size(); i++){
        buttons.push_back(Button{pos, clickTape, (void*) i});
        pos.top += TAPE_Y_SPACING;
    }
}

void GameMenuScene::setupWarpButton(){
    if(!warpEnabled)
        return;
    sf::FloatRect pos(WARP_BUTTON_X, WARP_BUTTON_Y, WARP_BUTTON_WIDTH,
            WARP_BUTTON_HEIGHT);
    buttons.push_back(Button{pos, warp, nullptr});
}

void GameMenuScene::setupButtonHighlights(){
    buttonHighlights.clear();
    for(unsigned int i = baseButtons.size(); i < buttons.size(); i++){
        if(buttons[i].callback == warp)
            continue;
        sf::FloatRect pos = buttons[i].pos;
        buttonHighlights.emplace_back(sf::Vector2f(pos.width, pos.height));
        buttonHighlights.back().setPosition(pos.left, pos.top);
        buttonHighlights.back().setOutlineColor(BUTTON_HIGHLIGHT_COLOUR);
        buttonHighlights.back().setOutlineThickness(BUTTON_HIGHLIGHT_THICKNESS);
        buttonHighlights.back().setFillColor(sf::Color::Transparent);
    }
}

SceneTransition GameMenuScene::escapeKeyScene(){
    if(selected != nullptr){
        Element* outgoing = selected;
        selected = nullptr;
        shadeElement(outgoing);
        setupButtons();
        return SceneTransition();
    }
    return SceneTransition(Transition::EXIT_CURRENT_SCENE);
}

void GameMenuScene::renderGame(sf::RenderWindow* window){
    MenuScene::renderGame(window);
    window->draw(exitText);
    window->draw(mainMenuText);
    window->draw(warpText);
    window->draw(lightWeaponText);
    window->draw(heavyWeaponText);

    float scale = Settings::instance().scale();
    sf::RenderStates state;
    state.transform.scale(scale, scale);
    for(auto current : buttonHighlights)
        window->draw(current, state);
    MenuScene::drawHighlight(window);

    for(auto current : tapeText)
        window->draw(current);
    window->draw(tapeVertices, state);

    state.texture = &weaponIconTexture;
    window->draw(chainVertices, state);
    window->draw(availableVertices, state);

    state.texture = &keysTexture;
    window->draw(keysVertices, state);
}

void GameMenuScene::selectElement(Element* element){
    selected = element;
    shadeElement(element);
    setupButtons();
}

void GameMenuScene::selectLight(Element* element){
    updateChain(lightChain, element, true);
}

void GameMenuScene::selectHeavy(Element* element){
    updateChain(heavyChain, element, false);
}

void GameMenuScene::updateChain(std::list<Element*>& chain, Element* element,
        bool isLight){
    if(selected == nullptr){
        element->inUse = false;
        removeFromChain(chain, element);
    }
    else{
        if(element == nullptr)
            chain.push_back(selected);
        else
            insertSelected(chain, element);
        selected->inUse = true;
        element = selected;
        selected = nullptr;
    }
    shadeElement(element);
    updateWeapon(chain, isLight);
    updateChainVisuals();
    setupButtons();
}

void GameMenuScene::removeFromChain(std::list<Element*>& chain, Element* element){
    for(auto i = chain.begin(); i != chain.end(); i++){
        if(*i == element){
            chain.erase(i);
            element->reset();
            return;
        }
    }
}

void GameMenuScene::insertSelected(std::list<Element*>& chain, Element* before){
    for(auto i = chain.begin(); i != chain.end(); i++){
        if(*i == before){
            chain.insert(i, selected);
            return;
        }
    }
}

void GameMenuScene::updateWeapon(std::list<Element*>& chain, bool isLight){
    Inventory& inv = Inventory::instance();
    Weapon* weapon;
    if(chain.empty())
        weapon = nullptr;
    else if(isLight)
        weapon = chain.front()->light;
    else
        weapon = chain.front()->heavy;

    if(!chain.empty()){
        auto iter = std::next(chain.begin());
        Mod* mod = nullptr;
        while(iter != chain.end()){
            Element* element = *iter;
            if(mod == nullptr)
                weapon->setMod(element->mod);
            else
                mod->setNextMod(element->mod);
            mod = element->mod;
            iter++;
        }
        if(mod == nullptr)
            weapon->setMod(nullptr);
        else
            mod->setNextMod(nullptr);
    }

    if(isLight)
        inv.setLightWeapon(weapon);
    else
        inv.setHeavyWeapon(weapon);
}

void GameMenuScene::updateChainVisuals(){
    chainVertices.resize((lightChain.size() + heavyChain.size()) * QUAD);
    updateChainVisual(LIGHT_CHAIN_Y, lightChain, 0);
    updateChainVisual(HEAVY_CHAIN_Y, heavyChain, lightChain.size());
}

void GameMenuScene::updateChainVisual(float y, std::list<Element*>& chain,
        int initIndex){
    int index = 0;
    bool doneWeapon = false;
    sf::FloatRect pos(0.f, y, WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    sf::FloatRect tex(0.f, 0.f, WEAPON_ICON_SIZE, WEAPON_ICON_SIZE);
    for(Element* element : chain){
        if(!doneWeapon){
            pos.left = CHAIN_WEAPON_X;
            doneWeapon = true;
        }
        else
            pos.left = CHAIN_MOD_X + (index-1) * CHAIN_SPACING_X;
        tex.left = element->index * WEAPON_ICON_SIZE;
        int vertexIndex = (initIndex + index) * QUAD;
        SpriteHelper::setTextureQuad(&chainVertices[vertexIndex], pos, tex);
        index++;
    }
}

void GameMenuScene::removeTape(int index){
    Inventory::instance().removeTape(index);
    setupTapes();
    setupButtons();
}

void GameMenuScene::clickAvailable(MenuScene& scene, void* data){
    GameMenuScene* gameMenu = dynamic_cast<GameMenuScene*>(&scene);
    gameMenu->selectElement((Element*) data);
}

void GameMenuScene::clickLight(MenuScene& scene, void* data){
    GameMenuScene* gameMenu = dynamic_cast<GameMenuScene*>(&scene);
    gameMenu->selectLight((Element*) data);
}
void GameMenuScene::clickHeavy(MenuScene& scene, void* data){
    GameMenuScene* gameMenu = dynamic_cast<GameMenuScene*>(&scene);
    gameMenu->selectHeavy((Element*) data);
}

void GameMenuScene::clickTape(MenuScene& scene, void* data){
    GameMenuScene* gameMenu = dynamic_cast<GameMenuScene*>(&scene);
    gameMenu->removeTape((long) data);
}

void GameMenuScene::warp(MenuScene& scene, void* data){
    (void) data;
    Inventory::instance().warp();
    scene.setSceneTransition(SceneTransition(Transition::EXIT_TO_BASE));
}

void GameMenuScene::exitGame(MenuScene& scene, void* data){
    (void) data;
    Inventory& inv = Inventory::instance();
    scene.setSceneTransition(SceneTransition(Transition::EXIT_ALL));
    if(inv.hasCollectable("invis_final_battle_lockdown") &&
            !inv.hasCollectable("end_cleared")){
        inv.resetRun();
        SaveManager::saveToStart();
    }
    else
        SaveManager::save();
}

void GameMenuScene::mainMenu(MenuScene& scene, void* data){
    (void) data;
    Inventory& inv = Inventory::instance();
    scene.setSceneTransition(SceneTransition("menu_main", Transition::EXIT_ALL));
    if(inv.hasCollectable("invis_final_battle_lockdown") &&
            !inv.hasCollectable("end_cleared")){
        inv.resetRun();
        SaveManager::saveToStart();
    }
    else
        SaveManager::save();
}
