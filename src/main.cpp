#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <cstdlib>
#include <ctime>
#include "game_manager.hpp"
#include "helpers/settings.hpp"
#include "helpers/resource_manager.hpp"

#ifdef _WIN32
#include <windows.h>
#endif

int main() {

#ifdef _WIN32
    ShowWindow(FindWindowA("ConsoleWindowClass", NULL), false);
#endif

    sf::Clock clock;
    GameManager gameManager;
    sf::RenderWindow window;

    srand(time(0));
    Settings::instance().setupWindow(&window);

    while (window.isOpen() && !gameManager.shouldQuit()){
        window.clear();
        gameManager.renderGame(&window);
        window.display();

        sf::Event event;
        while (window.pollEvent(event))
            gameManager.handleInput(event);

        sf::Time elapsed = clock.restart();
        gameManager.updateGame(elapsed.asSeconds());
    }

    return 0;
}
