#include "actrian_leader.hpp"
#include "../ai/boss_chase.hpp"
#include "../ai/stationary.hpp"

const float WIDTH = 13.f;               // Width of hitbox
const float HEIGHT = 29.f;              // Height of hitbox
const float X_OFFSET = 10.f;            // Left offset of hitbox in tile
const float Y_OFFSET = 19.f;            // Top offset of hitbox in tile
const float CENTER_OFFSET = 7.f;        // X center of hitbox from its left
const float SPRITE_TILE = 48.f;         // Tile size of spritesheet

const int MAX_HP = 750;                 // Health
const float X_VEL = 75.f;               // Horizontal speed
const float Y_VEL = 170.f;              // Vertical speed
const float SIGHT_RANGE = 300.f;        // X sight range to agro

const int N_WALKING_FRAMES = 10;        // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.1f;   // Seconds per frame in walking animation

const int N_RANGED_ATK_FRAMES = 22;         // Number of frames in animation
const float RANGED_ATK_FRAME_DUR = 0.1f;    // Seconds per frame in animation
const int RANGED_ATTACK_FRAME = 13;         // Frame to create attack on
const int RANGED_ATK_DAMAGE = 50;       // Damage of attack
const float RANGED_ATK_COOLDOWN = 4.f;  // Attack cooldown, starting immediately
const float RANGED_ATTACK_NEAR = 4.f;   // Reduce perceived range so not at max
const float RANGED_ATK_X_OFFSET = 25.f; // Left offset in hitbox to create attack
const float RANGED_ATK_Y_OFFSET = 0.f;  // Top offset in hitbox to create attack
const float RANGED_ATK_WIDTH = 6.f;     // Attack hitbox width
const float RANGED_ATK_HEIGHT = 26.f;   // Attack hitbox height
const float RANGED_ATK_X_VEL = 150.f;   // Horizontal speed of attack projectile
const float RANGED_ATK_Y_VEL = 0.f;     // Vertical speed of attack projectile
const float RANGED_ATK_GRAVITY = 0.f;   // Gravity coefficient on projectile

const int N_MELEE_ATK_FRAMES = 18;          // Number of frames in animation
const float MELEE_ATK_FRAME_DUR = 0.08f;    // Seconds per frame in animation
const int MELEE_ATTACK_FRAME = 11;          // Frame to create attack on
const int MELEE_ATK_DAMAGE = 100;       // Damage of attack
const float MELEE_ATK_COOLDOWN = 2.0f;  // Attack cooldown, starting immediately
const float MELEE_ATTACK_NEAR = 6.f;    // Reduce perceived range so not at max
const float MELEE_ATK_X_OFFSET = 14.f;  // Left offset in hitbox to create attack
const float MELEE_ATK_Y_OFFSET = 8.f;   // Top offset in hitbox to create attack
const float MELEE_ATK_WIDTH = 23.f;     // Attack hitbox width
const float MELEE_ATK_HEIGHT = 20.f;    // Attack hitbox height

const int RANGED_TEXTURE_ROW = 2;
const int MELEE_TEXTURE_ROW = 3;
const int MELEE_ATTACK_INDEX = 0.f;     // AttackIndex of melee
const float BIG_RANGE = 1000.f;         // Helper - infinite range

ActrianLeader::ActrianLeader(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Boss(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setActiveAiCallbacks(BossChase::Enabled, BossChase::Disabled);
    setPassiveAiCallbacks(Stationary::Enabled, Stationary::Disabled);
}

ActrianLeader::~ActrianLeader(){}

bool ActrianLeader::drawHealthBar(){
    return false;
}

float ActrianLeader::getSightRange(){
    return SIGHT_RANGE;
}

float ActrianLeader::getCenterOffset(){
    return CENTER_OFFSET;
}

bool ActrianLeader::attackHasVisual(){
    return true;
}

sf::FloatRect ActrianLeader::getAttackRange(){
    return sf::FloatRect();
}

int ActrianLeader::numberOfAttacks(){
    return 2;
}

void ActrianLeader::setAttack(int index){
    if(index == MELEE_ATTACK_INDEX){
        attackType = AttackType::MELEE;
        setAttackInfo(N_MELEE_ATK_FRAMES, MELEE_ATK_FRAME_DUR, MELEE_ATTACK_FRAME,
                      MELEE_ATK_COOLDOWN);
    }
    else{
        attackType = AttackType::RANGED;
        setAttackInfo(N_RANGED_ATK_FRAMES, RANGED_ATK_FRAME_DUR,
                      RANGED_ATTACK_FRAME, RANGED_ATK_COOLDOWN);
        createdAttacks = 0.f;
    }
}

bool ActrianLeader::hasNewAttack(){
    if(attackType == AttackType::MELEE)
        return Enemy::hasNewAttack();

    if(!areDead() && attacking && frame == RANGED_ATTACK_FRAME
                  && createdAttacks < 2){
        createdAttacks++;
        return true;
    }
    return false;
}

EnemyAttack* ActrianLeader::createAttack(){
    EnemyAttack* attack;
    if(attackType == AttackType::MELEE){
        attack = new EnemyAttack(MELEE_ATK_DAMAGE, getMeleeAttackHitbox());
        attack->setTexturePos(sf::FloatRect(0,0,0,0));
    }
    else{
        Direction dir = makeLeftDir(createdAttacks == 1);
        attack = new EnemyAttack(RANGED_ATK_DAMAGE, getRangedAttackHitbox(dir));
        attack->setProjectile(dir, RANGED_ATK_X_VEL, RANGED_ATK_Y_VEL,
                              RANGED_ATK_GRAVITY);
    }
    return attack;
}

std::vector<BossAttack> ActrianLeader::getAttackOptions(){
    std::vector<BossAttack> attacks;
    sf::FloatRect hitbox;

    hitbox = getMeleeAttackHitbox();
    hitbox.width -= MELEE_ATTACK_NEAR;
    if(xDirection == Direction::LEFT)
        hitbox.left += MELEE_ATTACK_NEAR;
    attacks.push_back(BossAttack{hitbox, true});

    hitbox = getRangedAttackHitbox(Direction::LEFT);
    hitbox.left -= RANGED_ATTACK_NEAR;
    hitbox.width = -BIG_RANGE;
    attacks.push_back(BossAttack{hitbox, false});

    hitbox = getRangedAttackHitbox(Direction::RIGHT);
    hitbox.left += RANGED_ATTACK_NEAR;
    hitbox.width = BIG_RANGE;
    attacks.push_back(BossAttack{hitbox, false});

    return attacks;
}

sf::FloatRect ActrianLeader::getMeleeAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + MELEE_ATK_Y_OFFSET,
            MELEE_ATK_WIDTH, MELEE_ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += MELEE_ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - MELEE_ATK_X_OFFSET - MELEE_ATK_WIDTH;
    return attackHitbox;
}

sf::FloatRect ActrianLeader::getRangedAttackHitbox(Direction dir){
    sf::FloatRect attackHitbox(position.left, position.top + RANGED_ATK_Y_OFFSET,
            RANGED_ATK_WIDTH, RANGED_ATK_HEIGHT);
    if(dir == Direction::RIGHT)
        attackHitbox.left += RANGED_ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - RANGED_ATK_X_OFFSET - RANGED_ATK_WIDTH;
    return attackHitbox;
}

void ActrianLeader::stagger(float timeStunned, float timeKnocked, Direction dir,
                            float velKnockBack, float velKnockUp){
    (void) velKnockUp;
    (void) velKnockBack;
    if(attacking)
        return;
    float stunnedTime = std::max(timeStunned, timeKnocked) / 2.f;
    if(stunnedTime > 0.2f)
        Enemy::stagger(stunnedTime, 0.f, dir, 0.f, 0.f);
}

void ActrianLeader::updateVisual(float dt){
    Enemy::updateVisual(dt);
    if(textureRow == RANGED_TEXTURE_ROW && attackType == AttackType::MELEE)
        textureRow = MELEE_TEXTURE_ROW;
}
