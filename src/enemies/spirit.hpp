# pragma once

#include "../characters/enemy.hpp"

class Spirit : public Enemy {
public:
    Spirit(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~Spirit();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();
    virtual float getAttackingVel();

    virtual float getSightRange();
    virtual float getCenterOffset();

    virtual void update(float dt, World& world);

    virtual void stagger(float timeStunned, float timeKnocked, Direction dir,
            float velKnockBack, float velKnockUp);
};
