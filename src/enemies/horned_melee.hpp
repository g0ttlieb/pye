# pragma once

#include "../characters/enemy.hpp"

class HornedMelee : public Enemy {
public:
    HornedMelee(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~HornedMelee();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();

private:
    sf::FloatRect getAttackHitbox();
};
