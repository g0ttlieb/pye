file(GLOB enemy_files "*.cpp")
add_library(enemies ${enemy_files})

target_link_libraries(enemies
    ai
    characters
)
