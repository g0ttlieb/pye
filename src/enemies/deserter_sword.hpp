# pragma once

#include "../characters/enemy.hpp"

class DeserterSword : public Enemy {
public:
    DeserterSword(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~DeserterSword();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();
private:
    sf::FloatRect getAttackHitbox();
};
