# pragma once

#include "../characters/enemy.hpp"

class SoldierSword : public Enemy {
public:
    SoldierSword(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~SoldierSword();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();
private:
    sf::FloatRect getAttackHitbox();
};
