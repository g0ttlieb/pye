#include "spirit.hpp"
#include "../ai/chase.hpp"
#include "../ai/loiter.hpp"

const float WIDTH = 10.f;               // Width of hitbox
const float HEIGHT = 14.f;              // Height of hitbox
const float X_OFFSET = 2.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 2.f;             // Top offset of hitbox in tile
const float CENTER_OFFSET = 5.f;        // X center of hitbox from its left
const float SPRITE_TILE = 16.f;         // Tile size of spritesheet

const int MAX_HP = 100;                 // Health
const float X_VEL = 70.f;               // Horizontal speed
const float Y_VEL = 150.f;              // Vertical speed
const float SLOW_FACTOR = 0.4f;         // Factor to slow all movement
const float SIGHT_RANGE = 180.f;        // X sight range to agro

const int N_WALKING_FRAMES = 5;         // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.18f * SLOW_FACTOR;    // Seconds per frame in
                                                        //   walking animation

const int N_ATK_FRAMES = 1;             // Number of frames in attack animation
const int ATTACK_FRAME = 0;             // Frame in animation to create attack on

const float ATK_COOLDOWN = 0.f;         // Attack cooldown, starting immediately
const float ATK_TIME = 0.05;            // Duration of each attack tick
const float ATK_DAMAGE = 50 * ATK_TIME; // Damage of attack / second
const float ATK_NEAR = 2.f;             // Reduce perceived hitbox so not always
                                        // at max range

Spirit::Spirit(float xPos, float yPos, Direction dir, sf::Vertex* tex):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, tex);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_TIME, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Chase::Enabled, Chase::Disabled);
    setPassiveAiCallbacks(Loiter::Enabled, Loiter::Disabled);
}

Spirit::~Spirit(){
}

bool Spirit::attackHasVisual(){
    return false;
}

EnemyAttack* Spirit::createAttack(){
    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, position);
    attack->setProjectile(Direction::RIGHT, 0, 0, 0); // So dont end on first hit
    attack->setDamageOverTime(ATK_TIME);
    return attack;
}

sf::FloatRect Spirit::getAttackRange(){
    sf::FloatRect attackHitbox = position;
    attackHitbox.width -= ATK_NEAR;
    if(xDirection == Direction::LEFT)
        attackHitbox.left += ATK_NEAR;
    return attackHitbox;
}

float Spirit::getAttackingVel(){
    return 0;
}

float Spirit::getSightRange(){
    return SIGHT_RANGE;
}

float Spirit::getCenterOffset(){
    return CENTER_OFFSET;
}

void Spirit::update(float dt, World& world){
    if(!areDead() && !attacking)
        dt *= SLOW_FACTOR;
    Enemy::update(dt, world);
}

void Spirit::stagger(float timeStunned, float timeKnocked, Direction dir,
                             float velKnockBack, float velKnockUp){
    (void) velKnockBack;
    (void) velKnockUp;
    float stunnedTime = std::max(timeStunned, timeKnocked) / 2.f;
    Enemy::stagger(stunnedTime, 0.f, dir, 0.f, 0.f);
}
