# pragma once

#include "../characters/enemy.hpp"

class SoldierAxe : public Enemy {
public:
    SoldierAxe(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~SoldierAxe();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();
private:
    sf::FloatRect getAttackHitbox();
};
