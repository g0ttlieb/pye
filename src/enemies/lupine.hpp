# pragma once

#include "../characters/enemy.hpp"

class Lupine : public Enemy {
public:
    Lupine(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~Lupine();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();
    virtual float getAttackingVel();

    virtual float getSightRange();
    virtual float getCenterOffset();

    virtual void stagger(float timeStunned, float timeKnocked, Direction dir,
            float velKnockBack, float velKnockUp);
    virtual void onDeath();

private:
    EnemyAttack* currentAttack;
    sf::FloatRect getAttackHitbox();
};
