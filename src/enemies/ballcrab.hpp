# pragma once

#include "../characters/enemy.hpp"

class Ballcrab : public Enemy {
public:
    Ballcrab(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~Ballcrab();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();
    virtual float getAttackingVel();

    virtual float getSightRange();
    virtual float getCenterOffset();

private:
    sf::FloatRect getAttackHitbox();
};
