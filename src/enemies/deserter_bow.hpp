# pragma once

#include "../characters/enemy.hpp"

class DeserterBow : public Enemy {
public:
    DeserterBow(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~DeserterBow();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();

    virtual void update(float dt, World& world);

private:
    sf::FloatRect getAttackHitbox();
};
