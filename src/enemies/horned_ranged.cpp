#include "horned_ranged.hpp"
#include "../ai/scared.hpp"
#include "../ai/patrol.hpp"

const float WIDTH = 7.f;                // Width of hitbox
const float HEIGHT = 14.f;              // Height of hitbox
const float X_OFFSET = 1.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 2.f;             // Top offset of hitbox in tile
const float CENTER_OFFSET = 4.5f;       // X center of hitbox from its left
const float SPRITE_TILE = 16.f;         // Tile size of spritesheet

const int MAX_HP = 40;                  // Health
const float X_VEL = 70.f;               // Horizontal speed
const float Y_VEL = 150.f;              // Vertical speed
const float SIGHT_RANGE = 140.f;        // X sight range to agro

const int N_WALKING_FRAMES = 8;         // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.1f;   // Seconds per frame in walking animation
const int N_ATK_FRAMES = 8;             // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.08f;      // Seconds per frame in attack animation
const int ATTACK_FRAME = 7;             // Frame in animation to create attack on

const float ATK_HEIGHT = 3.f;           // Attack hitbox height
const float ATK_WIDTH = 8.f;            // Attack hitbox width
const float ATK_X_OFFSET = 6.f;         // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = 5.f;         // Top offset in hitbox to create attack
const float ATK_X_VEL = 150.f;          // Horizontal speed of attack projectile
const float ATK_Y_VEL = 10.f;           // Vertical speed of attack projectile
const float ATK_GRAVITY = 0.05f;        // Gravity coefficient on projectile

const int ATK_DAMAGE = 10;              // Damage of attack
const float ATK_COOLDOWN = 1.f;         // Attack cooldown, starting immediately
const float ATK_CD_REDUCTION = 0.75f;   // Percent attack cooldown reduces while
                                        //   stationary

const float BIG_RANGE = 1000.f;                 // Helper - infinite range
const float INVALID_POS = -100.f;               // Helper - impossible pos
const std::string ATTACK_POS = "attack_pos";    // Helper - state key for last
                                                //   position that attacked

HornedRanged::HornedRanged(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Scared::Enabled, Scared::Disabled);
    setPassiveAiCallbacks(Patrol::Enabled, Patrol::Disabled);
}

HornedRanged::~HornedRanged(){}

float HornedRanged::getCenterOffset(){
    return CENTER_OFFSET;
}

bool HornedRanged::attackHasVisual(){
    return true;
}

EnemyAttack* HornedRanged::createAttack(){
    std::map<std::string, float>& state = getState();
    if(state[ATTACK_POS] == position.left){
        attackCooldown *= ATK_CD_REDUCTION;
        attackRemainingCooldown = attackCooldown;
    }
    else
        state[ATTACK_POS] = position.left;

    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    attack->setProjectile(xDirection, ATK_X_VEL, ATK_Y_VEL, ATK_GRAVITY);
    return attack;
}

sf::FloatRect HornedRanged::getAttackRange(){
    sf::FloatRect attackHitbox = getAttackHitbox();
    attackHitbox.width = xDirection * BIG_RANGE;
    return attackHitbox;
}

sf::FloatRect HornedRanged::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float HornedRanged::getSightRange(){
    return SIGHT_RANGE;
}

void HornedRanged::update(float dt, World& world){
    std::map<std::string, float>& state = getState();
    if(state[ATTACK_POS] != position.left){
        state[ATTACK_POS] = INVALID_POS;
        attackCooldown = ATK_COOLDOWN;
    }
    Enemy::update(dt, world);
}
