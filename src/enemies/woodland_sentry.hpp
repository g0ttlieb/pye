# pragma once

#include "../characters/enemy.hpp"

class WoodlandSentry : public Enemy {
public:
    WoodlandSentry(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~WoodlandSentry();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();
    virtual void stagger(float timeStunned, float timeKnocked, Direction dir,
                         float velKnockBack, float velKnockUp);
    virtual void onDeath();

private:
    EnemyAttack* currentAttack;
    sf::FloatRect getAttackHitbox();
};
