# pragma once

#include "../characters/enemy.hpp"

class HornedRanged : public Enemy {
public:
    HornedRanged(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~HornedRanged();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();

    virtual void update(float dt, World& world);

private:
    sf::FloatRect getAttackHitbox();
};
