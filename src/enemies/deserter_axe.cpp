#include "deserter_axe.hpp"
#include "../ai/chase.hpp"
#include "../ai/loiter.hpp"

const float WIDTH = 6.f;                // Width of hitbox
const float HEIGHT = 12.f;              // Height of hitbox
const float X_OFFSET = 4.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 4.f;             // Top offset of hitbox in tile
const float CENTER_OFFSET = 3.f;        // X center of hitbox from its left
const float SPRITE_TILE = 16.f;         // Tile size of spritesheet

const int MAX_HP = 55;                  // Health
const float X_VEL = 70.f;               // Horizontal speed
const float Y_VEL = 167.5f;             // Vertical speed
const float SIGHT_RANGE = 150.f;        // X sight range to agro

const int N_WALKING_FRAMES = 10;        // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.1f;   // Seconds per frame in walking animation

const int N_ATK_FRAMES = 14;            // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.07f;      // Seconds per frame in attack animation
const int ATTACK_FRAME = 8;             // Frame in animation to create attack on

const float ATK_HEIGHT = 12.f;          // Attack hitbox height
const float ATK_WIDTH = 10.f;           // Attack hitbox width
const float ATK_X_OFFSET = 4.f;         // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = 0.f;         // Top offset in hitbox to create attack

const int ATK_DAMAGE = 20;              // Damage of attack
const float ATK_COOLDOWN = 2.5f;        // Attack cooldown, starting immediately
const float ATTACK_NEAR = 6.f;          // Reduce pereived range so not always
                                        //   at max range

DeserterAxe::DeserterAxe(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Chase::Enabled, Chase::Disabled);
    setPassiveAiCallbacks(Loiter::Enabled, Loiter::Disabled);
}

DeserterAxe::~DeserterAxe(){}

float DeserterAxe::getCenterOffset(){
    return CENTER_OFFSET;
}

bool DeserterAxe::attackHasVisual(){
    return false;
}

EnemyAttack* DeserterAxe::createAttack(){
    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    return attack;
}

sf::FloatRect DeserterAxe::getAttackRange(){
    sf::FloatRect attackHitbox = getAttackHitbox();
    attackHitbox.width -= ATTACK_NEAR;
    if(xDirection == Direction::LEFT)
        attackHitbox.left += ATTACK_NEAR;
    return attackHitbox;
}

sf::FloatRect DeserterAxe::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float DeserterAxe::getSightRange(){
    return SIGHT_RANGE;
}
