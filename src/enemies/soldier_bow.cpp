#include "soldier_bow.hpp"
#include "../ai/scared.hpp"
#include "../ai/patrol.hpp"

const float WIDTH = 5.f;                // Width of hitbox
const float HEIGHT = 12.f;              // Height of hitbox
const float X_OFFSET = 4.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 4.f;             // Top offset of hitbox in tile
const float CENTER_OFFSET = 3.f;        // X center of hitbox from its left
const float SPRITE_TILE = 16.f;         // Tile size of spritesheet

const int MAX_HP = 50;                  // Health
const float X_VEL = 60.f;               // Horizontal speed
const float Y_VEL = 150.f;              // Vertical speed
const float SIGHT_RANGE = 140.f;        // X sight range to agro

const int N_WALKING_FRAMES = 10;        // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.1f;   // Seconds per frame in walking animation

const int N_ATK_FRAMES = 11;            // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.06f;      // Seconds per frame in attack animation
const int ATTACK_FRAME = 7;             // Frame in animation to create attack on

const float ATK_HEIGHT = 3.f;           // Attack hitbox height
const float ATK_WIDTH = 8.f;            // Attack hitbox width
const float ATK_X_OFFSET = 4.f;         // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = 6.f;         // Top offset in hitbox to create attack
const float ATK_X_VEL = 150.f;          // Horizontal speed of attack projectile
const float ATK_Y_VEL = 10.f;           // Vertical speed of attack projectile
const float ATK_GRAVITY = 0.05f;        // Gravity coefficient on projectile

const int ATK_DAMAGE = 10;              // Damage of attack
const float ATK_COOLDOWN = 2.f;         // Attack cooldown, starting immediately
const int ATK_FLURRY = 3;               // Number of attacks in a row

const float BIG_RANGE = 1000.f;                     // Helper - infinite range
const std::string ATTACK_POS = "attack_pos";        // Last pos attacked
const std::string ATTACK_COUNT = "attack_flurry";   // Number of attacks in a row
const std::string ATTACK_MOVED = "attack_moved";    // Have moved since last attack

SoldierBow::SoldierBow(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Scared::Enabled, Scared::Disabled);
    setPassiveAiCallbacks(Patrol::Enabled, Patrol::Disabled);
}

SoldierBow::~SoldierBow(){}

float SoldierBow::getCenterOffset(){
    return CENTER_OFFSET;
}

bool SoldierBow::attackHasVisual(){
    return true;
}

EnemyAttack* SoldierBow::createAttack(){
    std::map<std::string, float>& state = getState();
    if(state[ATTACK_MOVED]){
        state[ATTACK_POS] = position.left;
        state[ATTACK_MOVED] = false;
    }
    state[ATTACK_COUNT]++;
    if(state[ATTACK_COUNT] < ATK_FLURRY)
        attackRemainingCooldown = 0.f;
    else
        state[ATTACK_COUNT] = 0;

    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    attack->setProjectile(xDirection, ATK_X_VEL, ATK_Y_VEL, ATK_GRAVITY);
    return attack;
}

sf::FloatRect SoldierBow::getAttackRange(){
    sf::FloatRect attackHitbox = getAttackHitbox();
    attackHitbox.width = xDirection * BIG_RANGE;
    return attackHitbox;
}

sf::FloatRect SoldierBow::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float SoldierBow::getSightRange(){
    return SIGHT_RANGE;
}

void SoldierBow::update(float dt, World& world){
    std::map<std::string, float>& state = getState();
    if(state[ATTACK_POS] != position.left && !state[ATTACK_MOVED]){
        state[ATTACK_MOVED] = true;
        state[ATTACK_COUNT] = 0;
        attackRemainingCooldown = attackCooldown;
    }
    Enemy::update(dt, world);
}
