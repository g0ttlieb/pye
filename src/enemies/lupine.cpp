#include "lupine.hpp"
#include "../ai/chase.hpp"
#include "../ai/loiter.hpp"

const float TILE_Y = 7.99f;

const float WIDTH = 20.f;               // Width of hitbox
const float HEIGHT = 12.f;              // Height of hitbox
const float X_OFFSET = 6.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 20.f;            // Top offset of hitbox in tile
const float CENTER_OFFSET = 10.5f;      // X center of hitbox from its left
const float SPRITE_TILE = 32.f;         // Tile size of spritesheet

const int MAX_HP = 75;                  // Health
const float X_VEL = 90.f;               // Horizontal speed
const float Y_VEL = 120.f;              // Vertical speed
const float ATTACKING_VEL = 120.f;            // Velocity while attacking
const float SIGHT_RANGE = 200.f;        // X sight range to agro

const int N_WALKING_FRAMES = 8;         // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.08f;  // Seconds per frame in walking animation

const int N_ATK_FRAMES = 13;            // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.08f;      // Seconds per frame in attack animation
const int ATTACK_FRAME = 3;             // Frame in animation to create attack on
const int ATTACK_FRAME_LAST = 10;       // Last animation frame of attack

const float ATK_HEIGHT = 8.f;           // Attack hitbox height
const float ATK_WIDTH = 4.f;            // Attack hitbox width
const float ATK_X_OFFSET = 16.f;        // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = -2.f;        // Top offset in hitbox to create attack

const int ATK_DAMAGE = 25;              // Damage of attack
const float ATK_COOLDOWN = 1.5f;        // Attack cooldown, starting immediately

// Calculate how far to persist moving attack
const int N_ATK_DAMAGE_FRAMES = ATTACK_FRAME_LAST - ATTACK_FRAME;
const float ATK_LEAP_RANGE = X_VEL * N_ATK_DAMAGE_FRAMES * ATK_FRAME_DUR;
const float ATTACK_NEAR = 16.f;

Lupine::Lupine(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL),
            currentAttack(nullptr){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Chase::Enabled, Chase::Disabled);
    setPassiveAiCallbacks(Loiter::Enabled, Loiter::Disabled);
}

Lupine::~Lupine(){}

float Lupine::getCenterOffset(){
    return CENTER_OFFSET;
}

bool Lupine::attackHasVisual(){
    return false;
}

EnemyAttack* Lupine::createAttack(){
    getState()["MOVE_TOWARDS_X"] = -1; // Clear cache so turn around when overshoot
    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    attack->setProjectile(xDirection, ATTACKING_VEL, 0, 0);
    attack->setMaxRange(ATK_LEAP_RANGE);
    currentAttack = attack;
    return attack;
}

sf::FloatRect Lupine::getAttackRange(){
    sf::FloatRect attackHitbox(position.left, getFeetY() - TILE_Y,
            WIDTH + ATK_LEAP_RANGE - ATTACK_NEAR, TILE_Y);
    if(xDirection == Direction::LEFT)
        attackHitbox.left -= ATK_LEAP_RANGE - ATTACK_NEAR;
    return attackHitbox;
}

sf::FloatRect Lupine::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float Lupine::getAttackingVel(){
    if(frame>= ATTACK_FRAME)
        return ATTACKING_VEL;
    return 0;
}

float Lupine::getSightRange(){
    return SIGHT_RANGE;
}

void Lupine::stagger(float timeStunned, float timeKnocked, Direction dir,
                     float velKnockBack, float velKnockUp){
    if(!attacking || frame < ATTACK_FRAME || frame > ATTACK_FRAME_LAST)
        Enemy::stagger(timeStunned, timeKnocked, dir, velKnockBack, velKnockUp);
}

void Lupine::onDeath(){
    if(attacking && frame < ATTACK_FRAME_LAST && currentAttack)
        currentAttack->setFinished();
}
