# pragma once

#include "../characters/enemy.hpp"

class SoldierBow : public Enemy {
public:
    SoldierBow(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~SoldierBow();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();

    virtual void update(float dt, World& world);

private:
    sf::FloatRect getAttackHitbox();
};
