#include "woodland_sentry.hpp"
#include "../ai/sentry.hpp"
#include "../ai/stationary.hpp"

const float WIDTH = 24.f;               // Width of hitbox
const float HEIGHT = 22.f;              // Height of hitbox
const float X_OFFSET = 4.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 10.f;            // Top offset of hitbox in tile
const float CENTER_OFFSET = 12.f;       // X center of hitbox from its left
const float SPRITE_TILE = 32.f;         // Tile size of spritesheet

const int MAX_HP = 150;                 // Health
const float X_VEL = 0.f;                // Horizontal speed
const float Y_VEL = 0.f;                // Vertical speed
const float SIGHT_RANGE = 360.f;        // X sight range to agro

const int N_WALKING_FRAMES = 0;         // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.f;    // Seconds per frame in walking animation

const int N_ATK_FRAMES = 9;             // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.15f;      // Seconds per frame in attack animation
const int ATTACK_FRAME = 3;             // Frame in animation to create attack on

const float ATK_HEIGHT = 5.f;           // Attack hitbox height
const float ATK_WIDTH = 0.f;            // Attack hitbox width
const float ATK_X_OFFSET = 9.f;         // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = 12.f;        // Top offset in hitbox to create attack
const float ATK_X_VEL = 0.0f;           // Horizontal speed of attack projectile
const float ATK_Y_VEL = 0.f;            // Vertical speed of attack projectile
const float ATK_GRAVITY = 0.f;          // Gravity coefficient on projectile

const int ATK_DAMAGE = 75;                                  // Damage of attack
const int N_ATTACKING_FRAMES = 4;                           // Frames of attack
const float ATK_TIME = N_ATTACKING_FRAMES * ATK_FRAME_DUR;  // Duration of attack
const float ATK_COOLDOWN = 3.f;                             // Attack CD, starting
                                                            //   immediately

const float BIG_RANGE = 1000.f;         // Helper

WoodlandSentry::WoodlandSentry(float xPos, float yPos, Direction dir,
        sf::Vertex* tex):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL),
            currentAttack(nullptr){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, tex);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_COOLDOWN);
    setActiveAiCallbacks(Sentry::Enabled, Sentry::Disabled);
    setPassiveAiCallbacks(Stationary::Enabled, Stationary::Disabled);
}

WoodlandSentry::~WoodlandSentry(){
}

float WoodlandSentry::getCenterOffset(){
    return CENTER_OFFSET;
}

bool WoodlandSentry::attackHasVisual(){
    return true;
}

EnemyAttack* WoodlandSentry::createAttack(){
    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    attack->setProjectile(xDirection, ATK_X_VEL, ATK_Y_VEL, ATK_GRAVITY);
    attack->setExtendTillWall();
    attack->setDamageOverTime(ATK_TIME);
    currentAttack = attack;
    return attack;
}

sf::FloatRect WoodlandSentry::getAttackRange(){
    sf::FloatRect attackHitbox = getAttackHitbox();
    attackHitbox.width = xDirection * BIG_RANGE;
    return attackHitbox;
}

sf::FloatRect WoodlandSentry::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float WoodlandSentry::getSightRange(){
    return SIGHT_RANGE;
}

void WoodlandSentry::stagger(float timeStunned, float timeKnocked, Direction dir,
                             float velKnockBack, float velKnockUp){
    (void) timeStunned;
    (void) timeKnocked;
    (void) dir;
    (void) velKnockBack;
    (void) velKnockUp;
}

void WoodlandSentry::onDeath(){
    if(attacking && frame < (ATTACK_FRAME + N_ATTACKING_FRAMES) && currentAttack)
        currentAttack->setFinished();
}
