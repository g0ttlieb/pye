# pragma once

#include "../characters/enemy.hpp"

class DeserterAxe : public Enemy {
public:
    DeserterAxe(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~DeserterAxe();

    virtual bool attackHasVisual();
    virtual EnemyAttack* createAttack();
    virtual sf::FloatRect getAttackRange();

    virtual float getSightRange();
    virtual float getCenterOffset();
private:
    sf::FloatRect getAttackHitbox();
};
