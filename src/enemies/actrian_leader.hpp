# pragma once

#include "../characters/boss.hpp"

class ActrianLeader : public Boss {
public:
    ActrianLeader(float xPos, float yPos, Direction dir, sf::Vertex* texture);
    virtual ~ActrianLeader();

    virtual bool drawHealthBar();
    virtual float getSightRange();
    virtual float getCenterOffset();
    virtual bool attackHasVisual();
    virtual sf::FloatRect getAttackRange();
    virtual int numberOfAttacks();

    virtual void setAttack(int index);
    virtual bool hasNewAttack();
    virtual EnemyAttack* createAttack();
    virtual std::vector<BossAttack> getAttackOptions();

    virtual void stagger(float timeStunned, float timeKnocked, Direction dir,
            float velKnockBack, float velKnockUp);

private:
    enum class AttackType {
        MELEE, RANGED
    } attackType;
    int createdAttacks;
    sf::FloatRect getMeleeAttackHitbox();
    sf::FloatRect getRangedAttackHitbox(Direction dir);

    virtual void updateVisual(float dt);
};
