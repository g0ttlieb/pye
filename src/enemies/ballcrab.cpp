#include "ballcrab.hpp"
#include "../helpers/ai_helpers.hpp"
#include "../ai/scared.hpp"
#include "../ai/loiter.hpp"

const float WIDTH = 8.f;                // Width of hitbox
const float HEIGHT = 7.f;               // Height of hitbox
const float X_OFFSET = 4.f;             // Left offset of hitbox in tile
const float Y_OFFSET = 9.f;             // Top offset of hitbox in tile
const float CENTER_OFFSET = 4.f;        // X center of hitbox from its left
const float SPRITE_TILE = 16.f;         // Tile size of spritesheet

const int MAX_HP = 25;                  // Health
const float X_VEL = 80.f;               // Horizontal speed
const float Y_VEL = 180.f;              // Vertical speed
const float SIGHT_RANGE = 240.f;        // X sight range to agro

const int N_WALKING_FRAMES = 4;         // Number of frames in walking animation
const float WALKING_FRAME_DUR = 0.06f;  // Seconds per frame in walking animation

const int N_ATK_FRAMES = 7;             // Number of frames in attack animation
const float ATK_FRAME_DUR = 0.085f;     // Seconds per frame in attack animation
const int ATTACK_FRAME = 6;             // Frame in animation to create attack on
const int ATTACKING_MOVE_START = 4;     // Frame when movement starts
const float ATTACKING_VEL = 10.f;       // Move speed when attacking

const float ATK_HEIGHT = 2.f;           // Attack hitbox height
const float ATK_WIDTH = 2.f;            // Attack hitbox width
const float ATK_X_OFFSET = 8.f;         // Left offset in hitbox to create attack
const float ATK_Y_OFFSET = -3.f;        // Top offset in hitbox to create attack
const float ATK_X_VEL = 150.f;          // Horizontal speed of attack projectile
const float ATK_Y_VEL = 80.f;           // Vertical speed of attack projectile
const float ATK_GRAVITY = 0.4f;         // Gravity coefficient on projectile

const float ATK_RANGE_HEIGHT = 32.f;    // Perceived height of attack range
const float ATK_RANGE_WIDTH = 200.f;    // Perceived width = distance of attack

const int ATK_DAMAGE = 5;               // Damage of attack
const float ATK_CD_MAX = 2.5f;          // Max attack cd, starting immediately
const float ATK_CD_MIN = 1.f;           // Min attack cd, starting immediately

const float BIG_RANGE = 1000.f;         // Helper - infinite range

Ballcrab::Ballcrab(float xPos, float yPos, Direction dir,
        sf::Vertex* texture):
            Enemy(xPos, yPos, WIDTH, HEIGHT, X_OFFSET, Y_OFFSET, X_VEL, Y_VEL){
    setMaxHp(MAX_HP);
    setDirection(dir);
    setTexture(SPRITE_TILE, texture);
    setWalkingInfo(N_WALKING_FRAMES, WALKING_FRAME_DUR);
    setAttackInfo(N_ATK_FRAMES, ATK_FRAME_DUR, ATTACK_FRAME, ATK_CD_MAX);
    setActiveAiCallbacks(Scared::Enabled, Scared::Disabled);
    setPassiveAiCallbacks(Loiter::Enabled, Loiter::Disabled);
}

Ballcrab::~Ballcrab(){}

float Ballcrab::getCenterOffset(){
    return CENTER_OFFSET;
}

bool Ballcrab::attackHasVisual(){
    return true;
}

EnemyAttack* Ballcrab::createAttack(){
    attackRemainingCooldown = AiHelpers::getRandom(ATK_CD_MIN, ATK_CD_MAX);
    EnemyAttack* attack = new EnemyAttack(ATK_DAMAGE, getAttackHitbox());
    attack->setProjectile(xDirection, ATK_X_VEL, ATK_Y_VEL, ATK_GRAVITY);
    return attack;
}

sf::FloatRect Ballcrab::getAttackRange(){
    sf::FloatRect attackHitbox;
    attackHitbox.height = ATK_RANGE_HEIGHT;
    attackHitbox.top = position.top + position.height - ATK_RANGE_HEIGHT;
    attackHitbox.width = xDirection * ATK_RANGE_WIDTH;
    attackHitbox.left = position.left + position.width/2.f;
    return attackHitbox;
}

float Ballcrab::getAttackingVel(){
    if(frame >= ATTACKING_MOVE_START)
        return ATTACKING_VEL;
    return 0;
}

sf::FloatRect Ballcrab::getAttackHitbox(){
    sf::FloatRect attackHitbox(position.left, position.top + ATK_Y_OFFSET,
            ATK_WIDTH, ATK_HEIGHT);
    if(xDirection == Direction::RIGHT)
        attackHitbox.left += ATK_X_OFFSET;
    else
        attackHitbox.left += WIDTH - ATK_X_OFFSET - ATK_WIDTH;
    return attackHitbox;
}

float Ballcrab::getSightRange(){
    return SIGHT_RANGE;
}
