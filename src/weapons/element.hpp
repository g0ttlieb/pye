#pragma once

#include "mod.hpp"
#include "weapon.hpp"

struct Element {
    Weapon* light;
    Weapon* heavy;
    Mod* mod;

    bool inUse;
    bool enabled;
    int index;

    Element(Weapon* light, Weapon* heavy, Mod* mod, int index);
    ~Element();
    void reset();
    static std::map<std::string,Element*> getAllElements();
};
