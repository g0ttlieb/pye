#pragma once

#include <SFML/Graphics.hpp>
#include "../characters/enemy_group.hpp"
#include "../world/world.hpp"

class Mod: public sf::Drawable, public sf::Transformable {
public:
    Mod();
    virtual ~Mod();

    virtual std::string getElementName() = 0;

    void setNextMod(Mod* mod);
    Mod* getNextMod();

    virtual void trigger(Enemy* enemy, Direction dir) = 0;
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    void updateResolution();
    virtual void updateVolume();
    virtual void reset();
protected:
    void triggerNextMod(Enemy* enemy, Direction dir);

private:
    Mod* nextMod;
};
