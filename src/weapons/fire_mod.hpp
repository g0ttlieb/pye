#pragma once

#include <list>
#include "mod.hpp"

struct Fire{
    int initFrame;
    float burnTime;
    Direction dir;
};

class FireMod: public Mod {
public:
    FireMod();
    ~FireMod();

    virtual std::string getElementName();

    virtual void trigger(Enemy* enemy, Direction dir);
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    virtual void reset();

private:
    sf::Texture fireTexture;
    sf::VertexArray fireVertices;
    std::map<Enemy*, Fire> fires;
    void updateFires(float dt);
    void setupVisual();

    sf::FloatRect getFirePos(Enemy* enemy);
};
