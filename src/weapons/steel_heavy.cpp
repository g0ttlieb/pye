#include "steel_heavy.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"

const int DAMAGE = 40;
const float COOLDOWN = 4.0f;
const float PLAYER_SPEED = 10.f;

const float KNOCKBACK_TIME = 0.5f;
const float KNOCKBACK_VEL = 50.f;
const float KNOCKUP_VEL = 150.f;

const float FRAME_DURATION = 0.075f;
const int N_FRAMES = 14;
const int SPRITE_X = 32;
const int SPRITE_Y = 32;

const int CANCEL_CUTOFF = 8;
const int DAMAGE_FRAME = 9;

const int ICON_INDEX = 1;

const float ATTACK_HEIGHT = 10.f;
const float ATTACK_WIDTH = 16.f;
const float VERT_OFFSET = 2.f;
const float RIGHT_OFFSET = 2.f;
const float LEFT_OFFSET = ATTACK_WIDTH + RIGHT_OFFSET;

SteelHeavy::SteelHeavy():
        Weapon("steel-heavy", COOLDOWN, FRAME_DURATION, N_FRAMES,
                SPRITE_X, SPRITE_Y){
    soundBuffer.loadFromFile(ResourceManager::getEffect("steel-heavy.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
}

SteelHeavy::~SteelHeavy(){}

std::string SteelHeavy::getElementName(){
    return "steel";
}

bool SteelHeavy::applyGravity(){
    return true;
}

float SteelHeavy::xVelocity(){
    if(getFrame() < DAMAGE_FRAME)
        return PLAYER_SPEED;
    return 0.f;
}

bool SteelHeavy::isCancellable(){
    return Weapon::getFrame() < CANCEL_CUTOFF;
}

int SteelHeavy::getIconIndex(){
    return ICON_INDEX;
}

void SteelHeavy::attack(sf::FloatRect& position, Direction direction, bool moving){
    (void) moving;
    Weapon::attack(position, direction, false);
    appliedDamage = false;
    pos = &position;
}

void SteelHeavy::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    if(!appliedDamage && getFrame() == DAMAGE_FRAME){
        appliedDamage = true;
        calculateAttackSize();
        hitEnemies(enemies);
        sound.play();
    }
    Weapon::update(dt, enemies, world);
}

void SteelHeavy::calculateAttackSize(){
    float leftPos;
    if(attackDirection == Direction::RIGHT)
        leftPos = pos->left + RIGHT_OFFSET;
    else
        leftPos = pos->left + pos->width - LEFT_OFFSET;
    attackHitbox = sf::FloatRect(leftPos, attackOrigin.top + VERT_OFFSET,
            ATTACK_WIDTH, ATTACK_HEIGHT);
}

void SteelHeavy::hitEnemies(std::vector<EnemyGroup*>& enemies){
    for(auto enemyGroup : enemies){
        for(auto enemy : enemyGroup->getEnemies()){
            if(!enemy->areDead() && enemy->getPos().intersects(attackHitbox)){
                enemy->dealDamage(DAMAGE);
                enemy->stagger(0.f, KNOCKBACK_TIME, attackDirection,
                               KNOCKBACK_VEL, KNOCKUP_VEL);
                triggerMod(enemy);
            }
        }
    }
}

void SteelHeavy::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}
