#include "fire_light.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const int DAMAGE = 30;
const float SHOT_X_VEL = 180.f;
const float SHOT_Y_VEL = -20.f;
const float SHOT_Y_ACCEL = 100.f;
const float COOLDOWN = 2.5f;

const int SPRITE_X = 16;
const int SPRITE_Y = 16;
const float FRAME_DURATION = 0.08f;
const int N_FRAMES = 13;
const float SHOT_FRAME_DURATION = 0.1f;
const int SHOT_N_FRAMES = 6;
const float EXPLOSION_FRAME_DURATION = 0.1f;
const int EXPLOSION_N_FRAMES = 3;
const int SOUND_FRAME = 3;

const int UNCANCELLABLE_START_FRAME = 6;
const int UNCANCELLABLE_END_FRAME = 9;
const int ATTACK_FRAME = 9;

const int ICON_INDEX = 4;

const float X_OFFSET = 4.f;
const float Y_OFFSET = -5.f;
const float SHOT_DIM = 8.f;
const float EXPLOSION_DIM = 16.f;
const float SIZE_CHANGE = (EXPLOSION_DIM - SHOT_DIM) / 2.f;

FireLight::FireLight():
        Weapon("fire-light", COOLDOWN, FRAME_DURATION, N_FRAMES,
               SPRITE_X, SPRITE_Y){
    soundBuffer.loadFromFile(ResourceManager::getEffect("fire-light.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
    shotTexture.loadFromFile(ResourceManager::getVisual("fire-light-shot.png"));
    shotVertices.setPrimitiveType(sf::Quads);
}

FireLight::~FireLight(){}

std::string FireLight::getElementName(){
    return "fire";
}

bool FireLight::isCancellable(){
    int frame = getFrame();
    return (frame < UNCANCELLABLE_START_FRAME || frame > UNCANCELLABLE_END_FRAME);
}

int FireLight::getIconIndex(){
    return ICON_INDEX;
}

void FireLight::attack(sf::FloatRect& position, Direction direction, bool moving){
    (void) moving;
    Weapon::attack(position, direction, false);
    createdShot = false;
    playedSound = false;
}

void FireLight::cancelAttack(){
    if(playedSound && getFrame() < UNCANCELLABLE_END_FRAME)
        sound.stop();
    Weapon::cancelAttack();
}

void FireLight::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    int frame = getFrame();
    if(frame == ATTACK_FRAME && !createdShot)
        createShot();
    if(frame == SOUND_FRAME && !playedSound){
        sound.play();
        playedSound = true;
    }
    updateShots(dt, world);
    hitEnemies(enemies);
    updateExplosions(dt);
    damageEnemies(enemies);
    prepVisual();
    Weapon::update(dt, enemies, world);
}

void FireLight::createShot(){
    createdShot = true;
    float leftPos = attackOrigin.left;
    if(attackDirection == Direction::RIGHT)
        leftPos += X_OFFSET;
    else
        leftPos += attackOrigin.width - SHOT_DIM - X_OFFSET;
    sf::FloatRect hitbox(leftPos, attackOrigin.top + Y_OFFSET, SHOT_DIM, SHOT_DIM);
    activeShots.emplace_back(hitbox, SHOT_Y_VEL, attackDirection);
}

void FireLight::updateShots(float dt, World& world){
    for(auto shot = activeShots.begin(); shot != activeShots.end(); ) {
        shot->elapsed += dt;
        shot->hitbox.left += shot->dir * SHOT_X_VEL * dt;
        shot->hitbox.top += shot->yVel * dt;
        shot->yVel += SHOT_Y_ACCEL * dt;

        float x = shot->hitbox.left;
        float y = shot->hitbox.top;
        bool topLeft = world.isObstacle(x, y);
        bool topRight = world.isObstacle(x + SHOT_DIM, y);
        bool botLeft = world.isObstacle(x, y + SHOT_DIM);
        bool botRight = world.isObstacle(x + SHOT_DIM, y + SHOT_DIM);
        if(topLeft || topRight || botLeft || botRight){
            createExplosion(*shot);
            shot = activeShots.erase(shot);
        }
        else
            shot++;
    }
}

void FireLight::hitEnemies(std::vector<EnemyGroup*>& enemies){
    for(auto shot = activeShots.begin(); shot != activeShots.end(); ) {
        if(shotHitEnemies(*shot, enemies)){
            createExplosion(*shot);
            shot = activeShots.erase(shot);
        }
        else
            shot++;
    }
}

bool FireLight::shotHitEnemies(Shot& shot, std::vector<EnemyGroup*>& enemies){
    for(auto enemyGroup : enemies){
        for(auto enemy : enemyGroup->getEnemies()){
            if(!enemy->areDead() && enemy->getPos().intersects(shot.hitbox))
                return true;
        }
    }
    return false;
}

void FireLight::createExplosion(Shot& shot){
    activeExplosions.push_back(shot);
    activeExplosions.back().elapsed = 0.f;
    activeExplosions.back().hitbox.left -= SIZE_CHANGE;
    activeExplosions.back().hitbox.top -= SIZE_CHANGE;
    activeExplosions.back().hitbox.width = EXPLOSION_DIM;
    activeExplosions.back().hitbox.height = EXPLOSION_DIM;
}

void FireLight::updateExplosions(float dt){
    for(auto explosion = activeExplosions.begin();
             explosion != activeExplosions.end(); ) {
        explosion->elapsed += dt;
        int frame = explosion->elapsed / EXPLOSION_FRAME_DURATION;
        if(frame >= EXPLOSION_N_FRAMES)
            explosion = activeExplosions.erase(explosion);
        else
            explosion++;
    }
}

void FireLight::damageEnemies(std::vector<EnemyGroup*>& enemies){
    for(auto& explosion : activeExplosions){
        if(explosion.doneDamage)
            continue;
        explosion.doneDamage = true;
        for(auto enemyGroup : enemies){
            for(auto enemy : enemyGroup->getEnemies()){
                if(!enemy->areDead() &&
                        enemy->getPos().intersects(explosion.hitbox)){
                    enemy->dealDamage(DAMAGE);
                    triggerMod(enemy);
                }
            }
        }
    }
}

void FireLight::prepVisual(){
    shotVertices.resize(4 * (activeShots.size() + activeExplosions.size()));
    int index = 0;
    for(auto shot : activeShots){
        int frame = ((int) (shot.elapsed / SHOT_FRAME_DURATION)) % SHOT_N_FRAMES;
        sf::FloatRect tex(frame * SHOT_DIM, 0, shot.dir * SHOT_DIM, SHOT_DIM);
        if(shot.dir == Direction::LEFT)
            tex.left += SHOT_DIM;
        SpriteHelper::setTextureQuad(&shotVertices[index], shot.hitbox, tex);
        index += 4;
    }
    for(auto explosion : activeExplosions){
        int frame = explosion.elapsed / EXPLOSION_FRAME_DURATION;
        sf::FloatRect tex(frame * EXPLOSION_DIM, SHOT_DIM,
                          explosion.dir * EXPLOSION_DIM, EXPLOSION_DIM);
        SpriteHelper::setTextureQuad(&shotVertices[index], explosion.hitbox, tex);
        index += 4;
    }
}

void FireLight::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    states.texture = &shotTexture;
    target.draw(shotVertices, states);
    Weapon::draw(target, states);
}

void FireLight::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}

void FireLight::reset(){
    activeShots.clear();
    activeExplosions.clear();
    createdShot = false;
    shotVertices.resize(0);
}
