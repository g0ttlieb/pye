#include "gunpowder_light.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const int DAMAGE = 10;
const float CANCEL_COOLDOWN = 2.f;
const float SHOT_SPEED = 200.f;
const float PLAYER_SPEED = 85.f;
const float MOVING_CD = 1.f;
const float STATIONARY_CD = 1.5f;

const int SPRITE_X = 16;
const int SPRITE_Y = 16;
const float MOVING_FRAME_DURATION = 0.06f;
const int MOVING_N_FRAMES = 14;
const float STATIONARY_FRAME_DURATION = 0.08f;
const float STATIONARY_N_FRAMES = 12;

const int MOVING_ATTACK_1_FRAME = 3;
const int MOVING_ATTACK_2_FRAME = 5;
const int MOVING_ATTACK_3_FRAME = 7;
const int STATIONARY_ATTACK_1_FRAME = 3;
const int STATIONARY_ATTACK_2_FRAME = 5;
const int STATIONARY_ATTACK_3_FRAME = 7;
const int STATIONARY_ATTACK_4_FRAME = 9;

const int ICON_INDEX = 2;

const float SHOT_HEIGHT = 1.f;
const float SHOT_WIDTH = 3.f;
const float MOVING_Y_OFFSET = 6.f;
const float MOVING_X_OFFSET = 2.f;
const float MOVING_UPPER_HAND_Y_OFFSET = 0.f;
const float MOVING_LOWER_HAND_Y_OFFSET = 2.f;
const float STATIONARY_Y_OFFSET = 4.f;
const float STATIONARY_X_OFFSET = 1.f;
const float STATIONARY_UPPER_HAND_X_OFFSET = 2.f;
const float STATIONARY_UPPER_HAND_Y_OFFSET = 0.f;
const float STATIONARY_LOWER_HAND_X_OFFSET = 0.f;
const float STATIONARY_LOWER_HAND_Y_OFFSET = 1.f;

GunpowderLight::GunpowderLight():
        Weapon("gunpowder-light", 0.f, 0.f, 0.f, SPRITE_X, SPRITE_Y){
    soundBuffer.loadFromFile(ResourceManager::getEffect("gunpowder-light.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
    shotTexture.loadFromFile(
            ResourceManager::getVisual("gunpowder-light-shot.png"));
    shotTexture.setRepeated(true);
    shotVertices.setPrimitiveType(sf::Quads);
}

GunpowderLight::~GunpowderLight(){}

std::string GunpowderLight::getElementName(){
    return "gunpowder";
}

bool GunpowderLight::applyGravity(){
    return attackMoving;
}

float GunpowderLight::xVelocity(){
    if(attackMoving)
        return PLAYER_SPEED;
    return 0.f;
}

bool GunpowderLight::isCancellable(){
    return true;
}

int GunpowderLight::getIconIndex(){
    return ICON_INDEX;
}

void GunpowderLight::attack(sf::FloatRect& position, Direction direction,
        bool moving){
    if(moving)
        updateAttackParams(MOVING_FRAME_DURATION, MOVING_N_FRAMES, MOVING_CD);
    else
        updateAttackParams(STATIONARY_FRAME_DURATION, STATIONARY_N_FRAMES,
                           STATIONARY_CD);
    Weapon::attack(position, direction, moving);
    createdShot = false;
    pos = &position;
}

void GunpowderLight::cancelAttack(){
    Weapon::cancelAttack();
    remainingCooldown = CANCEL_COOLDOWN;
}

void GunpowderLight::update(float dt, std::vector<EnemyGroup*>& enemies,
        World& world){
    if(attackMoving)
        createShotMoving();
    else
        createShotStationary();
    hitEnemies(enemies);
    moveShots(dt, world);
    prepShotVisual();
    Weapon::update(dt, enemies, world);
}

void GunpowderLight::createShotMoving(){
    int frame = getFrame();
    if(frame == MOVING_ATTACK_1_FRAME || frame == MOVING_ATTACK_3_FRAME){
        if(!createdShot)
            addShot(MOVING_X_OFFSET, MOVING_Y_OFFSET + MOVING_UPPER_HAND_Y_OFFSET);
    }
    else if(frame == MOVING_ATTACK_2_FRAME){
        if(!createdShot)
            addShot(MOVING_X_OFFSET, MOVING_Y_OFFSET + MOVING_LOWER_HAND_Y_OFFSET);
    }
    else
        createdShot = false;
}

void GunpowderLight::createShotStationary(){
    int frame = getFrame();
    if(frame == STATIONARY_ATTACK_1_FRAME || frame == STATIONARY_ATTACK_3_FRAME){
        if(!createdShot)
            addShot(STATIONARY_X_OFFSET + STATIONARY_UPPER_HAND_X_OFFSET,
                    STATIONARY_Y_OFFSET + STATIONARY_UPPER_HAND_Y_OFFSET);
    }
    else if(frame == STATIONARY_ATTACK_2_FRAME ||
            frame == STATIONARY_ATTACK_4_FRAME){
        if(!createdShot)
            addShot(STATIONARY_X_OFFSET + STATIONARY_LOWER_HAND_X_OFFSET,
                    STATIONARY_Y_OFFSET + STATIONARY_LOWER_HAND_Y_OFFSET);
    }
    else
        createdShot = false;
}

void GunpowderLight::addShot(float xOffset, float yOffset){
    createdShot = true;
    float yPos = pos->top + yOffset;
    float xPos = pos->left + xOffset;
    if(attackDirection == Direction::LEFT)
        xPos += pos->width - SHOT_WIDTH - 2*xOffset;
    sf::FloatRect hitbox(xPos, yPos, SHOT_WIDTH, SHOT_HEIGHT);
    activeShots.emplace_back(hitbox, attackDirection);
    sound.play();
}

void GunpowderLight::moveShots(float dt, World& world){
    for(auto shot = activeShots.begin(); shot != activeShots.end(); ) {
        shot->hitbox.left += shot->dir * SHOT_SPEED * dt;
        bool topLeft = world.isObstacle(shot->hitbox.left, shot->hitbox.top);
        bool topRight = world.isObstacle(shot->hitbox.left + shot->hitbox.width,
                shot->hitbox.top);
        bool botLeft = world.isObstacle(shot->hitbox.left,
                shot->hitbox.top + shot->hitbox.height);
        bool botRight = world.isObstacle(shot->hitbox.left + shot->hitbox.width,
                shot->hitbox.top + shot->hitbox.height);
        if(topLeft || topRight || botLeft || botRight)
            shot = activeShots.erase(shot);
        else
            shot++;
    }
}

void GunpowderLight::hitEnemies(std::vector<EnemyGroup*>& enemies){
    for(auto shot = activeShots.begin(); shot != activeShots.end(); ) {
        bool hit = false;
        for(auto enemyGroup : enemies){
            for(auto enemy : enemyGroup->getEnemies()){
                if(!enemy->areDead() && enemy->getPos().intersects(shot->hitbox)){
                    enemy->dealDamage(DAMAGE);
                    triggerMod(enemy);
                    hit = true;
                    break;
                }
            }
            if(hit)
                break;
        }
        if(hit)
            shot = activeShots.erase(shot);
        else
            shot++;
    }
}

void GunpowderLight::prepShotVisual(){
    shotVertices.resize(4 * activeShots.size());
    int index = 0;
    for(Shot shot : activeShots){
        sf::FloatRect tex(0, 0, shot.dir * SHOT_WIDTH, SHOT_HEIGHT);
        SpriteHelper::setTextureQuad(&shotVertices[index], shot.hitbox, tex);
        index += 4;
    }
}

void GunpowderLight::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    states.texture = &shotTexture;
    target.draw(shotVertices, states);
    Weapon::draw(target, states);
}

void GunpowderLight::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}

void GunpowderLight::reset(){
    activeShots.clear();
    createdShot = false;
    shotVertices.resize(0);
}
