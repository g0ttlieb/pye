#include "steel_light.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"

const int MOVING_DAMAGE = 25;
const int STATIONARY_DAMAGE = 15;
const float MOVING_CD = 1.5f;
const float STATIONARY_CD = 0.1f;
const float PLAYER_SPEED = 40.f;
const float MOVEMENT_CUTOFF = 11;

const float STUN_TIME = 0.2f;
const float KNOCKBACK_TIME = 0.10f;
const float KNOCKBACK_VEL = 10.f;

const int SPRITE_X = 32;
const int SPRITE_Y = 16;
const int MOVING_N_FRAMES = 16;
const float MOVING_FRAME_DURATION = 0.06f;
const int STATIONARY_N_FRAMES = 12;
const float STATIONARY_FRAME_DURATION = 0.035f;

const int MOVING_CANCEL_START = 12;
const int MOVING_DAMAGE_FRAME = 11;
const int STATIONARY_CANCEL_CUTOFF = 4;
const int STATIONARY_DAMAGE_FRAME = 8;

const int ICON_INDEX = 0;

const float MOVING_X_OFFSET = -2.f;
const float MOVING_Y_OFFSET = 6.f;
const float MOVING_ATTACK_WIDTH = 20.f;
const float MOVING_ATTACK_HEIGHT = 6.f;
const float STATIONARY_X_OFFSET = 2.f;
const float STATIONARY_Y_OFFSET = 2.f;
const float STATIONARY_ATTACK_HEIGHT = 10.f;
const float STATIONARY_ATTACK_WIDTH = 14.f;

SteelLight::SteelLight():
        Weapon("steel-light", 0, 0, 0, SPRITE_X, SPRITE_Y){
    soundBuffer.loadFromFile(ResourceManager::getEffect("steel-light.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
}

SteelLight::~SteelLight(){}

std::string SteelLight::getElementName(){
    return "steel";
}

bool SteelLight::applyGravity(){
    return attackMoving;
}

float SteelLight::xVelocity(){
    if(attackMoving && getFrame() < MOVEMENT_CUTOFF)
        return PLAYER_SPEED;
    return 0.f;
}

bool SteelLight::isCancellable(){
    if(attackMoving)
        return Weapon::getFrame() > MOVING_CANCEL_START;
    return Weapon::getFrame() < STATIONARY_CANCEL_CUTOFF;
}

int SteelLight::getIconIndex(){
    return ICON_INDEX;
}

void SteelLight::attack(sf::FloatRect& position, Direction direction, bool moving){
    if(moving)
        updateAttackParams(MOVING_FRAME_DURATION, MOVING_N_FRAMES, MOVING_CD);
    else
        updateAttackParams(STATIONARY_FRAME_DURATION, STATIONARY_N_FRAMES,
                           STATIONARY_CD);
    Weapon::attack(position, direction, moving);
    appliedDamage = false;
    pos = &position;
}

void SteelLight::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    int damageFrame = attackMoving ? MOVING_DAMAGE_FRAME : STATIONARY_DAMAGE_FRAME;
    if(!appliedDamage && getFrame() == damageFrame){
        appliedDamage = true;
        if(attackMoving)
            calculateAttackSize(*pos, MOVING_X_OFFSET, MOVING_Y_OFFSET,
                                MOVING_ATTACK_WIDTH, MOVING_ATTACK_HEIGHT);
        else
            calculateAttackSize(attackOrigin, STATIONARY_X_OFFSET,
                    STATIONARY_Y_OFFSET, STATIONARY_ATTACK_WIDTH,
                    STATIONARY_ATTACK_HEIGHT);
        hitEnemies(enemies);
        sound.play();
    }
    Weapon::update(dt, enemies, world);
}

void SteelLight::calculateAttackSize(sf::FloatRect& origin, float xOffset,
        float yOffset, float attackWidth, float attackHeight){
    float leftPos;
    if(attackDirection == Direction::RIGHT)
        leftPos = origin.left + xOffset;
    else
        leftPos = origin.left + attackOrigin.width - attackWidth - xOffset;
    attackHitbox = sf::FloatRect(leftPos, attackOrigin.top + yOffset,
                                 attackWidth, attackHeight);
}

void SteelLight::hitEnemies(std::vector<EnemyGroup*>& enemies){
    float damage = attackMoving ? MOVING_DAMAGE : STATIONARY_DAMAGE;
    for(auto enemyGroup : enemies){
        for(auto enemy : enemyGroup->getEnemies()){
            if(!enemy->areDead() && enemy->getPos().intersects(attackHitbox)){
                enemy->dealDamage(damage);
                enemy->stagger(STUN_TIME, KNOCKBACK_TIME, attackDirection,
                               KNOCKBACK_VEL, 0.f);
                triggerMod(enemy);
            }
        }
    }
}

void SteelLight::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}
