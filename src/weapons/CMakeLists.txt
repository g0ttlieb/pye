file(GLOB weapon_files "*.cpp")
add_library(weapons ${weapon_files})

target_link_libraries(weapons
    characters
    helpers
    world
)
