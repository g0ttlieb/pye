#pragma once

#include <SFML/Audio.hpp>
#include "weapon.hpp"

class SteelLight: public Weapon {
public:
    SteelLight();
    virtual ~SteelLight();

    virtual std::string getElementName();

    virtual bool applyGravity();
    virtual float xVelocity();

    virtual bool isCancellable();
    virtual int getIconIndex();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);

    virtual void updateVolume();
private:
    sf::SoundBuffer soundBuffer;
    sf::Sound sound;

    sf::FloatRect* pos;

    bool appliedDamage;
    sf::FloatRect attackHitbox;
    void calculateAttackSize(sf::FloatRect& origin, float xOffset, float yOffset,
            float attackWidth, float attackHeight);
    void calculateAttackSize();
    void hitEnemies(std::vector<EnemyGroup*>& enemies);
};
