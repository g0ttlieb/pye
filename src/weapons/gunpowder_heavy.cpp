#include "gunpowder_heavy.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const int DAMAGE = 25;
const float COOLDOWN = 4.0f;
const float CANCEL_COOLDOWN = 1.0f;

const float FRAME_DURATION = 0.1f;
const int N_FRAMES = 8;
const int SPRITE_X = 16;
const int SPRITE_Y = 16;
const int ICON_INDEX = 3;

const int DAMAGE_FRAME = 3;
const int VISUAL_CUTOFF = 4;

const float LEFT_OFFSET = 2.f;
const float RIGHT_OFFSET = 8.f;
const float VERT_OFFSET = 5.f;

const float SHOT_HEIGHT = 3.f;
const float SHOT_OFFSET = 1.f;
const float BLAST_HEIGHT = 9.f;
const float BLAST_WIDTH = 9.f;
const float HALF_BLAST_WIDTH = BLAST_WIDTH / 2.f;
const float BLAST_VERT_OFFSET = HALF_BLAST_WIDTH - 0.5;

const int BLAST_FRAMES = 3;
const float BLAST_FRAME_TIME = 0.1f;
const float BLAST_TIME = BLAST_FRAMES * BLAST_FRAME_TIME;

const float WIGGLE = 0.001f;
const float BIG_WIGGLE = 0.1f;
const float WORLD_PX = 16.f;
const int QUAD = 4;

GunpowderHeavy::GunpowderHeavy():
        Weapon("gunpowder-heavy", COOLDOWN, FRAME_DURATION, N_FRAMES,
                SPRITE_X, SPRITE_Y),
        blastTime(0.f){
    soundBuffer.loadFromFile(ResourceManager::getEffect("gunpowder-heavy.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
    shotTexture.loadFromFile(
            ResourceManager::getVisual("gunpowder-heavy-shot.png"));
    shotTexture.setRepeated(true);
    shotVertices.setPrimitiveType(sf::Quads);
}

GunpowderHeavy::~GunpowderHeavy(){}

std::string GunpowderHeavy::getElementName(){
    return "gunpowder";
}

bool GunpowderHeavy::isCancellable(){
    return (getFrame() < DAMAGE_FRAME) || (getFrame() > VISUAL_CUTOFF);
}

int GunpowderHeavy::getIconIndex(){
    return ICON_INDEX;
}

void GunpowderHeavy::attack(sf::FloatRect& position, Direction direction,
        bool moving){
    (void) moving;
    Weapon::attack(position, direction, false);
    appliedDamage = false;
    enemiesHit.clear();
}

void GunpowderHeavy::cancelAttack(){
    Weapon::cancelAttack();
    if(getFrame() < DAMAGE_FRAME)
        remainingCooldown = CANCEL_COOLDOWN;
}

void GunpowderHeavy::update(float dt, std::vector<EnemyGroup*>& enemies,
        World& world){
    if(!appliedDamage && getFrame() == DAMAGE_FRAME){
        appliedDamage = true;
        calculateShotSize(world);
        hitEnemies(enemies);
        prepShotVisual();
        sound.play();
        blastTime = BLAST_TIME;
    }
    if(blastTime > 0){
        blastTime -= dt;
        prepBlastVisual();
    }
    Weapon::update(dt, enemies, world);
}

void GunpowderHeavy::calculateShotSize(World& world){
    float vertPos = attackOrigin.top + VERT_OFFSET;
    float startPos = attackOrigin.left;
    float endPos = ((int) (startPos / WORLD_PX)) * WORLD_PX;
    while(!world.isObstacle(endPos + attackDirection*WIGGLE, vertPos))
        endPos += attackDirection * WORLD_PX;
    endPos -= BIG_WIGGLE;
    if(attackDirection == Direction::RIGHT)
        shotHitbox = sf::FloatRect(startPos + RIGHT_OFFSET, vertPos - SHOT_OFFSET,
                endPos - startPos - RIGHT_OFFSET, SHOT_HEIGHT);
    else
        shotHitbox = sf::FloatRect(endPos, vertPos - SHOT_OFFSET,
                startPos - endPos - LEFT_OFFSET, SHOT_HEIGHT);
}

void GunpowderHeavy::hitEnemies(std::vector<EnemyGroup*>& enemies){
    for(auto enemyGroup : enemies){
        for(auto enemy : enemyGroup->getEnemies()){
            if(!enemy->areDead() && enemy->getPos().intersects(shotHitbox)){
                enemy->dealDamage(DAMAGE);
                enemiesHit.insert(enemy);
                triggerMod(enemy);
            }
        }
    }
}

void GunpowderHeavy::prepShotVisual(){
    shotVertices.resize(QUAD);
    sf::FloatRect texturePos(0.f, 0.f, shotHitbox.width, shotHitbox.height);
    SpriteHelper::setTextureQuad(&shotVertices[0], shotHitbox, texturePos);
}

void GunpowderHeavy::prepBlastVisual(){
    int shotVis = getFrame() <= VISUAL_CUTOFF ? 1 : 0;
    shotVertices.resize((enemiesHit.size()+shotVis)*QUAD);
    int currentIndex = shotVis * QUAD;
    int frame = (BLAST_FRAMES-1) - (int) (blastTime / BLAST_FRAME_TIME);
    if(frame < 0)
        return;
    sf::FloatRect tex(frame * BLAST_WIDTH, SHOT_HEIGHT, BLAST_WIDTH, BLAST_HEIGHT);
    for(auto enemy : enemiesHit){
        float center = enemy->getPos().left + enemy->getCenterOffset();
        sf::FloatRect pos(center - HALF_BLAST_WIDTH,
                shotHitbox.top - BLAST_VERT_OFFSET, BLAST_WIDTH, BLAST_HEIGHT);
        SpriteHelper::setTextureQuad(&shotVertices[currentIndex], pos, tex);
        currentIndex += QUAD;
    }
}

void GunpowderHeavy::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    if(blastTime > 0){
        states.transform *= getTransform();
        states.texture = &shotTexture;
        target.draw(shotVertices, states);
    }
    Weapon::draw(target, states);
}

void GunpowderHeavy::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}

void GunpowderHeavy::reset(){
    appliedDamage = false;
    blastTime = 0.f;
    enemiesHit.clear();
    shotVertices.resize(0);
}
