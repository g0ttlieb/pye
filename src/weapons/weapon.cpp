#include "weapon.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../helpers/settings.hpp"

const int BASE_TILE_PX = 16;
const int NO_FRAME = -1;

Weapon::Weapon(std::string name, float cooldown, float frameDuration,
               float nFrames, int tileX, int tileY):
        attackMoving(false),
        remainingCooldown(0.f),
        mod(nullptr),
        frame(NO_FRAME),
        tileX(tileX),
        tileY(tileY),
        cooldown(cooldown),
        frameDuration(frameDuration),
        nFrames(nFrames),
        finishedTime(frameDuration * nFrames){
    texture.loadFromFile(ResourceManager::getVisual(name + ".png"));
    animationTime = finishedTime;
    updateResolution();
}

Weapon::~Weapon(){
}

bool Weapon::onCooldown(){
    return remainingCooldown > 0.f;
}

float Weapon::getCooldown(){
    return remainingCooldown;
}

void Weapon::resetCooldown(){
    remainingCooldown = 0.f;
    animationTime = finishedTime;
}

bool Weapon::requireGrounded(){
    return true;
}

bool Weapon::applyGravity(){
    return false;
}

float Weapon::xVelocity(){
    return 0.f;
}

void Weapon::setMod(Mod* newMod){
    mod = newMod;
}

Mod* Weapon::getMod(){
    return mod;
}

void Weapon::attack(sf::FloatRect& position, Direction direction, bool moving){
    frame = 0;
    animationTime = 0.f;
    attackOrigin = position;
    attackDirection = direction;
    attackMoving = moving;
    remainingCooldown = cooldown;
}

void Weapon::cancelAttack(){
    animationTime = finishedTime;
}

void Weapon::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    if(remainingCooldown > 0.f)
        remainingCooldown -= dt;
    if(animationTime < finishedTime){
        animationTime += dt;
        frame = (int) (animationTime / frameDuration);
    }
    else
        frame = NO_FRAME;
    if(mod != nullptr)
        mod->update(dt, enemies, world);
}

int Weapon::getFrame() const {
    return frame;
}

sf::Texture& Weapon::getTexture(){
    return texture;
}

bool Weapon::animationIsFinished(){
    return animationTime > finishedTime;
}

void Weapon::loadFrame(sf::Sprite& sprite, Direction direction){
    int row = attackMoving;
    SpriteHelper::setTextureTile(sprite, row, frame, tileX, tileY, direction);
    float xDif = 0.f;
    float yDif = 0.f;
    if(direction == Direction::LEFT)
        xDif = (tileX - BASE_TILE_PX);
    if(tileY > BASE_TILE_PX)
        yDif = (tileY - BASE_TILE_PX);
    sprite.move(-xDif, -yDif);
}

void Weapon::triggerMod(Enemy* enemy){
    if(mod != nullptr)
        mod->trigger(enemy, attackDirection);
}

void Weapon::updateAttackParams(float _frameDuration, int _nFrames, float cd){
    frameDuration = _frameDuration;
    nFrames = _nFrames;
    finishedTime = frameDuration * nFrames;
    cooldown = cd;
}

void Weapon::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    (void) states;
    if(mod != nullptr)
        target.draw(*mod);
}

void Weapon::updateResolution(){
    SpriteHelper::setScale(this);
}

void Weapon::updateVolume(){
}

void Weapon::reset(){
    remainingCooldown = 0.f;
    animationTime = finishedTime;
    frame = NO_FRAME;
}

void Weapon::pause(){
}

void Weapon::unpause(){
}
