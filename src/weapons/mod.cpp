#include "mod.hpp"
#include "../helpers/sprite_helper.hpp"

Mod::Mod(): nextMod(nullptr){
    updateResolution();
}

Mod::~Mod(){
}

void Mod::setNextMod(Mod* mod){
    nextMod = mod;
}

Mod* Mod::getNextMod(){
    return nextMod;
}

void Mod::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    if(nextMod != nullptr)
        nextMod->update(dt, enemies, world);
}

void Mod::triggerNextMod(Enemy* enemy, Direction dir){
    if(nextMod != nullptr)
        nextMod->trigger(enemy, dir);
}

void Mod::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    (void) states;
    if(nextMod != nullptr)
        target.draw(*nextMod);
}

void Mod::updateResolution(){
    SpriteHelper::setScale(this);
}

void Mod::updateVolume(){
}

void Mod::reset(){
}
