#pragma once

#include <SFML/Audio.hpp>
#include "weapon.hpp"

class FireHeavy: public Weapon {
public:
    FireHeavy();
    virtual ~FireHeavy();

    virtual std::string getElementName();

    virtual bool isCancellable();
    virtual int getIconIndex();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void cancelAttack();
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);

    virtual void pause();
    virtual void unpause();

    virtual void updateVolume();
private:
    sf::SoundBuffer soundBuffer;
    sf::Sound sound;
    bool soundPlayed;

    float attackTime;
    sf::FloatRect attackHitbox;
    void calculateAttackSize();
    void hitEnemies(std::vector<EnemyGroup*>& enemies, float dt);
};
