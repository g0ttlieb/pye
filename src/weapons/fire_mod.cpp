#include "fire_mod.hpp"
#include "../helpers/ai_helpers.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"

const int DAMAGE = 10;

const float VISUAL_DIAMETER = 11.f;
const float VISUAL_RADIUS = 5.5f;
const float VISUAL_FRAME_TIME = 0.12f;
const int VISUAL_FRAMES = 6;

const float BURN_TIME = 3.f;
const float DAMAGE_TICK_RATE = 0.1f;
const float MOD_TICK_RATE = 1.f;
const float BURN_DAMAGE_PER_TICK = DAMAGE / (BURN_TIME / DAMAGE_TICK_RATE);

const int QUAD = 4;

FireMod::FireMod(){
    fireTexture.loadFromFile(ResourceManager::getVisual("fire-mod.png"));
    fireVertices.setPrimitiveType(sf::Quads);
}

FireMod::~FireMod(){
}

std::string FireMod::getElementName(){
    return "fire";
}

void FireMod::trigger(Enemy* enemy, Direction dir){
    fires[enemy] = Fire{(int) AiHelpers::getRandom(0, VISUAL_FRAMES-1), 0.f, dir};
}

void FireMod::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    updateFires(dt);
    setupVisual();
    Mod::update(dt, enemies, world);
}

void FireMod::updateFires(float dt){
    for(auto fire = fires.begin(); fire != fires.end();){
        if(fire->first->areDead() || fire->second.burnTime > BURN_TIME){
            fire = fires.erase(fire);
            continue;
        }
        float oldTime = fire->second.burnTime;
        fire->second.burnTime += dt;
        float newTime = fire->second.burnTime;
        int damageTick = ((int) (newTime / DAMAGE_TICK_RATE)) -
                         ((int) (oldTime / DAMAGE_TICK_RATE));
        int modTick = ((int) (newTime / MOD_TICK_RATE)) -
                      ((int) (oldTime / MOD_TICK_RATE));
        for(int tick = 0; tick < damageTick; tick++)
            fire->first->dealDamage(BURN_DAMAGE_PER_TICK);
        for(int tick = 0; tick < modTick; tick++)
            triggerNextMod(fire->first, fire->second.dir);
        fire++;
    }
}

void FireMod::setupVisual(){
    fireVertices.resize(fires.size() * QUAD);
    sf::FloatRect tex(0, 0, VISUAL_DIAMETER, VISUAL_DIAMETER);
    int currentIndex = 0;
    for(auto current : fires){
        int frame = (((int) (current.second.burnTime / VISUAL_FRAME_TIME))
                    + current.second.initFrame) % VISUAL_FRAMES;
        sf::FloatRect pos = getFirePos(current.first);
        tex.left = frame * VISUAL_DIAMETER;
        SpriteHelper::setTextureQuad(&fireVertices[currentIndex], pos, tex);
        currentIndex += QUAD;
    }
}

sf::FloatRect FireMod::getFirePos(Enemy* enemy){
    sf::FloatRect& pos = enemy->getPos();
    return sf::FloatRect(pos.left + enemy->getCenterOffset() - VISUAL_RADIUS,
                         pos.top + pos.height/2.f - VISUAL_RADIUS,
                         VISUAL_DIAMETER, VISUAL_DIAMETER);
}

void FireMod::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    states.texture = &fireTexture;
    target.draw(fireVertices, states);
    Mod::draw(target, states);
}

void FireMod::reset(){
    fires.clear();
    fireVertices.resize(0);
}
