#pragma once

#include <list>
#include "mod.hpp"

class GunpowderMod: public Mod {
public:
    GunpowderMod();
    ~GunpowderMod();

    virtual std::string getElementName();

    virtual void trigger(Enemy* enemy, Direction dir);
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    virtual void reset();

private:
    Direction attackDir;
    std::list<Enemy*> newExplosions;
    void handleNewExplosions(std::vector<EnemyGroup*>& enemies);

    sf::Texture explosionTexture;
    sf::VertexArray explosionVertices;
    std::list<std::pair<Enemy*, float>> explosions;
    void updateExplosions(float dt);
    void updateTiming(float dt);
    void setupVisual();

    sf::Vector2f getEnemyCenter(Enemy* enemy);
};
