#include "steel_mod.hpp"

const float KNOCKBACK_TIME = 0.2f;
const float KNOCKBACK_VEL = 10.f;

SteelMod::SteelMod(){
}

SteelMod::~SteelMod(){
}

std::string SteelMod::getElementName(){
    return "steel";
}

void SteelMod::trigger(Enemy* enemy, Direction dir){
    enemy->stagger(0, KNOCKBACK_TIME, dir, KNOCKBACK_VEL, 0);
    triggerNextMod(enemy, dir);
}
