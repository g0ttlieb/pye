#include "gunpowder_mod.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/sprite_helper.hpp"

const int DAMAGE = 5;

const float VISUAL_DIAMETER = 9.f;
const float VISUAL_RADIUS = 4.5f;
const float VISUAL_FRAMES = 3;
const float VISUAL_FRAME_TIME = 0.1f;
const float VISUAL_TIME = VISUAL_FRAMES * VISUAL_FRAME_TIME;

const float BLAST_DIAMETER = 9.f;
const float BLAST_RADIUS = BLAST_DIAMETER / 2.f;

const int QUAD = 4;

GunpowderMod::GunpowderMod(){
    explosionTexture.loadFromFile(ResourceManager::getVisual("gunpowder-mod.png"));
    explosionVertices.setPrimitiveType(sf::Quads);
}

GunpowderMod::~GunpowderMod(){
}

std::string GunpowderMod::getElementName(){
    return "gunpowder";
}

void GunpowderMod::trigger(Enemy* enemy, Direction dir){
    attackDir = dir;
    newExplosions.push_back(enemy);
}

void GunpowderMod::update(float dt, std::vector<EnemyGroup*>& enemies,
        World& world){
    handleNewExplosions(enemies);
    updateExplosions(dt);
    Mod::update(dt, enemies, world);
}

void GunpowderMod::handleNewExplosions(std::vector<EnemyGroup*>& enemies){
    for(Enemy* current : newExplosions){
        sf::Vector2f center = getEnemyCenter(current);
        sf::FloatRect blast(center.x - BLAST_RADIUS, center.y - BLAST_RADIUS,
                BLAST_DIAMETER, BLAST_DIAMETER);
        for(auto enemyGroup : enemies){
            for(auto enemy : enemyGroup->getEnemies()){
                if(enemy != current && !enemy->areDead()
                        && enemy->getPos().intersects(blast)){
                    enemy->dealDamage(DAMAGE);
                    triggerNextMod(enemy, attackDir);
                }
            }
        }
        explosions.push_back(std::make_pair(current, VISUAL_TIME));
    }
    newExplosions.clear();
}

void GunpowderMod::updateExplosions(float dt){
    updateTiming(dt);
    setupVisual();
}

void GunpowderMod::updateTiming(float dt){
    for(auto i = explosions.begin(); i != explosions.end();){
        i->second -= dt;
        if(i->second < 0)
            i = explosions.erase(i);
        else
            i++;
    }
}

void GunpowderMod::setupVisual(){
    explosionVertices.resize(explosions.size() * QUAD);
    sf::FloatRect tex(0, 0, VISUAL_DIAMETER, VISUAL_DIAMETER);
    int currentIndex = 0;
    for(auto current : explosions){
        int frame = (VISUAL_FRAMES-1) - (int) (current.second / VISUAL_FRAME_TIME);
        sf::Vector2f center = getEnemyCenter(current.first);
        sf::FloatRect pos(center.x - VISUAL_RADIUS, center.y - VISUAL_RADIUS,
                VISUAL_DIAMETER, VISUAL_DIAMETER);
        tex.left = frame * VISUAL_DIAMETER;
        SpriteHelper::setTextureQuad(&explosionVertices[currentIndex], pos, tex);
        currentIndex += QUAD;
    }
}

sf::Vector2f GunpowderMod::getEnemyCenter(Enemy* enemy){
    sf::FloatRect& pos = enemy->getPos();
    return sf::Vector2f(pos.left + enemy->getCenterOffset(),
                        pos.top + pos.height/2);
}

void GunpowderMod::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    states.texture = &explosionTexture;
    target.draw(explosionVertices, states);
    Mod::draw(target, states);
}

void GunpowderMod::reset(){
    explosions.clear();
    explosionVertices.resize(0);
}
