#pragma once

#include <SFML/Audio.hpp>
#include <set>
#include "weapon.hpp"

class GunpowderHeavy: public Weapon {
public:
    GunpowderHeavy();
    virtual ~GunpowderHeavy();

    virtual std::string getElementName();

    virtual bool isCancellable();
    virtual int getIconIndex();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void cancelAttack();
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    virtual void updateVolume();
    virtual void reset();
private:
    sf::SoundBuffer soundBuffer;
    sf::Sound sound;

    bool appliedDamage;
    float blastTime;
    sf::FloatRect shotHitbox;
    std::set<Enemy*> enemiesHit;
    sf::Texture shotTexture;
    sf::VertexArray shotVertices;
    void calculateShotSize(World& world);
    void hitEnemies(std::vector<EnemyGroup*>& enemies);
    void prepShotVisual();
    void prepBlastVisual();
};
