#pragma once

#include <SFML/Audio.hpp>
#include "weapon.hpp"

class FireLight: public Weapon {
public:
    FireLight();
    virtual ~FireLight();

    virtual std::string getElementName();

    virtual bool isCancellable();
    virtual int getIconIndex();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void cancelAttack();
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    virtual void updateVolume();
    virtual void reset();
private:
    sf::SoundBuffer soundBuffer;
    sf::Sound sound;

    struct Shot {
        Shot(sf::FloatRect hitbox, float yVel, Direction dir):
            hitbox(hitbox), yVel(yVel), elapsed(0.f), doneDamage(false), dir(dir){}
        sf::FloatRect hitbox;
        float yVel;
        float elapsed;
        bool doneDamage;
        Direction dir;
    };
    std::list<Shot> activeShots;
    std::list<Shot> activeExplosions;

    bool playedSound;
    bool createdShot;
    sf::Texture shotTexture;
    sf::VertexArray shotVertices;
    void createShot();
    void updateShots(float dt, World& world);
    void hitEnemies(std::vector<EnemyGroup*>& enemies);
    bool shotHitEnemies(Shot& shot, std::vector<EnemyGroup*>& enemies);
    void createExplosion(Shot& shot);
    void updateExplosions(float dt);
    void damageEnemies(std::vector<EnemyGroup*>& enemies);
    void prepVisual();
};
