#pragma once

#include <SFML/Graphics.hpp>
#include "mod.hpp"
#include "../characters/enemy_group.hpp"
#include "../world/world.hpp"
#include "../helpers/direction.hpp"

class Weapon: public sf::Drawable, public sf::Transformable {
public:
    Weapon(std::string name, float cooldown, float frameDuration, float nFrames,
            int tileX, int tileY);
    virtual ~Weapon();

    virtual std::string getElementName() = 0;

    bool onCooldown();
    float getCooldown();
    void resetCooldown();

    virtual bool requireGrounded();
    virtual bool applyGravity();
    virtual float xVelocity();

    virtual bool isCancellable() = 0;
    virtual int getIconIndex() = 0;

    void setMod(Mod* mod);
    Mod* getMod();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void cancelAttack();
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    int getFrame() const;
    sf::Texture& getTexture();
    bool animationIsFinished();
    void loadFrame(sf::Sprite& sprite, Direction direction);

    void updateResolution();
    virtual void updateVolume();
    virtual void reset();
    virtual void pause();
    virtual void unpause();
protected:
    Direction attackDirection;
    sf::FloatRect attackOrigin;
    bool attackMoving;
    float remainingCooldown;

    void triggerMod(Enemy* enemy);
    void updateAttackParams(float frameDuration, int nFrames, float cooldown);
private:
    Mod* mod;
    int frame;
    int tileX;
    int tileY;
    float cooldown;
    float frameDuration;
    float nFrames;
    float animationTime;
    float finishedTime;
    sf::Texture texture;
};
