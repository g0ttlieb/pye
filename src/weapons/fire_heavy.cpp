#include "fire_heavy.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"

const int DAMAGE = 75.f;
const float COOLDOWN = 7.0f;

const float FRAME_DURATION = 0.1f;
const int N_FRAMES = 24;
const int SPRITE_X = 48;
const int SPRITE_Y = 16;

const int ICON_INDEX = 5;

const float ATTACK_HEIGHT = 12.f;
const float ATTACK_WIDTH = 32.f;
const float VERT_OFFSET = -2.f;     // From the top of the origin
const float HORIZ_OFFSET = 9.f;     // From the left of the origin

const int DAMAGE_START_FRAME = 3;   // First frame with damage
const int DAMAGE_END_FRAME = 22;    // Last frame with damage
const int ATTACK_GROWTH_FRAMES = 3; // How many frames it takes to go up / down
const float ATTACK_SIZE_CHANGE = ATTACK_WIDTH / (ATTACK_GROWTH_FRAMES + 1);

const float MOD_TICK_RATE = 0.5f;
const float DAMAGE_TICK_RATE = 0.1f;
const float DAMAGE_PER_TICK = DAMAGE / ((DAMAGE_END_FRAME - DAMAGE_START_FRAME) * FRAME_DURATION / DAMAGE_TICK_RATE);

const int SOUND_FRAME = 2;

FireHeavy::FireHeavy():
        Weapon("fire-heavy", COOLDOWN, FRAME_DURATION, N_FRAMES,
                SPRITE_X, SPRITE_Y){
    soundBuffer.loadFromFile(ResourceManager::getEffect("fire-heavy.wav"));
    sound.setBuffer(soundBuffer);
    updateVolume();
}

FireHeavy::~FireHeavy(){}

std::string FireHeavy::getElementName(){
    return "fire";
}

bool FireHeavy::isCancellable(){
    return true;
}

int FireHeavy::getIconIndex(){
    return ICON_INDEX;
}

void FireHeavy::attack(sf::FloatRect& position, Direction direction, bool moving){
    (void) moving;
    Weapon::attack(position, direction, false);
    attackTime = 0.f;
    soundPlayed = false;
    attackHitbox = sf::FloatRect();
}

void FireHeavy::cancelAttack(){
    sound.stop();
    Weapon::cancelAttack();
}

void FireHeavy::update(float dt, std::vector<EnemyGroup*>& enemies, World& world){
    int frame = getFrame();
    if(frame == SOUND_FRAME && !soundPlayed){
        sound.play();
        soundPlayed = true;
    }
    if(frame >= DAMAGE_START_FRAME && frame <= DAMAGE_END_FRAME){
        calculateAttackSize();
        hitEnemies(enemies, dt);
    }
    Weapon::update(dt, enemies, world);
}

void FireHeavy::calculateAttackSize(){
    float frame = getFrame();
    float hitboxOffset = 0.f;
    float attackWidth = ATTACK_WIDTH;
    if(frame < DAMAGE_START_FRAME + ATTACK_GROWTH_FRAMES)
        attackWidth = (frame - DAMAGE_START_FRAME + 1) * ATTACK_SIZE_CHANGE;
    else if(frame > DAMAGE_END_FRAME - ATTACK_GROWTH_FRAMES){
        attackWidth = (DAMAGE_END_FRAME - frame + 1) * ATTACK_SIZE_CHANGE;
        hitboxOffset = ATTACK_WIDTH - attackWidth;
    }

    float leftPos = attackOrigin.left;
    if(attackDirection == Direction::RIGHT)
        leftPos += HORIZ_OFFSET + hitboxOffset;
    else
        leftPos += attackOrigin.width - HORIZ_OFFSET - hitboxOffset - attackWidth;
    attackHitbox = sf::FloatRect(leftPos, attackOrigin.top + VERT_OFFSET,
            attackWidth, ATTACK_HEIGHT);
}

void FireHeavy::hitEnemies(std::vector<EnemyGroup*>& enemies, float dt){
    float oldTime = attackTime;
    attackTime += dt;
    int damageTick = ((int) (attackTime / DAMAGE_TICK_RATE)) -
                     ((int) (oldTime / DAMAGE_TICK_RATE));
    int modTick = ((int) (attackTime / MOD_TICK_RATE)) -
                  ((int) (oldTime / MOD_TICK_RATE));
    if(damageTick == 0 && modTick == 0)
        return;

    for(auto enemyGroup : enemies){
        for(auto enemy : enemyGroup->getEnemies()){
            if(!enemy->areDead() && enemy->getPos().intersects(attackHitbox)){
                for(int i = 0; i < damageTick; ++i)
                    enemy->dealDamage(DAMAGE_PER_TICK);
                for(int i = 0; i < modTick; ++i)
                    triggerMod(enemy);
            }
        }
    }
}

void FireHeavy::pause(){
    if(sound.getStatus() == sf::SoundSource::Status::Playing)
        sound.pause();
}

void FireHeavy::unpause(){
    if(sound.getStatus() == sf::SoundSource::Status::Paused)
        sound.play();
}

void FireHeavy::updateVolume(){
    sound.setVolume(Settings::instance().getEffectsVolume());
}
