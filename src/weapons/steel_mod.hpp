#pragma once

#include "mod.hpp"

class SteelMod: public Mod {
public:
    SteelMod();
    ~SteelMod();

    virtual std::string getElementName();

    virtual void trigger(Enemy* enemy, Direction dir);
};
