#include "element.hpp"

#include "gunpowder_light.hpp"
#include "gunpowder_heavy.hpp"
#include "gunpowder_mod.hpp"

#include "steel_light.hpp"
#include "steel_heavy.hpp"
#include "steel_mod.hpp"

#include "fire_light.hpp"
#include "fire_heavy.hpp"
#include "fire_mod.hpp"

Element::Element(Weapon* light, Weapon* heavy, Mod* mod, int index):
    light(light),
    heavy(heavy),
    mod(mod),
    inUse(false),
    enabled(false),
    index(index){
}

Element::~Element(){
    delete light;
    delete heavy;
    delete mod;
}

void Element::reset(){
    light->reset();
    heavy->reset();
    mod->reset();
}

std::map<std::string,Element*> Element::getAllElements(){
    std::map<std::string,Element*> elements;
    elements["steel"] = new Element(
        new SteelLight(),
        new SteelHeavy(),
        new SteelMod(),
        0
    );
    elements["gunpowder"] = new Element(
        new GunpowderLight(),
        new GunpowderHeavy(),
        new GunpowderMod(),
        1
    );
    elements["fire"] = new Element(
        new FireLight(),
        new FireHeavy(),
        new FireMod(),
        2
    );
    return elements;
}
