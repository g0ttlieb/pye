#pragma once

#include <SFML/Audio.hpp>
#include "weapon.hpp"

class GunpowderLight: public Weapon {
public:
    GunpowderLight();
    virtual ~GunpowderLight();

    virtual std::string getElementName();

    virtual bool applyGravity();
    virtual float xVelocity();

    virtual bool isCancellable();
    virtual int getIconIndex();

    virtual void attack(sf::FloatRect& position, Direction direction, bool moving);
    virtual void cancelAttack();
    virtual void update(float dt, std::vector<EnemyGroup*>& enemies, World& world);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    virtual void updateVolume();
    virtual void reset();
private:
    sf::SoundBuffer soundBuffer;
    sf::Sound sound;

    sf::FloatRect* pos;

    struct Shot {
        Shot(sf::FloatRect hitbox, Direction dir): hitbox(hitbox), dir(dir){}
        sf::FloatRect hitbox;
        Direction dir;
    };
    std::list<Shot> activeShots;

    bool createdShot;
    sf::Texture shotTexture;
    sf::VertexArray shotVertices;
    void createShotMoving();
    void createShotStationary();
    void addShot(float xOffset, float yOffset);
    void moveShots(float dt, World& world);
    void hitEnemies(std::vector<EnemyGroup*>& enemies);
    void prepShotVisual();
};
