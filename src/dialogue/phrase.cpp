#include "phrase.hpp"

const int NUMBER_OF_LINES = 5;

Phrase::Phrase(std::string name):
    name(name){
}

Phrase::~Phrase(){
    for(auto response : responses)
        delete response;
}

void Phrase::addLine(std::string line){
    if(line == "-")
        lines.push_back("");
    else
        lines.push_back(line);
}

std::string Phrase::getLine(int index){
    return lines[index];
}

void Phrase::addResponse(Response* response){
    responses.push_back(response);
}

bool Phrase::hasResponse(int index){
    return getResponse(index) != NULL;
}

Response* Phrase::getResponse(int index){
    int currentIndex = 0;
    for(auto current : responses){
        if(!current->shouldShow())
            continue;
        if(currentIndex == index)
            return current;
        currentIndex++;
    }
    return NULL;
}

int Phrase::getNumberOfLines(){
    return NUMBER_OF_LINES;
}
