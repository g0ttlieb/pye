#include "response.hpp"
#include "../inventory/inventory.hpp"

Response::Response(std::string text, std::string nextPhrase,
                   std::string showIfFlag, std::string setFlag):
        text(text),
        nextPhrase(nextPhrase),
        showIfFlag(showIfFlag),
        setFlag(setFlag){
}

bool Response::shouldShow(){
    if(showIfFlag == "-")
        return true;
    return Inventory::instance().hasCollectable(showIfFlag);
}

void Response::setSelectedFlag(){
    if(setFlag != "-")
        addFlag(setFlag);
}

void Response::addFlag(std::string flag){
    size_t commaIndex = flag.find(",");
    if(commaIndex != std::string::npos){
        addFlag(flag.substr(0, commaIndex));
        addFlag(flag.substr(commaIndex+1));
        return;
    }

    if(flag[0] == '%')
        Inventory::instance().addCollectable(flag.substr(1), true);
    else
        Inventory::instance().addCollectable(flag, false);
}
