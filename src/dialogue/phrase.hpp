#pragma once

#include <vector>
#include "response.hpp"

class Phrase {
public:
    Phrase(std::string name);
    ~Phrase();

    void addLine(std::string line);
    std::string getLine(int index);

    void addResponse(Response* response);
    bool hasResponse(int index);
    Response* getResponse(int index);

    static int getNumberOfLines();

    std::string name;
private:
    std::vector<Response*> responses;
    std::vector<std::string> lines;
};
