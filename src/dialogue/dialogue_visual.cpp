#include "dialogue_visual.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const int LINE_LEFT = 324;
const int LINE_TOP = 188;
const int RESP_LEFT = 262;
const int RESP_TOP = 276;

const int FONT_SIZE = 9;
const int LINE_HEIGHT = 13;

const int NPC_LEFT = 16;
const int NPC_TOP = 16;
const float NPC_SCALE_DOWN_FACTOR = 3.f;
const float NPC_SCALE = 1.f / NPC_SCALE_DOWN_FACTOR;

DialogueVisual::DialogueVisual(std::string npc){
    setupText();
    setupNpc(npc);
    setupOverlay();
    SpriteHelper::setScale(this);
}

void DialogueVisual::setupText(){
    font.loadFromFile(ResourceManager::getFont("dialogue.ttf"));
    float scale = Settings::instance().scale();

    int characterSize = FONT_SIZE * scale;
    for(int i = 0; i < Phrase::getNumberOfLines(); i++){
        lines.push_back(sf::Text());
        responses.push_back(sf::Text());

        lines[i].setFont(font);
        responses[i].setFont(font);

        lines[i].setCharacterSize(characterSize);
        responses[i].setCharacterSize(characterSize);

        lines[i].setFillColor(sf::Color::White);
        responses[i].setFillColor(sf::Color::White);

        lines[i].setPosition(LINE_LEFT * scale,
                (LINE_TOP + (i * LINE_HEIGHT)) * scale);
        responses[i].setPosition(RESP_LEFT * scale,
                (RESP_TOP + (i * LINE_HEIGHT)) * scale);
    }
}

void DialogueVisual::setupNpc(std::string npc){
    npcTexture.loadFromFile(ResourceManager::getVisual(npc + ".png"));
    npcSprite.setTexture(npcTexture);
    npcSprite.setPosition(NPC_LEFT, NPC_TOP);
    npcSprite.scale(NPC_SCALE, NPC_SCALE);
}

void DialogueVisual::setupOverlay(){
    overlayTexture.loadFromFile(ResourceManager::getVisual("dialogue-overlay.png"));
    overlaySprite.setTexture(overlayTexture);
    overlaySprite.setPosition(0.f, 0.f);
}

DialogueVisual::~DialogueVisual(){
}

void DialogueVisual::setPhrase(Phrase* phrase){
    for(int i = 0; i < Phrase::getNumberOfLines(); i++){
        lines[i].setString(phrase->getLine(i));
        std::string responseText = "";
        if(phrase->hasResponse(i))
            responseText = std::to_string(i+1) + ". " +
                phrase->getResponse(i)->text;
        responses[i].setString(responseText);
    }
}

void DialogueVisual::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    target.draw(overlaySprite, states);
    target.draw(npcSprite, states);
    for(int i = 0; i < Phrase::getNumberOfLines(); i++){
        target.draw(lines[i]);
        target.draw(responses[i]);
    }
}
