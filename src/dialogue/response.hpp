#pragma once

#include <string>

class Response {
public:
    Response(std::string text, std::string nextPhrase, std::string showIfFlag,
             std::string setFlag);

    bool shouldShow();
    void setSelectedFlag();

    std::string text;
    std::string nextPhrase;
private:
    std::string showIfFlag;
    std::string setFlag;
    void addFlag(std::string flag);
};
