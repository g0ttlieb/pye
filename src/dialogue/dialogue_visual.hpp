#pragma once

#include <SFML/Graphics.hpp>
#include "phrase.hpp"

class DialogueVisual : public sf::Drawable, public sf::Transformable {
public:
    DialogueVisual(std::string npc);
    ~DialogueVisual();
    void setPhrase(Phrase* phrase);
private:
    sf::Font font;
    std::vector<sf::Text> lines;
    std::vector<sf::Text> responses;
    void setupText();

    sf::Texture npcTexture;
    sf::Sprite npcSprite;
    void setupNpc(std::string npc);

    sf::Texture overlayTexture;
    sf::Sprite overlaySprite;
    void setupOverlay();

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};
