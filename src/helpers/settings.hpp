# pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include <string>

enum class WindowMode{
    BORDERLESS=0, FULLSCREEN=1, WINDOWED=2
};

class Settings {
public:
    static Settings& instance();

    void setupWindow(sf::Window* window);

    float scale();
    float getEffectsVolume();

    int getXRes();
    int getYRes();
    int getMode();
    WindowMode getWindowMode();
    bool vSyncEnabled();
    int getMusicLevel();
    int getEffectsLevel();

    bool resHasChanged();

    bool changeRes(int dif);
    void changeMusicLevel(int dif);
    void changeEffectsLevel(int dif);
    void setMode(WindowMode mode);
    void setVSync(bool enabled);
private:
    Settings();
    void saveSettings();

    sf::Image icon;

    bool resUpdated;

    float resScale;
    float effectsVolume;
    std::map<std::string, int> config;

    void initDefaultValues();
    void loadFromConfig();
    void loadLine(std::string line);
};
