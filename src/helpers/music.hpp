# pragma once

#include <SFML/Audio.hpp>

class Music {
public:
    static void playMusic(std::string music);
    static void setVolume(float volume);
private:
    Music();
    static Music& getInstance();

    sf::Music music;
    std::string currentlyPlaying;
};
