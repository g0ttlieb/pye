#include <cstdlib>
#include "ai_helpers.hpp"

const float Y_SIGHT_UP = 32.f;
const float Y_SIGHT_DOWN = 18.f;
const float BACKWARDS_SIGHT = -22.f;
const int FACING_TOWARDS = -1;

const int MAX_DROP = 4;
const int MAX_JUMP_ACROSS = 5;

const float TILE_X_PX = 16.f;
const float TILE_Y_PX = 8.f;

const float WIGGLE = 3.f;

bool AiHelpers::playerInSight(Enemy& enemy, World& world,
        sf::FloatRect& target){
    sf::FloatRect& pos = enemy.getPos();
    float xDif = (pos.left - target.left) * enemy.getDirection() * FACING_TOWARDS;
    float yDif = pos.top - target.top;
    if(xDif > BACKWARDS_SIGHT && xDif < enemy.getSightRange() &&
            yDif > -Y_SIGHT_DOWN && yDif < Y_SIGHT_UP)
        return world.isClearPath(sf::Vector2f(pos.left, pos.top),
                sf::Vector2f(target.left, target.top + (target.height/2.f)));
    return false;
}

bool AiHelpers::canAttack(Enemy& enemy, World& world,
        sf::FloatRect& target){
    if(enemy.isMidair())
        return false;
    sf::FloatRect attackRange = enemy.getAttackRange();
    if(!attackRange.intersects(target))
        return false;

    float rangeCheckY = attackRange.top + attackRange.height/2.f;
    sf::Vector2f targetPos(target.left, rangeCheckY);
    sf::Vector2f attackPos(attackRange.left, rangeCheckY);
    if(enemy.getDirection() == Direction::RIGHT)
        return world.isClearPath(attackPos, targetPos);
    return world.isClearPath(targetPos, attackPos);
}

bool AiHelpers::isBlockedByWall(Enemy& enemy, World& world){
    return !enemy.isMidair() && enemy.getDirection() == enemy.isNextToWall(world);
}

bool AiHelpers::isAboutToDrop(Enemy& enemy, World& world){
    if(enemy.isMidair())
        return false;
    sf::FloatRect& pos = enemy.getPos();
    float xPos = pos.left;
    if(enemy.getDirection() == Direction::RIGHT)
        xPos += pos.width;
    float yPos = pos.top + pos.height + WIGGLE;
    return !world.isObstacle(xPos, yPos);
}

void AiHelpers::moveInDirection(Enemy& enemy, Direction dir){
    if(dir == Direction::LEFT)
        enemy.moveLeft();
    else
        enemy.moveRight();
}

float AiHelpers::getRandom(float minVal, float maxVal){
    return minVal + (((float) rand() / RAND_MAX) * (maxVal - minVal));
}

int AiHelpers::getRandomInt(int minVal, int maxVal){
    return minVal + (rand() % (maxVal - minVal + 1));
}
