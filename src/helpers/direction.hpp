#pragma once

enum class Direction : signed char {
    LEFT = -1,
    RIGHT = 1
};

float operator*(float val, Direction dir);
float operator*(Direction dir, float val);
Direction operator!(Direction dir);

Direction makeDirection(float val);
Direction makeLeftDir(bool yes);
Direction makeRightDir(bool yes);
