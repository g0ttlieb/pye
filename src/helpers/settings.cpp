#include <SFML/Window.hpp>
#include <fstream>
#include "music.hpp"
#include "settings.hpp"
#include "resource_manager.hpp"
#include "../inventory/inventory.hpp"

const int ICON_SIZE = 120;
const int FPS_LIMIT = 60;
const int VOLUME_LIMIT = 10;

const int DEFAULT_RES = 1;
const int DEFAULT_MODE = 1;
const int DEFAULT_VSYNC = false;
const int DEFAULT_MUSIC_VOLUME = 7;
const int DEFAULT_EFFECTS_VOLUME = 7;

const std::vector<int>   X_RES {1280, 1920, 2560, 3840};
const std::vector<int>   Y_RES { 720, 1080, 1440, 2160};
const std::vector<float> SCALE {2.0f, 3.0f, 4.0f, 6.0f};

const std::vector<int> WINDOW_STYLE {
    sf::Style::None,
    sf::Style::Fullscreen,
    sf::Style::Titlebar | sf::Style::Close
};

Settings::Settings(){
    initDefaultValues();
    loadFromConfig();
    resScale = SCALE[config["resolution"]];
    effectsVolume = config["effects_volume"] * config["effects_volume"];
    Music::setVolume(config["music_volume"] * config["music_volume"]);
    icon.loadFromFile(ResourceManager::getVisual("icon.png"));
}

void Settings::initDefaultValues(){
    config.insert(std::make_pair("resolution", DEFAULT_RES));
    config.insert(std::make_pair("window_mode", DEFAULT_MODE));
    config.insert(std::make_pair("vsync", DEFAULT_VSYNC));
    config.insert(std::make_pair("music_volume", DEFAULT_MUSIC_VOLUME));
    config.insert(std::make_pair("effects_volume", DEFAULT_EFFECTS_VOLUME));
}

void Settings::loadFromConfig(){
    std::ifstream file(ResourceManager::getSystem("config.ini"));
    std::string line;
    while(std::getline(file, line))
        loadLine(line);
    file.close();
}

void Settings::loadLine(std::string line){
    if(line.size() == 0 || line[0] == ';')
        return;
    size_t equalsPos = line.find('=');
    if(equalsPos == std::string::npos)
        return;
    std::string key = line.substr(0, equalsPos);
    if(config.count(key) == 0)
        return;
    std::string valueStr = line.substr(equalsPos+1, line.size() - equalsPos - 1);
    try{
        int value = std::stoi(valueStr);
        config[key] = value;
    }
    catch(...){
    }
}

Settings& Settings::instance(){
    static Settings instance;
    return instance;
}

void Settings::saveSettings(){
    std::ofstream file(ResourceManager::getSystem("config.ini"));
    for(auto setting : config)
        file << setting.first << "=" << setting.second << std::endl;
    file.close();
}

void Settings::setupWindow(sf::Window* window){
    sf::VideoMode res(getXRes(), getYRes());
    window->create(res, "Pye", getMode());
    window->setIcon(ICON_SIZE, ICON_SIZE, icon.getPixelsPtr());
    window->setFramerateLimit(60);
    window->setKeyRepeatEnabled(false);
    window->setVerticalSyncEnabled(vSyncEnabled());
}

float Settings::scale(){
    return resScale;
}

float Settings::getEffectsVolume(){
    return effectsVolume;
}

int Settings::getXRes(){
    return X_RES[config["resolution"]];
}

int Settings::getYRes(){
    return Y_RES[config["resolution"]];
}

int Settings::getMode(){
    return WINDOW_STYLE[config["window_mode"]];
}

WindowMode Settings::getWindowMode(){
    return (WindowMode) config["window_mode"];
}

bool Settings::vSyncEnabled(){
    return config["vsync"];
}

int Settings::getMusicLevel(){
    return config["music_volume"];
}

int Settings::getEffectsLevel(){
    return config["effects_volume"];
}

bool Settings::resHasChanged(){
    if(!resUpdated)
        return false;
    resUpdated = false;
    return true;
}

bool Settings::changeRes(int dif){
    if(dif == 0)
        return false;
    int newRes = config["resolution"] + dif;
    if(newRes < 0 || newRes > (int) SCALE.size())
        return false;
    sf::VideoMode newMode(X_RES[newRes], Y_RES[newRes]);
    if(!newMode.isValid())
        return false;
    config["resolution"] = newRes;
    resScale = SCALE[newRes];
    resUpdated = true;
    saveSettings();
    Inventory::instance().updateResolution();
    return true;
}

void Settings::changeMusicLevel(int dif){
    if(dif == 0)
        return;
    int newVal = config["music_volume"] + dif;
    if(newVal < 0 || newVal > VOLUME_LIMIT)
        return;
    config["music_volume"] = newVal;
    saveSettings();
    Music::setVolume(newVal * newVal);
}

void  Settings::changeEffectsLevel(int dif){
    if(dif == 0)
        return;
    int newVal = config["effects_volume"] + dif;
    if(newVal < 0 || newVal > VOLUME_LIMIT)
        return;
    config["effects_volume"] = newVal;
    effectsVolume = newVal * newVal;
    saveSettings();
    Inventory::instance().updateVolume();
}

void Settings::setMode(WindowMode mode){
    config["window_mode"] = (int) mode;
    saveSettings();
}

void Settings::setVSync(bool enabled){
    config["vsync"] = enabled;
    saveSettings();
}
