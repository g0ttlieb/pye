#include "music.hpp"
#include "resource_manager.hpp"

Music::Music(){
    music.setLoop(true);
}

Music& Music::getInstance(){
    static Music instance;
    return instance;
}

void Music::playMusic(std::string track){
    Music& instance = getInstance();
    if(track == "-" || track == instance.currentlyPlaying)
        return;
    instance.currentlyPlaying = track;
    if(instance.music.openFromFile(ResourceManager::getMusic(track + ".wav")))
        instance.music.play();
}

void Music::setVolume(float volume){
    Music& instance = getInstance();
    instance.music.setVolume(volume);
}
