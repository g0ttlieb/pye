#pragma once

#include "../characters/enemy.hpp"
#include "../world/world.hpp"

class AiHelpers {
public:
    static bool playerInSight(Enemy& enemy, World& world, sf::FloatRect& target);
    static bool canAttack(Enemy& enemy, World& world, sf::FloatRect& target);

    static bool isBlockedByWall(Enemy& enemy, World& world);
    static bool isAboutToDrop(Enemy& enemy, World& world);
    static void moveInDirection(Enemy& enemy, Direction dir);

    static float getRandom(float minVal, float maxVal);
    static int getRandomInt(int minVal, int maxVal);
};
