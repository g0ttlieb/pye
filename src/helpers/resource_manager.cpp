#include <sstream>
#include "resource_manager.hpp"

#ifdef _WIN32
#include <Windows.h>
#elif __linux__
#include <unistd.h>
#endif

const int PATH_MAX = 2048;

ResourceManager::ResourceManager(){
#ifdef _WIN32
    sep = '\\';
    wchar_t result[MAX_PATH];
    GetModuleFileName(NULL, result, PATH_MAX);
    std::wstring temp(result);
    path = std::string(temp.begin(), temp.end());
#elif __linux__
    sep = '/';
    char result[PATH_MAX];
    readlink("/proc/self/exe", result, PATH_MAX);
    path = std::string(result);
#endif
    while (path.back() != sep)
        path.pop_back();
}

ResourceManager& ResourceManager::getInstance(){
    static ResourceManager instance;
    return instance;
}

std::string ResourceManager::getEffect(std::string file){
    return getFile("effects", file);
}

std::string ResourceManager::getFont(std::string file){
    return getFile("fonts", file);
}

std::string ResourceManager::getLevel(std::string file){
    return getFile("levels", file);
}

std::string ResourceManager::getMusic(std::string file){
    return getFile("music", file);
}

std::string ResourceManager::getSave(std::string file){
    return getFile("saves", file);
}

std::string ResourceManager::getScene(std::string file){
    return getFile("scenes", file);
}

std::string ResourceManager::getSystem(std::string file){
    return getFile("system", file);
}

std::string ResourceManager::getVisual(std::string file){
    return getFile("visual", file);
}

std::string ResourceManager::getFile(std::string location, std::string file){
    ResourceManager& rm = getInstance();
    std::ostringstream ss;
    ss << rm.path << "resources" << rm.sep << location << rm.sep << file;
    return ss.str();
}
