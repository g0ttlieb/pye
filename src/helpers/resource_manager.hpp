# pragma once

#include <string>

class ResourceManager {
public:
    static std::string getEffect(std::string file);
    static std::string getFont(std::string file);
    static std::string getLevel(std::string file);
    static std::string getMusic(std::string file);
    static std::string getSave(std::string file);
    static std::string getScene(std::string file);
    static std::string getSystem(std::string file);
    static std::string getVisual(std::string file);
private:
    char sep;
    std::string path;
    ResourceManager();
    static ResourceManager& getInstance();
    static std::string getFile(std::string location, std::string file);
};
