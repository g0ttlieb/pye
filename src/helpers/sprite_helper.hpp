#pragma once

#include <SFML/Graphics.hpp>
#include "direction.hpp"

class SpriteHelper {
public:
    static void shadeQuad(sf::Vertex* quad, sf::Color color);
    static void setQuadPos(sf::Vertex* quad, sf::FloatRect& pos);
    static void setQuadTex(sf::Vertex* quad, sf::FloatRect& tex);

    static void setColorQuad(sf::Vertex* quad, sf::FloatRect& pos,
            sf::Color color);
    static void setTextureQuad(sf::Vertex* quad, sf::FloatRect& pos,
            sf::FloatRect& texture);

    static void setTextureTile(sf::Sprite& sprite, int row, int col,
            int tileX, int tileY, Direction direction);

    static void centerAlignText(sf::Text& text, std::string content);
    static void leftAlignText(sf::Text& text, std::string content);
    static void rightAlignText(sf::Text& text, std::string content);

    static void setScale(sf::Transformable* object);
};
