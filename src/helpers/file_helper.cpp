#include <fstream>
#include "file_helper.hpp"
#include "resource_manager.hpp"

std::vector<std::string> FileHelper::readSaveFile(std::string name){
    return readG0ttliebFile(ResourceManager::getSave(name + ".g0ttlieb"));
}

std::vector<std::string> FileHelper::readSceneFile(std::string name){
    return readG0ttliebFile(ResourceManager::getScene(name + ".g0ttlieb"));
}

std::vector<std::string> FileHelper::readG0ttliebFile(std::string fullPath){
    std::vector<std::string> lines;
    std::string line;
    std::ifstream file(fullPath);
    while(std::getline(file, line)){
        if(!(line.find("#") == 0))
            lines.push_back(line);
    }
    file.close();
    return lines;
}

void FileHelper::writeSaveFile(std::string name, std::vector<std::string>& lines){
    std::ofstream file(ResourceManager::getSave(name + ".g0ttlieb"));
    for(std::string line : lines)
        file << line << std::endl;
    file.close();
}
