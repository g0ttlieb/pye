#pragma once

#include <vector>
#include <string>

class FileHelper {
public:
    static std::vector<std::string> readSaveFile(std::string name);
    static std::vector<std::string> readSceneFile(std::string name);

    static void writeSaveFile(std::string name, std::vector<std::string>& lines);
private:
    static std::vector<std::string> readG0ttliebFile(std::string fullPath);
};
