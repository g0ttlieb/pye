#include "sprite_helper.hpp"
#include "settings.hpp"

void SpriteHelper::shadeQuad(sf::Vertex* quad, sf::Color color){
    quad[0].color = color;
    quad[1].color = color;
    quad[2].color = color;
    quad[3].color = color;
}

void SpriteHelper::setQuadPos(sf::Vertex* quad, sf::FloatRect& pos){
    quad[0].position = sf::Vector2f(pos.left, pos.top);
    quad[1].position = sf::Vector2f(pos.left+pos.width, pos.top);
    quad[2].position = sf::Vector2f(pos.left+pos.width, pos.top+pos.height);
    quad[3].position = sf::Vector2f(pos.left, pos.top+pos.height);
}

void SpriteHelper::setQuadTex(sf::Vertex* quad, sf::FloatRect& tex){
    quad[0].texCoords = sf::Vector2f(tex.left, tex.top);
    quad[1].texCoords = sf::Vector2f(tex.left+tex.width, tex.top);
    quad[2].texCoords = sf::Vector2f(tex.left+tex.width, tex.top+tex.height);
    quad[3].texCoords = sf::Vector2f(tex.left, tex.top+tex.height);
}

void SpriteHelper::setColorQuad(sf::Vertex* quad, sf::FloatRect& pos,
        sf::Color color){
    setQuadPos(quad, pos);
    shadeQuad(quad, color);
}

void SpriteHelper::setTextureQuad(sf::Vertex* quad, sf::FloatRect& pos,
            sf::FloatRect& tex){
    setQuadPos(quad, pos);
    setQuadTex(quad, tex);
}

void SpriteHelper::setTextureTile(sf::Sprite& sprite, int row, int col,
        int tileX, int tileY, Direction direction){
    int xPos = col * tileX;
    if(direction == Direction::LEFT)
        xPos += tileX;
    sprite.setTextureRect(sf::IntRect(xPos, row * tileY, direction*tileX, tileY));
}

void SpriteHelper::centerAlignText(sf::Text& text, std::string content){
    text.setString(content);
    sf::FloatRect bounds = text.getLocalBounds();
    text.setOrigin((int) (bounds.left + bounds.width/2.f),
            (int) (bounds.top + bounds.height/2.f));
}

void SpriteHelper::leftAlignText(sf::Text& text, std::string content){
    text.setString(content);
    sf::FloatRect bounds = text.getLocalBounds();
    text.setOrigin((int) bounds.left,
            (int) (bounds.top + bounds.height/2.f));
}

void SpriteHelper::rightAlignText(sf::Text& text, std::string content){
    text.setString(content);
    sf::FloatRect bounds = text.getLocalBounds();
    text.setOrigin((int) (bounds.left + bounds.width),
            (int) (bounds.top + bounds.height/2.f));
}

void SpriteHelper::setScale(sf::Transformable* object){
    float scale = Settings::instance().scale();
    object->setScale(scale, scale);
}
