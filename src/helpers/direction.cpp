#include "direction.hpp"

float operator*(float val, Direction dir){
    return val * (float) dir;
}

float operator*(Direction dir, float val){
    return val * dir;
}

Direction operator!(Direction dir){
    if(dir == Direction::LEFT)
        return Direction::RIGHT;
    return Direction::LEFT;
}

Direction makeDirection(float val){
    if(val < 0)
        return Direction::LEFT;
    return Direction::RIGHT;
}

Direction makeLeftDir(bool yes){
    if(yes)
        return Direction::LEFT;
    return Direction::RIGHT;
}

Direction makeRightDir(bool yes){
    return makeLeftDir(!yes);
}
