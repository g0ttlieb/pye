#include "game_manager.hpp"
#include "scenes/scene_loader.hpp"

GameManager::GameManager(){
    scenes.push_back(SceneLoader::getScene(
                SceneTransition("menu_main", Transition::OVERLAY_CURRENT)));
}

GameManager::~GameManager(){
    for(auto scene: scenes)
        delete scene;
}

void GameManager::updateGame(float dt){
    handleSceneTransition(scenes.back()->getNextScene());
    if(scenes.empty()){
        quit = true;
        return;
    }
    bool doneActiveUpdate = false;
    for(auto it = scenes.rbegin(); it != scenes.rend(); it++){
        if(!doneActiveUpdate){
            (*it)->updateGame(dt);
            doneActiveUpdate = true;
        }
        else
            (*it)->inactiveUpdate();
    }
}

void GameManager::handleSceneTransition(SceneTransition newScene){
    if(newScene.exitAll()){
        for(auto scene : scenes)
            delete scene;
        scenes.clear();
    }
    else if(newScene.exitCurrentScene()){
        delete scenes.back();
        scenes.pop_back();
    }
    else if(newScene.exitToBase()){
        while(scenes.size() > 1){
            delete scenes.back();
            scenes.pop_back();
        }
    }
    if(!newScene.noNewScene())
        scenes.push_back(SceneLoader::getScene(newScene));
}

void GameManager::renderGame(sf::RenderWindow* window){
    for(auto scene: scenes)
        scene->renderGame(window);
    if(window->hasFocus())
        window->setMouseCursorVisible(scenes.back()->mouseVisible());
    else
        window->setMouseCursorVisible(true);
}

void GameManager::handleInput(sf::Event& e){
    if(e.type == sf::Event::Closed)
        quit = true;
    else if(e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Escape)
        handleSceneTransition(scenes.back()->escapeKeyScene());
    scenes.back()->getController().handleInput(e);
}

bool GameManager::shouldQuit(){
    return quit;
}
