#include "enemy_attack.hpp"
#include "../inventory/inventory.hpp"

const float GRAVITY = 350.f;
const float TILE_X = 16.f;
const float TILE_Y = 8.f;

EnemyAttack::EnemyAttack(float damage, sf::FloatRect hitbox):
    damage(damage),
    hitbox(hitbox),
    projectile(false),
    hasMaxRange(false),
    extendsTillWall(false),
    isDamageOverTime(false),
    texturePos(0.f, 0.f, hitbox.width, hitbox.height),
    finished(false){
}

EnemyAttack::~EnemyAttack(){}

void EnemyAttack::setProjectile(Direction _dir, float _xVel, float upVel,
        float gravity){
    projectile = true;
    dir = _dir;
    xVel = _xVel;
    yVel = -upVel;
    gravityCoef = gravity;
}

void EnemyAttack::setMaxRange(float range){
    hasMaxRange = true;
    maxRange = hitbox.left + (dir * range);
}

void EnemyAttack::setExtendTillWall(){
    extendsTillWall = true;
    needToExtendTillWall = true;
}

void EnemyAttack::setDamageOverTime(float totalDuration){
    isDamageOverTime = true;
    damage /= totalDuration;
    remainingTime = totalDuration;
}

void EnemyAttack::setTexturePos(sf::FloatRect pos){
    texturePos = pos;
}

bool EnemyAttack::isFinished(){
    return finished;
}

Direction EnemyAttack::getDirection(){
    return dir;
}

sf::FloatRect& EnemyAttack::getHitbox(){
    return hitbox;
}

sf::FloatRect EnemyAttack::getTexturePos(){
    return texturePos;
}

void EnemyAttack::update(float dt, World& world, sf::FloatRect playerPos){
    updateWidthIfInfinite(world);
    updateTime(dt);
    updatePos(dt);
    checkForPlayerHit(playerPos, dt);
    checkForCollision(world);
}

void EnemyAttack::updateWidthIfInfinite(World& world){
    if(!extendsTillWall || !needToExtendTillWall)
        return;
    int testPos = ((int) hitbox.left / TILE_X) * TILE_X;
    while(!world.isObstacle(testPos, hitbox.top))
        testPos += dir * TILE_X;
    hitbox.width = dir * (testPos - hitbox.left);
    if(dir == Direction::LEFT)
        hitbox.left = testPos;
    needToExtendTillWall = false;
}

void EnemyAttack::updateTime(float dt){
    if(!isDamageOverTime)
        return;
    remainingTime -= dt;
    if(remainingTime < 0)
        finished = true;
}

void EnemyAttack::updatePos(float dt){
    if(!projectile){
        finished = true;
        return;
    }
    hitbox.left += dir * xVel * dt;
    hitbox.top += yVel * dt;
    yVel += GRAVITY * gravityCoef * dt;
    if(!hasMaxRange)
        return;
    if(dir * (hitbox.left - maxRange) > 0){
        hitbox.left = maxRange;
        finished = true;
    }
}

void EnemyAttack::checkForPlayerHit(sf::FloatRect& playerPos, float dt){
    if(playerPos.intersects(hitbox)){
        if(isDamageOverTime)
            Inventory::instance().dealDamage(damage * dt);
        else{
            Inventory::instance().dealDamage(damage);
            finished = true;
        }
    }
}

// For now, we are only checking against the block the attack is currently in and
//   not accounting for any others that may have been moved through since the
//   last frame. This is much simpler / faster and should be fine unless we get
//   attacks that are moving fast enough.
// If we do need to change it over, pure x attacks just need to check every 16px
//   while mixed need to consider the current angle and thus all blocks moved
//   through in the past frame.
void EnemyAttack::checkForCollision(World& world){
    if(finished || extendsTillWall)
        return;
    float maxX = hitbox.left + hitbox.width;
    float maxY = hitbox.top + hitbox.height;
    for(float x = hitbox.left; x <= maxX; x += std::min(maxX-x, TILE_X)){
        for(float y = hitbox.top; y <= maxY; y += std::min(maxY-y, TILE_Y)){
            if(world.isObstacle(x, y)){
                finished = true;
                return;
            }
            if(y == maxY) break;
        }
        if(x == maxX) break;
    }
}

void EnemyAttack::setFinished(){
    finished = true;
}
