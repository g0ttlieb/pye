# pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include "character.hpp"
#include "enemy_attack.hpp"

class PathPlanner;

class Enemy : public Character {

    typedef void (*AI_CALLBACK)(Enemy&, PathPlanner& pathPlanner, World&,
                                sf::FloatRect&, float);

public:
    Enemy(float xPos, float yPos, float width, float height,
            float xOffset, float yOffset, float xTerminal, float yTerminal);
    virtual ~Enemy();

    sf::Vertex* getTexture();
    virtual bool drawHealthBar();

    void setMaxHp(int hp);
    void setDirection(Direction dir);
    void setTexture(float tilePx, sf::Vertex* texture);
    void setWalkingInfo(int nWalkingFrames, float walkingFrameDuration);
    void setAttackInfo(int nAttackFrames, float attackFrameDuration,
            int attackFrame, float attackCooldown);
    void setActiveAiCallbacks(AI_CALLBACK enabled, AI_CALLBACK disabled);
    void setPassiveAiCallbacks(AI_CALLBACK enabled, AI_CALLBACK disabled);

    bool areDead();
    float getHpPercent();
    float getHpMax();
    float getHpCurrent();
    virtual void dealDamage(float damage);
    virtual void onDeath();

    std::map<std::string, float>& getState();
    void activateAggression();
    void deactivateAggression();
    bool areAggressive();

    virtual bool attack();
    bool areAttacking();
    virtual bool hasNewAttack();
    virtual float getAttackingVel();
    virtual bool attackHasVisual() = 0;
    virtual EnemyAttack* createAttack() = 0;
    virtual sf::FloatRect getAttackRange() = 0;

    virtual float getSightRange() = 0;
    virtual float getCenterOffset() = 0;

    virtual void jump();
    void move();
    void turnAround();
    virtual void moveLeft();
    virtual void moveRight();

    void run();
    void walk(float slow);
    float getHorizSpeed();
    float getVertSpeed();

    bool areStaggered();
    virtual void stagger(float timeStunned, float timeKnocked, Direction dir,
                         float velKnockBack, float velKnockUp);

    virtual void update(float dt, World& world);

    virtual void activeUpdateAI(PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);
    virtual void passiveUpdateAI(PathPlanner& pathPlanner, World& world,
            sf::FloatRect& target, float dt);

protected:
    bool attacking;
    float attackCooldown;
    float attackRemainingCooldown;

    int frame;
    int textureRow;
    virtual void updateVisual(float dt);

private:
    std::map<std::string, float> state;

    float maxHp;
    float currentHp;
    float currentHpPercent;

    Direction knockedDir;
    float knockedVel;
    float runTerminal;

    int nWalkingFrames;
    float walkingFrameDuration;

    bool createdAttack;
    int attackFrame;
    int nAttackFrames;
    float attackFrameDuration;
    float totalAttackTime;

    float tilePx;
    float xOffset;
    float yOffset;

    int textureCol;
    sf::Vertex* texture;
    void setTexturePosition();

    float deathTime;
    float stunnedTime;
    float knockedTime;
    float walkingTime;
    float attackingTime;
    virtual void updatePosition(float dt, World& world);
    virtual void updateAttack(float dt);

    AI_CALLBACK activeAiEnabled;
    AI_CALLBACK activeAiDisabled;
    AI_CALLBACK passiveAiEnabled;
    AI_CALLBACK passiveAiDisabled;
};
