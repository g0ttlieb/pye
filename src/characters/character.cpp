#include <algorithm>
#include "character.hpp"
#include "../helpers/settings.hpp"

const float GRAVITY = 350.f;
const float WIGGLE = 0.001f;
const float WORLD_TILE_PX = 8.f;

const int TOP_LEFT = 0;
const int TOP_RIGHT = 1;
const int BOT_RIGHT = 2;
const int BOT_LEFT = 3;

const int TILE_X_PX = 16;
const int TILE_Y_PX = 8;
const int CENTER_X = 320;

Character::Character(sf::FloatRect position, float xTerminal, float yTerminal):
    position(position),
    xDirection(Direction::RIGHT),
    xVel(0.f),
    yVel(0.f),
    xTerminal(xTerminal),
    yTerminal(yTerminal),
    midair(false){}

Character::~Character(){}

bool Character::isMidair(){
    return midair;
}

Direction Character::getDirection(){
    return xDirection;
}

bool Character::isStationary(){
    if(midair)
        return false;
    return xVel == 0;
}

sf::FloatRect& Character::getPos(){
    return position;
}

float Character::getBackFootX(){
    float pos = position.left;
    if(xDirection == Direction::LEFT)
        pos += position.width;
    return pos;
}

float Character::getFrontFootX(){
    float pos = position.left;
    if(xDirection == Direction::RIGHT)
        pos += position.width;
    return pos;
}

float Character::getFeetY(){
    return position.top + position.height;
}

Direction Character::isNextToWall(World& world){
    float leftPos = position.left - WIGGLE - WIGGLE;
    float rightPos = position.left + position.width + WIGGLE;
    for(float yPos = position.top; yPos <= position.top + position.height;
            yPos += WORLD_TILE_PX){
        if(world.isObstacle(leftPos, yPos))
            return Direction::LEFT;
        if(world.isObstacle(rightPos, yPos))
            return Direction::RIGHT;
    }
    return (Direction) 0;
}

void Character::jump(){
    if(!midair){
        yVel = -yTerminal;
        midair = true;
    }
}

void Character::moveLeft(){
    xVel = xTerminal;
    xDirection = Direction::LEFT;
}

void Character::moveRight(){
    xVel = xTerminal;
    xDirection = Direction::RIGHT;
}

void Character::stopMoving(){
    xVel = 0;
}

void Character::stopMovingLeft(){
    if(xDirection == Direction::LEFT)
        xVel = 0;
}

void Character::stopMovingRight(){
    if(xDirection == Direction::RIGHT)
        xVel = 0;
}

void Character::update(float dt, World& worldRef){
    world = &worldRef;
    updatePosition(dt);
    checkForDrop();
    handleCollisions();
    checkForMiddleCollision();
}

void Character::updatePosition(float dt){
    position.left += xDirection * xVel * dt;
    if(midair){
        position.top += yVel * dt;
        yVel += GRAVITY * dt;
    }
}

// Check for collisions fully contained within the hitbox.
// Only a problem if midair as should get a full wall collision if grounded.
// If characters are introduced that are big enough to not be able to get under
//   ledges that they are taller than we need to remove this easy out.
void Character::checkForMiddleCollision(){
    if(!midair)
        return;
    float leftPos = position.left;
    float rightPos = position.left + position.width - WIGGLE;
    for(float yPos = position.top + TILE_Y_PX;
            yPos < position.top + position.height; yPos += TILE_Y_PX){
        if(world->isObstacle(leftPos, yPos)){
            correctSide(Side::LEFT);
            return;
        }
        else if(world->isObstacle(rightPos, yPos)){
            correctSide(Side::RIGHT);
            return;
        }
    }

}

void Character::checkForDrop(){
    if(midair)
        return;
    bool rightGrounded = world->isObstacle(position.left + position.width - WIGGLE,
            position.top + position.height + WIGGLE);
    bool leftGrounded = world->isObstacle(position.left,
            position.top + position.height + WIGGLE);
    if(!leftGrounded && !rightGrounded){
        midair = true;
        yVel = 0;
    }
}

// Move character to be flush with any wall that they are inside.
void Character::handleCollisions(){
    std::vector<bool> collisions = getCollisions();
    int collidedCorners = std::count(collisions.begin(), collisions.end(), true);
    if(collidedCorners == 0)
        return;
    else if (collidedCorners == 1){
        if(collisions[TOP_LEFT])
            correctCorner(Side::LEFT, Side::TOP);
        else if(collisions[TOP_RIGHT])
            correctCorner(Side::RIGHT, Side::TOP);
        else if(collisions[BOT_RIGHT])
            correctCorner(Side::RIGHT, Side::BOT);
        else if(collisions[BOT_LEFT])
            correctCorner(Side::LEFT, Side::BOT);
    }
    else if(collidedCorners < 4){
        if(collisions[TOP_LEFT] && collisions[TOP_RIGHT])
            correctSide(Side::TOP);
        if(collisions[BOT_RIGHT] && collisions[BOT_LEFT])
            correctSide(Side::BOT);
        if(collisions[TOP_RIGHT] && collisions[BOT_RIGHT])
            correctSide(Side::RIGHT);
        if(collisions[BOT_LEFT] && collisions[TOP_LEFT])
            correctSide(Side::LEFT);
    }
    else
        pushToCenter();
}

std::vector<bool> Character::getCollisions(){
    std::vector<bool> vec(4);
    vec[TOP_LEFT] = world->isObstacle(position.left,
            position.top);
    vec[TOP_RIGHT] = world->isObstacle(position.left + position.width - WIGGLE,
            position.top);
    vec[BOT_RIGHT] = world->isObstacle(position.left + position.width - WIGGLE,
            position.top + position.height - WIGGLE);
    vec[BOT_LEFT] = world->isObstacle(position.left,
            position.top + position.height - WIGGLE);
    return vec;
}

// Correct the side that requires the least work, with priority given to vertical.
void Character::correctCorner(Side horizSide, Side vertSide){
    float verticalDifference = calculateDifference(vertSide);
    float horizontalDifference = calculateDifference(horizSide);
    if(verticalDifference <= horizontalDifference)
        correctSide(vertSide);
    else
        correctSide(horizSide);
}

void Character::correctSide(Side side){
    float newPos = calculateNewPos(side);
    if(side == Side::RIGHT)
        position.left = newPos - position.width;
    else if(side == Side::LEFT)
        position.left = newPos;
    else if(side == Side::BOT){
        position.top = newPos - position.height;
        if(yVel > 0)
            midair = false;
    }
    else if(side == Side::TOP){
        position.top = newPos;
        if(yVel < 0)
            yVel = 0;
    }
}

// Assuming that no character can move more than a tile per frame so it is
//   sufficient to bump them back to the nearest tile.
float Character::calculateNewPos(Side side){
    if(side == Side::LEFT){
        float current = position.left;
        if(current < 0) return WIGGLE;
        else return (((((int) current / TILE_X_PX) + 1)) * TILE_X_PX) + WIGGLE;
    }
    else if(side == Side::RIGHT){
        float current = position.left + position.width;
        return ((((int) current / TILE_X_PX)) * TILE_X_PX) - WIGGLE;
    }
    else if(side == Side::TOP){
        float current = position.top;
        if(current < 0) return WIGGLE;
        else return (((((int) current / TILE_Y_PX) + 1)) * TILE_Y_PX) + WIGGLE;
    }
    else /* if(side == Side::BOT) */ {
        float current = position.top + position.height;
        return ((((int) current / TILE_Y_PX)) * TILE_Y_PX) - WIGGLE;
    }
}

float Character::calculateDifference(Side side){
    float current;
    float newPos = calculateNewPos(side);
    if(side == Side::LEFT){
        current = position.left;
        while(world->isObstacle(newPos, position.top)
                || world->isObstacle(newPos, position.top + position.height))
            newPos += TILE_X_PX;
    }
    else if(side == Side::RIGHT){
        current = position.left + position.width;
        while(world->isObstacle(newPos, position.top)
                || world->isObstacle(newPos, position.top + position.height))
            newPos -= TILE_X_PX;
    }
    else if(side == Side::TOP){
        current = position.top;
        while(world->isObstacle(position.left, newPos) ||
                world->isObstacle(position.left + position.width, newPos))
            newPos += TILE_Y_PX;
    }
    else /* if(side == Side::BOT) */{
        current = position.top + position.height;
        newPos = ((((int) current / TILE_Y_PX)) * TILE_Y_PX) - WIGGLE;
        while(world->isObstacle(position.left, newPos) ||
                world->isObstacle(position.left + position.width, newPos))
            newPos -= TILE_Y_PX;
    }
    return std::abs(current - newPos);
}

void Character::pushToCenter(){
    if(position.left < CENTER_X)
        position.left = (((int) position.left / TILE_X_PX) + 1) * TILE_X_PX;
    else
        position.left = (((int)(position.left + position.width) / TILE_X_PX)
                * TILE_X_PX) - position.width;
}
