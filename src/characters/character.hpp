# pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include "../world/world.hpp"
#include "../helpers/direction.hpp"

enum class Side{LEFT, RIGHT, TOP, BOT};

class Character {
public:
    Character(sf::FloatRect position, float xTerminal, float yTerminal);
    virtual ~Character();

    bool isMidair();
    Direction getDirection();
    bool isStationary();
    sf::FloatRect& getPos();
    float getBackFootX();
    float getFrontFootX();
    float getFeetY();
    Direction isNextToWall(World& world);

    virtual void jump();
    virtual void moveLeft();
    virtual void moveRight();
    virtual void stopMoving();
    virtual void stopMovingLeft();
    virtual void stopMovingRight();

    virtual void update(float dt, World& world);
protected:
    sf::FloatRect position;
    Direction xDirection;
    float xVel;
    float yVel;
    float xTerminal;
    float yTerminal;
    bool midair;
private:
    World* world;
    void updatePosition(float dt);
    void checkForMiddleCollision();
    void checkForDrop();
    void handleCollisions();
    std::vector<bool> getCollisions();
    void correctCorner(Side horizontalSide, Side verticalSide);
    void correctSide(Side side);
    float calculateNewPos(Side side);
    float calculateDifference(Side side);
    void pushToCenter();
};
