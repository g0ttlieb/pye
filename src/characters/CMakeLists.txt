file(GLOB character_files "*.cpp")
add_library(characters ${character_files})

target_link_libraries(characters
    controllers
    enemies
    helpers
    inventory
    weapons
    world
    ${SFML_LIBRARIES}
)
