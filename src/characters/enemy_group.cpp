#include "enemy_group.hpp"
#include "spawner.hpp"
#include "../controllers/enemy_controller.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"

const float HP_JUT = 1.f;
const float HP_HOVER = 1.f;
const float HP_HEIGHT = 1.f;
const float HP_INSET = 0.25f;

const sf::Color HP_BACK_COLOR(115,14,35,255);
const sf::Color HP_FORE_COLOR(217,24,64,255);
sf::FloatRect DISABLED(0,0,0,0);

EnemyGroup::EnemyGroup(std::string name,
            std::vector<std::pair<sf::Vector2f,Direction>>& spawns):
        name(name),
        aiEnabled(true){
    texture.loadFromFile(ResourceManager::getVisual(name + ".png"));
    textureVertices.setPrimitiveType(sf::Quads);
    textureVertices.resize(spawns.size() * 4);
    for(unsigned int i = 0; i < spawns.size(); i++){
        sf::Vertex* quad = &textureVertices[i*4];
        sf::Vector2f pos = spawns[i].first;
        Direction dir = spawns[i].second;
        instances.push_back(Spawner::create(name, pos.x, pos.y, dir, quad));
    }
    numberAlive = instances.size();
    shouldDrawHealthBars = instances.front()->drawHealthBar();
    attackHasVisual = instances.front()->attackHasVisual();
    if(attackHasVisual){
        attackTexture.loadFromFile(ResourceManager::getVisual(name+"-attack.png"));
        attackTexture.setRepeated(true);
    }
    SpriteHelper::setScale(this);
}

EnemyGroup::~EnemyGroup(){
    for(auto instance : instances)
        delete instance;
    for(auto attack : attacks)
        delete attack;
}

std::string EnemyGroup::getName(){
    return name;
}

bool EnemyGroup::areRemainingEnemies(){
    return numberAlive > 0;
}

std::list<Enemy*>& EnemyGroup::getEnemies(){
    return instances;
}

void EnemyGroup::disableAi(){
    aiEnabled = false;
}

void EnemyGroup::removeEnemy(Enemy* enemy){
    int index = 0;
    for(auto it = instances.begin(); it != instances.end(); ++it){
        index++;
        if(*it != enemy)
            continue;
        instances.erase(it);
        SpriteHelper::setTextureQuad(enemy->getTexture(), DISABLED, DISABLED);
        delete enemy;
        return;
    }
}

void EnemyGroup::update(float dt, PathPlanner& pathPlanner, World& world,
        sf::FloatRect& playerPos){
    numberAlive = 0;
    for(auto enemy : instances){
        if(aiEnabled)
            EnemyController::update(*enemy, pathPlanner, world, playerPos, dt);
        enemy->update(dt, world);
        if(enemy->hasNewAttack())
            attacks.push_back(enemy->createAttack());
        if(!enemy->areDead())
            numberAlive++;
    }
    for(auto current = attacks.begin(); current != attacks.end();){
        if((*current)->isFinished()){
            delete *current;
            current = attacks.erase(current);
        }
        else{
            (*current)->update(dt, world, playerPos);
            ++current;
        }
    }
}

void EnemyGroup::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    if(shouldDrawHealthBars)
        drawHealthBars(target, states);
    states.texture = &texture;
    target.draw(textureVertices, states);
    if(attackHasVisual){
        states.texture = &attackTexture;
        drawAttacks(target, states);
    }
}

void EnemyGroup::drawHealthBars(sf::RenderTarget& target,
        sf::RenderStates& states) const {
    sf::VertexArray healthBars(sf::Quads, numberAlive*8);
    int currentIndex = 0;
    for(auto enemy : instances){
        if(!enemy->areDead()){
            setHealthBar(&healthBars[currentIndex], enemy);
            currentIndex += 8;
        }
    }
    target.draw(healthBars, states);
}

void EnemyGroup::setHealthBar(sf::Vertex* quad, Enemy* enemy) const{
    sf::FloatRect enemyPos = enemy->getPos();
    sf::FloatRect pos(enemyPos.left - HP_JUT, enemyPos.top - HP_HOVER - HP_HEIGHT,
            enemyPos.width + HP_JUT + HP_JUT, HP_HEIGHT);
    SpriteHelper::setColorQuad(quad, pos, HP_BACK_COLOR);

    pos.left += HP_INSET;
    pos.top += HP_INSET;
    pos.width -= HP_INSET * 2.f;
    pos.height -= HP_INSET * 2.f;
    pos.width *= enemy->getHpPercent();
    SpriteHelper::setColorQuad(quad + 4, pos, HP_FORE_COLOR);
}

void EnemyGroup::drawAttacks(sf::RenderTarget& target, sf::RenderStates& states)
        const {
    sf::VertexArray attackVertices(sf::Quads, attacks.size()*4);
    int index = 0;
    for(auto attack : attacks){
        Direction dir = attack->getDirection();
        sf::FloatRect pos = attack->getHitbox();
        sf::FloatRect tex = attack->getTexturePos();
        if(dir == Direction::LEFT){
            tex.width *= -1;
            tex.left += pos.width;
        }
        SpriteHelper::setTextureQuad(&attackVertices[index], pos, tex);
        index += 4;
    }
    target.draw(attackVertices, states);
}
