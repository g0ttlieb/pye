#include "spawner.hpp"

#include "../enemies/horned_melee.hpp"
#include "../enemies/horned_ranged.hpp"
#include "../enemies/woodland_sentry.hpp"
#include "../enemies/soldier_axe.hpp"
#include "../enemies/deserter_axe.hpp"
#include "../enemies/soldier_sword.hpp"
#include "../enemies/deserter_sword.hpp"
#include "../enemies/soldier_bow.hpp"
#include "../enemies/deserter_bow.hpp"
#include "../enemies/lupine.hpp"
#include "../enemies/spirit.hpp"
#include "../enemies/ballcrab.hpp"
#include "../enemies/actrian_leader.hpp"

Enemy* Spawner::create(std::string name, float posX, float posY, Direction dir,
        sf::Vertex* texture){
    if(name == "horned-melee")
        return new HornedMelee(posX, posY, dir, texture);
    else if(name == "horned-ranged")
        return new HornedRanged(posX, posY, dir, texture);
    else if(name == "woodland-sentry")
        return new WoodlandSentry(posX, posY, dir, texture);
    else if(name == "soldier-axe")
        return new SoldierAxe(posX, posY, dir, texture);
    else if(name == "deserter-axe")
        return new DeserterAxe(posX, posY, dir, texture);
    else if(name == "soldier-sword")
        return new SoldierSword(posX, posY, dir, texture);
    else if(name == "deserter-sword")
        return new DeserterSword(posX, posY, dir, texture);
    else if(name == "soldier-bow")
        return new SoldierBow(posX, posY, dir, texture);
    else if(name == "deserter-bow")
        return new DeserterBow(posX, posY, dir, texture);
    else if(name == "lupine")
        return new Lupine(posX, posY, dir, texture);
    else if(name == "spirit")
        return new Spirit(posX, posY, dir, texture);
    else if(name == "ballcrab")
        return new Ballcrab(posX, posY, dir, texture);
    else if(name == "actrian-leader")
        return new ActrianLeader(posX, posY, dir, texture);
    return NULL;
}
