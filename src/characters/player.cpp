#include "player.hpp"
#include "../helpers/resource_manager.hpp"
#include "../helpers/settings.hpp"
#include "../helpers/sprite_helper.hpp"
#include "../inventory/inventory.hpp"

const float X_TERMINAL_VEL = 85.f;
const float Y_TERMINAL_VEL = 175.f;

const float DASH_VEL = 160.f;
const float DASH_FALLOFF = 225.f;
const float DASH_COOLDOWN = 0.5f;

const float SLIDE_VEL = 30.f;
const float SLIDE_THRESHOLD_MIN = 0.f;
const float SLIDE_THRESHOLD_MAX = 200.f;
const float SLIDE_JUMP_GRACE_PERIOD = 0.1f;

const float WIDTH = 6.f;
const float HEIGHT = 12.f;
const float X_OFFSET = 5.f;
const float Y_OFFSET = 4.f;

const int SPRITE_TILE_PX = 16;
const int N_WALKING_FRAMES = 8;
const float FRAME_DURATION = 0.08f;

const float WIGGLE = 0.001f;
const float WORLD_TILE_PX = 8.f;
const float HALFWAY_X = 320.f;
const float MAX_Y = 360.f;
const float JUMP_UP_BOOST = 3.f;

const float DYING_FRAME_DURATION = 0.12f;
const int N_DYING_FRAMES = 9;
const float DYING_TIME = N_DYING_FRAMES * DYING_FRAME_DURATION + 0.8f;

const float WARPING_FRAME_DURATION = 0.1f;
const int N_WARPING_FRAMES = 13;
const float WARPING_TIME = N_WARPING_FRAMES * WARPING_FRAME_DURATION;

Player::Player(float xPos, float yPos):
    Character(sf::FloatRect(xPos, yPos + Y_OFFSET - WIGGLE, WIDTH, HEIGHT),
            X_TERMINAL_VEL, Y_TERMINAL_VEL),
    attacking(false),
    sliding(false),
    recentlySliding(0.f),
    recentlyJumped(0.f),
    canDash(true),
    dashing(false),
    dashCooldown(0.f),
    animationTime(0.f),
    dying(false),
    warping(false){
        texture.loadFromFile(ResourceManager::getVisual("player.png"));
        sprite.setTexture(texture);
        loadSounds();
        SpriteHelper::setScale(this);
        updateVisual(0.f);
        if(position.left > HALFWAY_X)
            xDirection = Direction::LEFT;
        if(position.top > MAX_Y){
            position.top = MAX_Y - HEIGHT - JUMP_UP_BOOST;
            Character::jump(); // Don't play jump sound
        }
}

void Player::loadSounds(){
    dashSoundBuffer.loadFromFile(ResourceManager::getEffect("player-dash.wav"));
    jumpSoundBuffer.loadFromFile(ResourceManager::getEffect("player-jump.wav"));
    dashSound.setBuffer(dashSoundBuffer);
    jumpSound.setBuffer(jumpSoundBuffer);
    float vol = Settings::instance().getEffectsVolume();
    dashSound.setVolume(vol);
    jumpSound.setVolume(vol);
}

Player::~Player(){}

void Player::lightAttack(){
    attack(Inventory::instance().lightWeapon());
}

void Player::heavyAttack(){
    attack(Inventory::instance().heavyWeapon());
}

void Player::attack(Weapon* weapon){
    if(weapon == nullptr)
        return;
    if(attacking && (!currentWeapon->isCancellable() || weapon == currentWeapon))
        return;
    if(sliding)
        return;
    if(midair && weapon->requireGrounded())
        return;
    if(weapon->onCooldown())
        return;
    if(attacking)
        cancelAttack();
    currentWeapon = weapon;
    startAttack();
}

void Player::startAttack(){
    currentWeapon->attack(position, xDirection, xVel > 0);
    sprite.setTexture(currentWeapon->getTexture());
    attacking = true;
    finishDash();
    postActionDir = xDirection;
    postActionVel = xVel;
    yVel = 0;
}

void Player::cancelAttack(){
    currentWeapon->cancelAttack();
    finishAttack();
}

void Player::finishAttack(){
    attacking = false;
    sprite.setTexture(texture);
    xDirection = postActionDir;
    xVel = postActionVel;
}

bool Player::canCancelAttack(){
    if(!attacking)
        return true;
    if(currentWeapon->isCancellable()){
        cancelAttack();
        return true;
    }
    return false;
}

void Player::dash(){
    if(!canDash || sliding)
        return;
    if(!canCancelAttack())
        return;
    postActionDir = xDirection;
    postActionVel = xVel;
    xVel = DASH_VEL;
    yVel = 0;
    dashing = true;
    canDash = false;
    dashCooldown = DASH_COOLDOWN;
    dashSound.play();
}

void Player::jump(){
    if(!sliding && recentlySliding > 0)
        midair = false;
    if(sliding){
        recentlyJumped = SLIDE_JUMP_GRACE_PERIOD;
        return;
    }
    if(midair || !canCancelAttack())
        return;
    resetDash();
    jumpSound.play();
    Character::jump();
}

void Player::moveLeft(){
    if(dashing || attacking){
        postActionVel = xTerminal;
        postActionDir = Direction::LEFT;
    }
    else
        Character::moveLeft();
}

void Player::moveRight(){
    if(dashing || attacking){
        postActionVel = xTerminal;
        postActionDir = Direction::RIGHT;
    }
    else
        Character::moveRight();
}

void Player::stopMoving(){
    if(dashing | attacking)
        postActionVel = 0;
    else
        Character::stopMoving();
}

void Player::stopMovingLeft(){
    if(dashing || attacking){
        if(postActionDir == Direction::LEFT)
            postActionVel = 0;
    }
    else
        Character::stopMovingLeft();
}

void Player::stopMovingRight(){
    if(dashing || attacking){
        if(postActionDir == Direction::RIGHT)
            postActionVel = 0;
    }
    else
        Character::stopMovingRight();
}

void Player::die(){
    if(!dying){
        if(attacking)
            cancelAttack();
        stopMoving();
        if(midair && yVel < 0)
            yVel = 0.f;
        dying = true;
        animationTime = 0.f;
    }
}

bool Player::isWarping(){
    return warping;
}

bool Player::isDying(){
    return dying;
}

bool Player::finishedWarp(){
    return (warping && animationTime > WARPING_TIME);
}

bool Player::finishedDying(){
    return (dying && animationTime > DYING_TIME);
}

void Player::updateWarp(float dt){
    if(!warping){
        warping = true;
        animationTime = 0.f;
        sprite.setTexture(texture);
    }
    updateVisual(dt);
}

void Player::update(float dt, World& world){
    if(dying){
        Character::update(dt, world);
        sprite.setPosition(position.left - X_OFFSET, position.top - Y_OFFSET);
        updateVisual(dt);
        return;
    }

    if(attacking){
        xVel = currentWeapon->xVelocity();
        if(!currentWeapon->applyGravity())
            yVel = 0;
    }
    Character::update(dt, world);
    updateWallCling(dt, world);
    updateDash(dt);
    sprite.setPosition(position.left - X_OFFSET, position.top - Y_OFFSET);
    updateVisual(dt);
}

void Player::updateWallCling(float dt, World& world){
    // Must be before slide update so not still sliding
    if(recentlyJumped > 0 && recentlySliding > 0){
        jump();
        recentlyJumped = 0.f;
        recentlySliding = 0.f;
    }
    else if(recentlyJumped > 0)
        recentlyJumped -= dt;

    if(sliding && xDirection != isNextToWall(world))
        recentlySliding = SLIDE_JUMP_GRACE_PERIOD;
    else if(recentlySliding > 0)
        recentlySliding -= dt;

    if(shouldClingToWall(world)){
        resetDash();
        yVel = SLIDE_VEL;
        sliding = true;
    }
    else
        sliding = false;
}

bool Player::shouldClingToWall(World& world){
    if(!midair || attacking)
        return false;
    if(!sliding && (yVel < SLIDE_THRESHOLD_MIN || yVel > SLIDE_THRESHOLD_MAX))
        return false;
    Direction wallDirection = isNextToWall(world);
    return wallDirection == Direction::LEFT || wallDirection == Direction::RIGHT;
}

void Player::updateDash(float dt){
    if(dashCooldown > 0)
        dashCooldown -= dt;
    else if(!midair)
        canDash = true;

    if(!dashing)
        return;
    else if(xVel > X_TERMINAL_VEL){
        xVel -= dt * DASH_FALLOFF;
        yVel = 0;
    }
    else
        finishDash();
}

void Player::resetDash(){
    canDash = true;
    dashCooldown = 0.f;
    finishDash();
}

void Player::finishDash(){
    if(dashing){
        dashing = false;
        xVel = postActionVel;
        xDirection = postActionDir;
    }
}

void Player::updateVisual(float dt){
    if(attacking && currentWeapon->animationIsFinished())
        finishAttack();

    animationTime += dt;
    bool resetAnimation = true;
    if(dying && !midair){
        int frame = std::min((int) (animationTime / DYING_FRAME_DURATION),
                             N_DYING_FRAMES);
        selectSprite(3, frame);
        resetAnimation = false;
    }
    else if(warping){
        int frame = std::min((int) (animationTime / WARPING_FRAME_DURATION),
                             N_WARPING_FRAMES);
        selectSprite(2, frame);
        resetAnimation = false;
    }
    else if(attacking)
        currentWeapon->loadFrame(sprite, xDirection);
    else if(sliding)
        selectSprite(1,3);
    else if(dashing)
        selectSprite(1,2);
    else if(midair)
        selectSprite(1,1);
    else if(xVel == 0)
        selectSprite(1,0);
    else{
        resetAnimation = false;
        int frame = (int) (animationTime / FRAME_DURATION) % N_WALKING_FRAMES;
        selectSprite(0,frame);
    }
    if(resetAnimation)
        animationTime = 0.f;
}

void Player::selectSprite(int row, int col){
    SpriteHelper::setTextureTile(sprite, row, col, SPRITE_TILE_PX, SPRITE_TILE_PX,
            xDirection);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= getTransform();
    target.draw(sprite, states);
}
