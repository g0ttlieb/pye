#pragma once

#include "enemy.hpp"

class Spawner {
public:
    static Enemy* create(std::string name, float posX, float posY, Direction dir,
            sf::Vertex* texture);
};
