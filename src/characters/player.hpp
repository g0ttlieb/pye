# pragma once

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "character.hpp"
#include "../weapons/weapon.hpp"

class Player : public Character, public sf::Drawable, public sf::Transformable {
public:
    Player(float xPos, float yPos);
    virtual ~Player();

    void lightAttack();
    void heavyAttack();

    void dash();
    virtual void jump();
    virtual void moveLeft();
    virtual void moveRight();
    virtual void stopMoving();
    virtual void stopMovingLeft();
    virtual void stopMovingRight();

    void die();
    bool isWarping();
    bool isDying();
    bool finishedDying();
    bool finishedWarp();

    void updateWarp(float dt);
    virtual void update(float dt, World& world);
private:
    Direction postActionDir;
    float postActionVel;

    Weapon* currentWeapon;
    bool attacking;
    void attack(Weapon* weapon);
    void startAttack();
    void cancelAttack();
    void finishAttack();
    bool canCancelAttack();

    bool sliding;
    float recentlySliding;
    float recentlyJumped;
    void updateWallCling(float dt, World& world);
    bool shouldClingToWall(World& world);

    bool canDash;
    bool dashing;
    float dashCooldown;
    void updateDash(float dt);
    void resetDash();
    void finishDash();

    float animationTime;
    bool dying;
    bool warping;
    sf::Texture texture;
    sf::Sprite sprite;
    void updateVisual(float dt);
    void selectSprite(int row, int col);
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    sf::SoundBuffer dashSoundBuffer;
    sf::Sound dashSound;
    sf::SoundBuffer jumpSoundBuffer;
    sf::Sound jumpSound;
    void loadSounds();
};
