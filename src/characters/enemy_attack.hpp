#pragma once

#include <SFML/Graphics.hpp>
#include "../world/world.hpp"
#include "../helpers/direction.hpp"

class EnemyAttack{
public:
    EnemyAttack(float damage, sf::FloatRect hitbox);
    ~EnemyAttack();

    void setProjectile(Direction dir, float xVel, float upVel, float gravityCoef);
    void setMaxRange(float range);
    void setExtendTillWall();
    void setDamageOverTime(float totalDuration);
    void setTexturePos(sf::FloatRect pos);

    bool isFinished();
    Direction getDirection();
    sf::FloatRect& getHitbox();
    sf::FloatRect getTexturePos();

    void update(float dt, World& world, sf::FloatRect playerPos);
    void setFinished();
private:
    float damage;
    sf::FloatRect hitbox;

    bool projectile;
    Direction dir;
    float xVel;
    float yVel;
    float gravityCoef;

    bool hasMaxRange;
    float maxRange;

    bool extendsTillWall;
    bool needToExtendTillWall;

    bool isDamageOverTime;
    float remainingTime;

    sf::FloatRect texturePos;

    bool finished;

    void updateWidthIfInfinite(World& world);
    void updateTime(float dt);
    void updatePos(float dt);
    void checkForPlayerHit(sf::FloatRect& playerPos, float dt);
    void checkForCollision(World& world);
};
