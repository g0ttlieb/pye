# pragma once

#include <list>
#include "enemy_attack.hpp"
#include "enemy.hpp"

class EnemyGroup : public sf::Drawable, public sf::Transformable {
public:
    EnemyGroup(std::string name,
            std::vector<std::pair<sf::Vector2f,Direction>>& spawns);
    ~EnemyGroup();

    std::string getName();
    bool areRemainingEnemies();
    std::list<Enemy*>& getEnemies();
    void disableAi();
    void removeEnemy(Enemy* enemy);

    void update(float dt, PathPlanner& pathPlanner, World& world,
            sf::FloatRect& playerPos);
private:
    std::string name;
    bool aiEnabled;

    std::list<EnemyAttack*> attacks;
    std::list<Enemy*> instances;

    sf::Texture texture;
    sf::VertexArray textureVertices;
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    bool shouldDrawHealthBars;
    int numberAlive;
    void setHealthBar(sf::Vertex* quad, Enemy* enemy) const;
    void drawHealthBars(sf::RenderTarget& target, sf::RenderStates& states) const;

    bool attackHasVisual;
    sf::Texture attackTexture;
    void drawAttacks(sf::RenderTarget& target, sf::RenderStates& states) const;
};
