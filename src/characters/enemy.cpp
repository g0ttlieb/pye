#include <cstdlib>
#include "enemy.hpp"
#include "../helpers/sprite_helper.hpp"

const float DEATH_TIME = 1.f;
const float ACTIVE_COOLDOWN = 5.f;

const float RNG_SPEED_MIN = 1.f;
const float RNG_SPEED_DIF = 0.2f;

const float A_LITTLE_BIT_LONGER = 0.01f;

const int MOVE_TEXTURES = 0;
const int MISC_TEXTURES = 1;
const int ATTACK_TEXTURES = 2;
const int PASSIVE_STATIONARY_TEXTURE = 0;
const int ACTIVE_STATIONARY_TEXTURE = 1;
const int MIDAIR_TEXTURE = 2;
const int STUNNED_TEXTURE = 3;
const int DYING_TEXTURE = 6;
const int DEAD_TEXTURE = 7;

Enemy::Enemy(float xPos, float yPos, float width, float height,
             float xOffset, float yOffset, float xTerminal, float yTerminal):
        Character(sf::FloatRect(xPos+xOffset, yPos+yOffset, width, height),
                xTerminal, yTerminal),
        attacking(false),
        attackRemainingCooldown(0.f),
        xOffset(xOffset),
        yOffset(yOffset),
        deathTime(0.f),
        stunnedTime(0.f),
        knockedTime(0.f),
        walkingTime(0.f){
    float speed = RNG_SPEED_MIN + rand() / (RAND_MAX/RNG_SPEED_DIF);
    this->xTerminal *= speed;
    runTerminal = this->xTerminal;
}

Enemy::~Enemy(){}

sf::Vertex* Enemy::getTexture(){
    return texture;
}

bool Enemy::drawHealthBar(){
    return true;
}

void Enemy::setMaxHp(int hp){
    maxHp = hp;
    currentHp = hp;
    currentHpPercent = 1.f;
}

void Enemy::setDirection(Direction dir){
    xDirection = dir;
}

void Enemy::setTexture(float _tilePx, sf::Vertex* _texture){
    tilePx = _tilePx;
    texture = _texture;
}

void Enemy::setWalkingInfo(int _nWalkingFrames, float _walkingFrameDuration){
    nWalkingFrames = _nWalkingFrames;
    walkingFrameDuration = _walkingFrameDuration;
}

void Enemy::setAttackInfo(int _nAttackFrames, float _attackFrameDuration,
        int _attackFrame, float _attackCooldown){
    nAttackFrames = _nAttackFrames;
    attackFrameDuration = _attackFrameDuration;
    attackFrame = _attackFrame;
    totalAttackTime = nAttackFrames * attackFrameDuration;
    attackCooldown = _attackCooldown;
}

void Enemy::setActiveAiCallbacks(AI_CALLBACK enabled, AI_CALLBACK disabled){
    activeAiEnabled = enabled;
    activeAiDisabled = disabled;
}

void Enemy::setPassiveAiCallbacks(AI_CALLBACK enabled, AI_CALLBACK disabled){
    passiveAiEnabled = enabled;
    passiveAiDisabled = disabled;
}

bool Enemy::areDead(){
    return currentHp <= 0;
}

float Enemy::getHpPercent(){
    return currentHpPercent;
}

float Enemy::getHpMax(){
    return maxHp;
}

float Enemy::getHpCurrent(){
    return currentHp;
}

void Enemy::dealDamage(float damage){
    if(areDead())
        return;
    currentHp -= std::min(damage, currentHp);
    currentHpPercent = currentHp / maxHp;
    activateAggression();
    if(areDead()){
        deathTime = DEATH_TIME;
        onDeath();
    }
}

void Enemy::onDeath(){
}

std::map<std::string, float>& Enemy::getState(){
    return state;
}

void Enemy::activateAggression(){
    state["activeCooldown"] = ACTIVE_COOLDOWN;
}

void Enemy::deactivateAggression(){
    state["activeCooldown"] = 0.f;
}

bool Enemy::areAggressive(){
    return state["activeCooldown"] > 0.f;
}

bool Enemy::attack(){
    if(attacking || midair || attackRemainingCooldown > 0)
        return false;
    attacking = true;
    attackingTime = 0.f;
    createdAttack = false;
    attackRemainingCooldown = attackCooldown;
    return true;
}

bool Enemy::areAttacking(){
    return attacking;
}

bool Enemy::hasNewAttack(){
    if(!areDead() && attacking && textureCol == attackFrame && !createdAttack){
        createdAttack = true;
        return true;
    }
    return false;
}

float Enemy::getAttackingVel(){
    return 0.f;
}

void Enemy::jump(){
    if(!areDead() && !attacking)
        Character::jump();
}

void Enemy::move(){
    if(!attacking)
        xVel = xTerminal;
}

void Enemy::turnAround(){
    if(!attacking)
        xDirection = !xDirection;
}

void Enemy::moveLeft(){
    if(!attacking)
        Character::moveLeft();
}

void Enemy::moveRight(){
    if(!attacking)
        Character::moveRight();
}

void Enemy::run(){
    xTerminal = runTerminal;
}

void Enemy::walk(float slow){
    xTerminal = runTerminal * slow;
}

float Enemy::getHorizSpeed(){
    return runTerminal;
}

float Enemy::getVertSpeed(){
    return yTerminal;
}

void Enemy::stagger(float timeStunned, float timeKnocked, Direction dir,
                    float velKnockBack, float velKnockUp){
    state["MOVE_TOWARDS_X"] = -1; // Clear cache so re-path to player
    stunnedTime = timeStunned;
    knockedTime = timeKnocked;
    knockedVel = xDirection * velKnockBack * dir;
    knockedDir = xDirection;
    yVel = -velKnockUp;
    if(velKnockUp != 0)
        midair = true;
}

bool Enemy::areStaggered(){
    return (stunnedTime > 0 || knockedTime > 0);
}

void Enemy::update(float dt, World& world){
    updatePosition(dt, world);
    updateAttack(dt);
    updateVisual(dt);
    setTexturePosition();
}

void Enemy::updatePosition(float dt, World& world){
    if(areDead() && !midair)
        return;
    if(stunnedTime > 0){
        xVel = 0;
        stunnedTime -= dt;
        if(stunnedTime <= 0 && midair)
            stunnedTime = A_LITTLE_BIT_LONGER;
        attacking = false;
    }
    if(knockedTime > 0){
        xVel = knockedVel;
        xDirection = knockedDir;
        knockedTime -= dt;
        if(knockedTime <= 0){
            if(midair)
                knockedTime = A_LITTLE_BIT_LONGER;
            else
                xVel = 0;
        }
        attacking = false;
    }
    if(attacking)
        xVel = getAttackingVel();
    Character::update(dt, world);
}

void Enemy::updateAttack(float dt){
    if(attackRemainingCooldown > 0)
        attackRemainingCooldown -= dt;
    if(!attacking)
        return;
    attackingTime += dt;
    if(attackingTime >= totalAttackTime){
        attacking = false;
        xVel = 0;
    }
}

void Enemy::updateVisual(float dt){
    bool walking = false;
    if(areDead() && (deathTime > 0.f || midair)){
        deathTime -= dt;
        textureRow = MISC_TEXTURES;
        textureCol = DYING_TEXTURE;
    }
    else if(areDead()){
        textureRow = MISC_TEXTURES;
        textureCol = DEAD_TEXTURE;
    }
    else if(stunnedTime > 0.f || knockedTime > 0.f){
        textureRow = MISC_TEXTURES;
        textureCol = STUNNED_TEXTURE;
    }
    else if(attacking){
        textureRow = ATTACK_TEXTURES;
        textureCol = (int) (attackingTime / attackFrameDuration);
    }
    else if(midair){
        textureRow = MISC_TEXTURES;
        textureCol = MIDAIR_TEXTURE;
    }
    else if(xVel == 0){
        textureRow = MISC_TEXTURES;
        if(areAggressive())
            textureCol = ACTIVE_STATIONARY_TEXTURE;
        else
            textureCol = PASSIVE_STATIONARY_TEXTURE;
    }
    else{
        walking = true;
        walkingTime += dt;
        textureRow = MOVE_TEXTURES;
        textureCol = (int) (walkingTime / walkingFrameDuration) % nWalkingFrames;
    }

    if(!walking)
        walkingTime = 0.f;
    if(attacking || walking)
        frame = textureCol;
    else
        frame = -1;
}

void Enemy::setTexturePosition(){
    float leftPos;
    if(xDirection == Direction::LEFT)
        leftPos = position.left - tilePx + position.width + xOffset;
    else
        leftPos = position.left - xOffset;
    sf::FloatRect pos(leftPos, position.top - yOffset,
            tilePx, tilePx);
    sf::FloatRect tex(textureCol * tilePx, textureRow * tilePx,
            xDirection * tilePx, tilePx);
    if(xDirection == Direction::LEFT)
        tex.left += tilePx;
    SpriteHelper::setTextureQuad(texture, pos, tex);
}

void Enemy::passiveUpdateAI(PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    passiveAiEnabled(*this, pathPlanner, world, target, dt);
    activeAiDisabled(*this, pathPlanner, world, target, dt);
}

void Enemy::activeUpdateAI(PathPlanner& pathPlanner, World& world,
        sf::FloatRect& target, float dt){
    activeAiEnabled(*this, pathPlanner, world, target, dt);
    passiveAiDisabled(*this, pathPlanner, world, target, dt);
}
