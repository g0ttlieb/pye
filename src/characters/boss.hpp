#pragma once

#include "enemy.hpp"

struct BossAttack {
    sf::FloatRect hitbox;
    bool isMelee;
};

class Boss : public Enemy {
public:
    Boss(float xPos, float yPos, float w, float h, float xOffset, float yOffset,
         float xVel, float yVel):
        Enemy(xPos, yPos, w, h, xOffset, yOffset, xVel, yVel){
    }
    virtual std::vector<BossAttack> getAttackOptions() = 0;
    virtual void setAttack(int index) = 0;
    virtual int numberOfAttacks() = 0;
};
