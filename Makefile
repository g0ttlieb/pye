.PHONY: help
help: ## Lists the available commands.
# Add a comment with '##' to describe a command.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build
build: ## Build the docker image
	docker build --tag pye .

.PHONY: run
run: ## Run the docker image
	xhost local:docker
	docker run -it --rm -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix pye:latest ./build/Pye

.PHONY: exec
exec: build run ## Build and then run the program
